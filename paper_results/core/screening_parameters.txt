# if you want to test multiple surface CIFs then add the name (name.cif) with commas inbetween
surfaces:CuOH2
# if you want to test multiple surface CIFs then add the name (name.cif) with commas inbetween
sub_metal:Cu
# set sub atom ionic radii for tolerance
# using IV coordinate Cu 2+ Ionic radii
sub_radii:0.57
# set preferential miller planes for each surface
surf_millers:010
# set type of films:
# currently support 'MOFs' or 'COFs'
film_type:MOFs
# you can leave the cifs row free if you want to just test all input CIF files in the current directory. Note that there is an exlusion list in "write_CIF_TODO.py" which ignores phrases associated with output CIFs.
films:
# set CIF TODO file name
TODO_name:CIF_TODO
# set output file name
output_csv_name:screening_results.csv
# set name of porosity CSV
porosity_csv:survey_porosity.csv
# set porosity property to test
porosity_prop:SA [m2/g]
# configuration of binding sites
configuration:carboxylate
# set the desired metal in the film structure
# "*" implies any metal is accetable
film_metal:Cu
# set film atom ionic radii for tolerance
# using II coordinate O 2- Ionic radii
film_radii:1.35
# set lattice matching rules:
# max area ratio tolerance set to 15%  - arbitrarily
max_area_ratio_tol:0.15
# an arbitrary hard limit for now - much bigger than the substrate size!
# this is for the lattice code where we just want to find any match within
# the tolerances
max_area:5000
film_max_miller:1
sub_max_miller:1
# length tolerance set to 10% - arbitrarily to allow for large translational
# mismatch that can occur for soft materials
max_length_tol:0.1
# angle tolerance set to 2% - arbitrarily, but here we do not allow for large
# deviations to simplify the mismatch and stresses to be translational
max_angle_tol:0.02
# ASO settings
# Number of MC trials
trials:40
# Effective inverse temperature of MC simulation
beta:50
# Initial resolution of trial moves in MC simulations (Angstrom)
resolution:0.5
# number of MC steps per trial
steps:400
# minimum angle from XY plane allowed in configuration search
XY_angle:30
# maximum distance in the Z component between oxygens in a carboxylate allowed
OO_buffer:0.6
# maximum area ratio of the film multiple to test in the ASO code
# this is different to the lattice matching code.
# note that the Zur algorithm goes up to the maximum area but is not inclusive!
# because of coding in pymatgen substrate analyser code:
#    - this value needs to be your desired value +1 - NO LONGER!
f_max_area_multi:9
# maximum area ratio of the film multiple to test in the ASO code
# this is different to the lattice matching code.
# note that the Zur algorithm goes up to the maximum area but is not inclusive!
# because of coding in pymatgen substrate analyser code:
#    - this value needs to be your desired value +1 - NO LONGER!
s_max_area_multi:90
# angstroms to consider in Z range for configuration search
# consider maximal Z distance between atoms in your configuration to test
# the buffer for substrates (Cu(OH)2 for example) is automatically set to 0.8
buffer:1.2
# ASO proliferation settings
# (switch, max_aso)
# if switch is true, then all CIFs go through to the next stage
# if switch is false, then we remove CIFs that do not produce a maximum ASO >
# min_aso
ASO_switch:T
ASO_min_aso:0.5
# set maximum number of CPUs to use - if None, then it will use the max available.
N_CPUs:6
# switch for whether to check for already completed binding planes
# defaults to False (meaning the code will produce binding planes)
# this process can be costly
check_complete:T
# Plot as a function of topology?
topology_switch:T
# CSV file with all topologies for this database of CIFs.
topology_csv:topis_CORE_formatted.csv
