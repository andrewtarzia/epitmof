# epitMOF

Source code and examples for screening of MOFs for aligned heteroepitaxial growth published:

- DOI: 
- CHEMRXIV: https://chemrxiv.org/articles/High-Throughput_Screening_of_Metal_organic_Frameworks_for_Macroscale_Heteroepitaxial_Alignment/7012007

Written by Andrew Tarzia (andrew.tarzia@adelaide.edu.au). 
All feedback and criticisms are welcome and appreciated.

# Installation

I have not done a good job of making this more than a collection of Python scripts. Therefore....

- Clone the repo
- Make sure all the dependancies are installed (below)
- Run through the terminal using the commands in src/README.txt

Only Python3 and libraries that are available through CONDA or PIP are required (requirements for source code is in src/requirements.txt)

- See ASE website for installation (https://wiki.fysik.dtu.dk/ase/)
- See Pymatgen website for installation (http://pymatgen.org/index.html)
- Note that this has been tested on Ubuntu 14.04 and 16.04.

# License

epitMOF is released under the MIT License. See src/LICENSE.txt

# Note

Throughout this git repo I have used the file name 'CuOH2' for the copper hydroxide CIF. This really should be changed.
