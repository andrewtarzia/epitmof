#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script to determine and output the progress of a current run of the screening
algorithm.
"""

import argparse
import time
import glob
import general_functions as genf
import analysis_output_functions as aoo

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

print("=====================================================================")
print("Progress report for epitCIF screening!")
print("=====================================================================")
start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# read in CIF TODO list as dataframe
CIF_TODO_files_step_1 = glob.glob(param_dict['TODO_name'][0]+"_step_1_*.csv")
CIF_TODO_files_step_3 = glob.glob(param_dict['TODO_name'][0]+"_step_3_*.csv")

# get the MOF list from all CIF_TODO files
# move MOFs into status dict
print("=====================================================================")
print("Step 1:")
status_dict_step_1 = aoo.get_status_dict_step_1(CIF_TODO_files_step_1)
print("=====================================================================")
print("Interpenetration Stats:")
aoo.get_interpen_info(CIF_TODO_files_step_3)
print("=====================================================================")
print("Step 3:")
status_dict_step_3 = aoo.get_status_dict_step_3(CIF_TODO_files_step_3)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
