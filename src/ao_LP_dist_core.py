#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses the unit cell angles of MOF binding planes in the CORE MOF database.

Produces components of Figures in the supproting information.
"""

import time
import glob
import numpy as np
import os
import pandas as pd
from collections import Counter
import argparse
from ase.io import read
import matplotlib
matplotlib.use('agg')
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import general_functions as genf
import ao_plot_fn as aoplt
import analysis_output_functions as aoo

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# topology file
topo_file = param_dict['topology_csv'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

# plot with topology set features?
plot_topo = False
if param_dict['topology_switch'][0] == 'T':
    plot_topo = True
if plot_topo is True:
    topo_data = pd.read_csv(topo_file)
    # ignoring unknown topologies ("-")
    counter = Counter([i for i in list(topo_data['topology']) if i != '-'])
    unknown_count = len([i for i in list(topo_data['topology']) if i == '-'])
    total_count = len(list(topo_data['topology']))
    # set colours for selected topologies only
    topo_select_names = ['tbo', 'pcu', 'dia', 'rob', 'fof', 'nbo', 'tfc']

    # ['dia', 'pcu', 'tbo', 'rob', 'fof', 'nbo', 'tfc']
    topo_select = [(i, counter[i]) for i in topo_select_names]
    # print(topo_select_names)
    # print(topo_select)
    # topo_select = counter.most_common()[:6]
    topo_select_names = [i[0] for i in topo_select]
    topo_select_counts = [i[1] for i in topo_select]
    other_count = len([i for i in list(topo_data['topology'])
                       if i != '-' and i not in topo_select_names])
    if unknown_count == 0:
        topo_colours = cm.Paired(np.linspace(0, 0.5, len(topo_select) + 1))
        topo_markers = ['o', 'X', 'P', 'v', '^', '<', '>', 's', 'p', 'D']
    else:
        topo_colours = cm.Paired(np.linspace(0, 0.5, len(topo_select) + 2))
        topo_markers = ['o', 'X', 'P', 'v', '^', '<', '>', 's', 'p', 'D']

# set standard plot values
standard_plot_values = {'i':
                        {'s': 80, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 0.8, 'marker': 'o'}}

# separate results into two files
res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

# list of interpenetrated CIFs
interpen_films = []
films = list(set(per_MOF_data['film']))
for i in films:
    if 'i_'+i+'_0' in films:
        interpen_films.append(i)

# define figures
fig = plt.figure()
# define sub plots of figure
ax1 = plt.subplot()

# define colour map
# set mid point based on DIB threshold of 0.67
DIB_thresh = 0.67

new_cmap = aoplt.define_plot_cmap(fig, ax1, DIB_thresh, cm.RdBu,
                                  ticks=[0, 0.25, 0.5, DIB_thresh, 0.75, 1.0],
                                  labels=['0', '0.25', '0.5', str(DIB_thresh),
                                          '0.75', '1.0'],
                                  cmap_label='$\Delta$IB')

fig1, ax21 = plt.subplots(figsize=(8, 5))  # 2, 1, figsize=(5, 10))
fig2, ax12 = plt.subplots(figsize=(8, 5))  # 2, 1, figsize=(8, 10))

new_cmap = aoplt.define_plot_cmap(fig1, ax21, DIB_thresh, cm.RdBu,
                                  ticks=[0, 0.25, 0.5, DIB_thresh, 0.75, 1.0],
                                  labels=['0', '0.25', '0.5', str(DIB_thresh),
                                          '0.75', '1.0'],
                                  cmap_label='$\Delta$IB$_2$')

# add a band for DIB figure
ax21.axhline(0.67, c='r', linestyle='--', alpha=0.5)
ax21.axhspan(0.6, 0.77, facecolor='grey', alpha=0.3)

hi_dib_angles = []
lo_dib_angles = []

for idx, row in per_MOF_data.iterrows():
    film = row['film']
    print('film:', film)
    # want to ignore interpenetrated versions of CIFs
    if film in interpen_films:
        continue
    # check if any data was collected
    if row['1_max_DIB'] == 'None':
        continue
    # assign DIB values
    else:
        DIB1 = float(row['1_max_DIB'])
        if row['2_max_DIB'] == 'None':
            DIB2 = 0
        else:
            DIB2 = float(row['2_max_DIB'])

    # top candidate?
    if DIB1 >= DIB_thresh:
        if DIB2 >= DIB_thresh:
            alpha = 0.4
        else:
            print('top candidate:', DIB1)
            alpha = 1.0
    else:
        alpha = 0.4

    # set plot values
    if film[:2] == 'i_':
        plt_val = standard_plot_values['i']
        no = film.split("_")[-1]
        short_name = film.replace('i_', '').replace("_"+no, '')
    else:
        plt_val = standard_plot_values['i']
        short_name = film
    if plot_topo is False:
        C = plt_val['c']
        M = plt_val['marker']
    elif plot_topo is True:
        # change marker to match topologies
        f_topo = topo_data[topo_data['film'] == short_name]
        topo = f_topo['topology'].iloc[0]
        if topo in topo_select_names:
            C = topo_colours[topo_select_names.index(topo)]
            M = topo_markers[topo_select_names.index(topo)]
        elif topo == '-':
            # unknown
            C = topo_colours[-1]
            M = topo_markers[-1]
        else:
            # other
            if unknown_count == 0:
                C = topo_colours[-1]
                M = topo_markers[-1]
            else:
                C = topo_colours[-2]
                M = topo_markers[-2]

    hkl = str(row['1_f_hkl'])
    y = DIB1
    y2 = DIB2
    config = str(row['1_config'])
    # get unit cell angles from binding plane
    BP_name = film+"_"+hkl+"_"+config+"_bind_plane.cif"

    if os.path.isfile(BP_name) is True:
        cif = read(BP_name)
        cell_angle = cif.get_cell_lengths_and_angles()[5]
    else:
        continue
    x = cell_angle
    ax21.scatter(x, y,
                 s=plt_val['s'],
                 alpha=alpha,
                 c=new_cmap(y2),
                 edgecolors=plt_val['edgecolors'],
                 marker=M)

    if DIB1 >= DIB_thresh and DIB2 < DIB_thresh:
        hi_dib_angles.append(x)
    else:
        lo_dib_angles.append(x)

    # get all other LPs of binding planes not considered the max DIB and
    # add to lo_dib_angles - because they are not top candidate binding planes
    other_BP = glob.glob(film+"*_bind_plane.cif")
    for BP in other_BP:
        if BP != BP_name:
            cif = read(BP)
            cell_angle = cif.get_cell_lengths_and_angles()[5]
            lo_dib_angles.append(cell_angle)


# decoy legend
for i, j in enumerate(topo_select):
    M = topo_markers[i]
    perc = (j[1] / total_count) * 100
    ax21.scatter(-1000,
                 -1000,
                 c='none',
                 marker=M,
                 s=plt_val['s'],
                 alpha=plt_val['alpha'],
                 edgecolors=plt_val['edgecolors'],
                 label=j[0]+' ('+str(round(perc, 1))+'%)')
M = topo_markers[-2]
perc = (other_count / total_count) * 100
ax21.scatter(-1000,
             -1000,
             c='none',
             marker=M,
             s=plt_val['s'],
             alpha=plt_val['alpha'],
             edgecolors=plt_val['edgecolors'],
             label='other ('+str(round(perc, 1))+'%)')
M = topo_markers[-1]
perc = (unknown_count / total_count) * 100
ax21.scatter(-1000,
             -1000,
             c='none',
             marker=M,
             s=plt_val['s'],
             alpha=plt_val['alpha'],
             edgecolors=plt_val['edgecolors'],
             label='unknown ('+str(round(perc, 1))+'%)')
ax21.legend(fancybox=True, ncol=3,
            fontsize=10)

# plot distribution figure
ax12.hist(lo_dib_angles, bins=range(0, 180 + 5, 2),
          alpha=0.4, density=True, histtype='stepfilled',
          label='not aligned',
          color='r')
ax12.hist(hi_dib_angles, bins=range(0, 180 + 5, 2),
          alpha=0.4, density=True, histtype='stepfilled',
          label='aligned',
          color='b')

ax12.legend(loc=1, fancybox=True, ncol=1, fontsize=16)

aoplt.define_plot_variables(ax21,
                            title='',
                            xtitle='in-plane unit cell angle [$\degree$]',
                            ytitle='$\Delta$IB',
                            xlim=(0, 180),
                            ylim=(0, 1.15))
aoplt.define_hist_variables(ax12,
                            title='',
                            ytitle='frequency',
                            xtitle="in-plane unit cell angle [$\degree$]",
                            xlim=(0, 180),
                            ylim=(0, 0.42))
ax12.set_xticks([0, 30, 60, 90, 120, 150, 180])
ax21.set_xticks([0, 30, 60, 90, 120, 150, 180])

##############################################################################
# save fig
fig.tight_layout()
fig.savefig("LP_cf_figure_cmap_core.pdf",
            bbox_inches='tight', dpi=720)
fig1.tight_layout()
fig1.savefig("LP_cf_DIB_core.pdf",
             bbox_inches='tight', dpi=720)
fig2.tight_layout()
fig2.savefig("LP_cf_dist_core.pdf",
             bbox_inches='tight', dpi=720)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
