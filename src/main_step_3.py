#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script that runs step 3 of the screening algorithm on a series of CIFs in the
current directory.
"""

import time
import argparse
import os
import sys
import pandas as pd
import numpy as np
import pymatgen as mg
from pymatgen.core.surface import generate_all_slabs
# My module IMPORTS #
import general_functions as genf
import binding_plane as bp
import monte_carlo as MC
import ASO_functions as ASO
# next import is modified PYMATGEN code
import pmg_substrate_analyzer as pmgsa_AT
import analysis_output_functions as aoo
import matplotlib
matplotlib.use('agg')

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

genf.print_welcome_message()
start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Run main step 3")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
parser.add_argument('NP', metavar='NP', type=int, nargs='+',
                    help='process to run. associated with CIFS_TODO_NP.csv')

args = parser.parse_args()
NP = args.NP[0]
param_file = args.paramfile[0]

# read in parameter dictionary
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# read in CIF list as dataframe
CIF_TODO_file = param_dict['TODO_name'][0]+"_step_3_"+str(NP)+".csv"
if os.path.isfile(CIF_TODO_file) is True:
    CIF_TODO = pd.read_csv(CIF_TODO_file)
else:
    sys.exit("CIF_TODO file does not exist! Exitting!")

# porosity file
porosity_file = param_dict['porosity_csv'][0]

# %%

# obtain necessary surface information for all required surfaces
print("------------------------------------------------------")
print("collecting surface information...")

substrates = {}
for surf, surf_metal in zip(param_dict['surfaces'], param_dict['sub_metal']):
    substrate = mg.Structure.from_file(surf+".cif")
    # build unit plane for all surfaces in surf_millers
    # as defined by configuration
    print('building', surf, 'slabs')
    # get substrate millers
    if param_dict['surf_millers'][0] == "*":
        substrate_millers = None
    else:
        substrate_millers = [genf.miller_reader(param_dict['surf_millers'][0])]
    sub_structures, s_millers = bp.produce_surface_slabs(surf, substrate,
                                                         substrate_millers,
                                                         surf_metal,
                                                         0.8)
    substrates[surf] = [sub_structures, s_millers]
print('done')
print("------------------------------------------------------")

# %%

# status meaning
# status = 0 : not tested
# status = 1 : did not pass chemistry test
# status = 2 : passed chemistry test
# status = 3 : did not pass lattice test
# status = 4 : passed lattice test
# status = 4a : binding planes built
# status = 5 : completed ASO screening


for index, row in CIF_TODO.iterrows():
    status = row['status']
    surface = row['surface name']
    film = row['film name']
    film_metal = param_dict['film_metal'][0]
    print("=== surface: ",
          surface,
          " -- film:",
          film,
          "===")

# %%

    if status == 4 or str(status) == '4a':
        # do ASO test
        print("------------------------------------------------------")
        print("doing ASO test...")
        ASO_start_time = time.time()
        completed_ASO = False
        # Set MC and configuration search settings
        # Number of MC trials
        trials = [int(i) for i in param_dict['trials']]
        # Effective inverse temperature of MC simulation
        beta = float(param_dict['beta'][0])
        # Initial resolution of trial moves in MC simulations (Angstrom)
        resolution = float(param_dict['resolution'][0])
        # number of MC steps per trial
        steps = int(param_dict['steps'][0])
        # minimum angle from XY plane allowed in configuration search
        XY_angle = float(param_dict['XY_angle'][0])
        # maximum distance in the Z component between oxygens in a carboxylate
        # allowed
        OO_buffer = float(param_dict['OO_buffer'][0])
        # angstroms to consider in Z range for configuration search
        # consider maximal Z distance between atoms in your configuration to
        # test the buffer for substrates (Cu(OH)2 for example) is automatically
        # set to 0.8
        buffer = float(param_dict['buffer'][0])
        # set lattice rules
        max_area_ratio_tol = float(param_dict['max_area_ratio_tol'][0])
        max_length_tol = float(param_dict['max_length_tol'][0])
        max_angle_tol = float(param_dict['max_angle_tol'][0])
        # maximum area multiple to test
        f_max_area_multi = float(param_dict['f_max_area_multi'][0])
        s_max_area_multi = float(param_dict['s_max_area_multi'][0])

        sub_structures, s_millers = substrates[surface]

        ##################
        # get tolerance from substrate binding atom and film
        # binding atom ionic radii
        film_radii = float(param_dict['film_radii'][0])
        sub_radii = float(param_dict['sub_radii'][0])
        # use Lorentz mixing rules for ionic radii
        # ij = (ii + jj) / 2
        # use radius!
        tolerance = (film_radii + sub_radii) / 2

        film_results = []
        film_mg_struct = mg.Structure.from_file(film+".cif").get_primitive_structure()
        # produce all possible slabs of film
        all_slabs = generate_all_slabs(film_mg_struct,
                                       max_index=1,
                                       min_slab_size=0.5,
                                       min_vacuum_size=0.0,
                                       primitive=False)
        print("-------------------")
        print('building film slabs...')
        check_complete = param_dict['check_complete'][0]
        if check_complete == 'T':
            check_complete = True
        else:
            check_complete = False
        film_structures, f_millers = bp.get_binding_structures(film,
                                                               all_slabs,
                                                               film_metal,
                                                               buffer,
                                                               XY_angle,
                                                               OO_buffer,
                                                               check_complete)
        print('done')
        print("-------------------")

        # remove duplicates in f_millers
        f_millers = set(f_millers)

        # iterate over all found configurations for this miller index then run
        # calculation!
        # film miller indices for which the ASO calculation has
        # been undertaken. We note that the pymatgen code used
        # creates multiple slabs for each miller with a
        # shifting factor (see get_slabs() function in
        # surface.py) our code only needs one version of the
        # slab as it checks for chemical configurations
        # throughout the slab cell (in the C direction)
        # as of 16/1/18 - where we want to add testing of multiple
        # configurations per slab, we will use a dictionary of
        # miller - config count pairs to determine if a config has
        # been tested
        millers_configs_tested = {}

        film_results = []
        for mill_idx in f_millers:
            print("-------------------")
            miller_name = str(mill_idx[0])+str(mill_idx[1])+str(mill_idx[2])
            # ## REMOVE THIS
            # if miller_name != '11-1':
            #     continue
            print('doing film hkl =', miller_name)
            config_counts = []
            for config_key in film_structures.keys():
                if config_key[0] == miller_name:
                    config_counts.append(int(config_key[1]))
            # iterate over config counts
            for config in config_counts:
                print('doing film config =', str(config))
                # skip if we have already tested these
                # miller indices and configuration
                try:
                    if millers_configs_tested[miller_name] == config:
                        continue
                except KeyError:
                    pass
                millers_configs_tested[miller_name] = config
                f_prefix = film+"_"+miller_name+"_"+str(config)
                film_out_cif_name = f_prefix+"_bind_plane.cif"
                film_bonds_name = f_prefix+"_bind_bonds.txt"
                for s_mill in s_millers:
                    sub_miller = str(s_mill[0])+str(s_mill[1])+str(s_mill[2])
                    s_prefix = surface+"_"+sub_miller
                    sub_out_cif_name = s_prefix+"_surf_out.cif"
                    # read in film and substrate slab with pymatgen
                    # to obtain slab area and cell vectors
                    # for Zurr and McGill algorithm.
                    # these vectors are not reduced, but I found that with
                    # the reduced vectors (the commented out code) the results
                    # were equivalent for the parameterisation data set.
                    # I have seen it applied differently in different codes.
                    # I generally would recommend using the reduced UC vectors
                    # as that follows the Zur algorithm and majority of the
                    # existing code (especially the PYMATGEN code used in
                    # lattice.py).
                    film_slab = mg.Structure.from_file(film_out_cif_name)
                    rpf1, rpf2 = [film_slab.lattice.matrix[0],
                                  film_slab.lattice.matrix[1]]
                    # rpf1, rpf2 = pmgsa_AT.reduce_vectors(
                    #         film_slab.lattice.matrix[0],
                    #         film_slab.lattice.matrix[1])
                    sub_slab = mg.Structure.from_file(sub_out_cif_name)
                    rps1, rps2 = [sub_slab.lattice.matrix[0],
                                  sub_slab.lattice.matrix[1]]
                    # rps1, rps2 = pmgsa_AT.reduce_vectors(
                    #          sub_slab.lattice.matrix[0],
                    #          sub_slab.lattice.matrix[1])
                    f_area = pmgsa_AT.vec_area(rpf1, rpf2)
                    s_area = pmgsa_AT.vec_area(rps1, rps2)

                    # the max_area to be used in Zur algorithm
                    # is defined here by the smallest area of either the max
                    # multiplier * the film unit-cell area or the sub unit cell
                    # area
                    # Note: it is often defined by the substrate
                    smallest_max = min([f_max_area_multi*round(f_area, 3),
                                        s_max_area_multi*round(s_area, 3)])
                    print("-------------------")
                    print("film area:", round(f_area, 3),
                          "substrate area:", round(s_area, 3))
                    print("film multi max:", f_max_area_multi,
                          "substrate multi max:", s_max_area_multi)
                    print("max allowed area:", round(smallest_max, 3))
                    print("-------------------")
                    print('getting matches...')
                    # define pymatgen ZSL class
                    ZSL = pmgsa_AT.ZSLGenerator(
                                    max_angle_tol=max_angle_tol,
                                    max_area=smallest_max,
                                    max_area_ratio_tol=max_area_ratio_tol,
                                    max_length_tol=max_length_tol
                                    )
                    # generate all possible sl transformation sets
                    # produces a generator of arrays
                    # each item in the array is tuple (i, j)
                    # where i is an array of Zur matrices possible for a given
                    # integer multiple of the film unit cell and j is an array
                    # of Zur matrices possible for the corresponding integer
                    # integer multiple of the substrate unit cell
                    trans_sets = ZSL.generate_sl_transformation_sets(
                            film_area=round(f_area, 3),
                            substrate_area=round(s_area, 3))

                    matches_done = []
                    matches = ZSL.get_equiv_transformations_wZM(
                            transformation_sets=trans_sets,
                            film_vectors=[rpf1, rpf2],
                            substrate_vectors=[rps1, rps2])
                    print('done')
                    # iterate over and check all produced
                    # transformations
                    print("-------------------")
                    print("test all matches...")
                    print("substrate -- hkl -- film -- hkl",
                          "-- config -- film SC match area --",
                          " multiple of film UC area --",
                          " replicated multiple of film UC area --",
                          " no. sub atoms -- no. film atoms")

                    ASO_match_output_file = s_prefix+"_"+f_prefix+"_ASO_match.data"
                    with open(ASO_match_output_file, 'w') as f:
                        f.write('sub_area,sub_rel.area,sub_new_rel.area,')
                        f.write('area,rel.area,new_area,new_rel.area,')
                        f.write('cell_area,cell_rel.area,aso,sa,sb,')
                        f.write('trials,steps,time(s),')
                        f.write('max_MM,avg_MM,')
                        f.write('f1,f2,f3,f4,s1,s2,s3,s4,')
                        f.write('fr1,fr2,fr3,fr4,sr1,sr2,sr3,sr4,')
                        f.write('m1,m2,bonds_list,translated')
                        f.write('\n')

                    miller_results = []
                    COP_sets = []
                    for match in matches:
                        match_start_time = time.time()
                        m = ZSL.match_as_dict(
                                    film_sl_vectors=match[0],
                                    substrate_sl_vectors=match[1],
                                    film_vectors=[rpf1, rpf2],
                                    substrate_vectors=[rps1, rps2],
                                    match_area=pmgsa_AT.vec_area(*match[0]))

                        # match = match rotation matrix [(x1 ,x2) , (x3, x4)]
                        # note that the match will correspond to the transform
                        # matrix pre reduction + replication
                        fmatch_1, fmatch_2, fmatch_3, fmatch_4 = match[2].reshape(4, )
                        smatch_1, smatch_2, smatch_3, smatch_4 = match[3].reshape(4, )

                        # calculate required multiples and
                        # apply replication algorithm
                        multi_1, multi_2 = ASO.get_replication_vect(rpf1, rpf2,
                                                                    match)
                        XY = np.asarray([(multi_1, multi_2)])

                        # mulltiply supercell vectors by X and Y
                        new_f_match = XY.reshape(2, 1) * np.array(match[0])
                        new_s_match = XY.reshape(2, 1) * np.array(match[1])
                        new_Zf_match = XY.reshape(2, 1) * np.array(match[2])
                        new_Zs_match = XY.reshape(2, 1) * np.array(match[3])

                        new_m = ZSL.match_as_dict(
                                film_sl_vectors=new_f_match,
                                substrate_sl_vectors=new_s_match,
                                film_vectors=[rpf1, rpf2],
                                substrate_vectors=[rps1, rps2],
                                match_area=pmgsa_AT.vec_area(*new_f_match))
                        new_match_area = new_m['match_area']

                        # check if this match dictionary has been tested before
                        match_done = ASO.check_match_duplicates(matches_done,
                                                                new_m)
                        if match_done is True:
                            continue
                        else:
                            matches_done.append(new_m)

                        print("-------------------------------------------")
                        print("match info:")
                        print(fmatch_1, fmatch_2, fmatch_3, fmatch_4)
                        print(smatch_1, smatch_2, smatch_3, smatch_4)
                        print(match[0][0])
                        print(match[0][1])
                        print(match[1][0])
                        print(match[1][1])
                        print(new_f_match)
                        print(new_s_match)
                        print(XY)
                        print("-------------------------------------------")

                        bps = ASO.read_in_binding_planes(sub_out_cif_name,
                                                         film_out_cif_name,
                                                         new_m, verbose=False)
                        substrate, mat2d, sub_latt_a, sub_latt_b, film_latt_a, film_latt_b, map_f, map_s = bps
                        frot_1, frot_2 = map_f[0][0], map_f[0][1]
                        frot_3, frot_4 = map_f[1][0], map_f[1][1]
                        srot_1, srot_2 = map_s[0][0], map_s[0][1]
                        srot_3, srot_4 = map_s[1][0], map_s[1][1]
                        if substrate is None and mat2d is None:
                            print("no matching super lattice found..")
                            print("therefore, this match has been skipped!")
                            continue

                        # get angle and direction of rotation necessary to
                        # make a vector (shortest before replication)
                        # of film SC parallel with a vector (shortest before
                        # replication) of substrate SC
                        R_fa_sa, fa_sa_angle = ASO.get_supercell_rot_matrix(
                                                        sub_latt_a,
                                                        film_latt_a,
                                                        verbose=False)

                        # do rotation into substrate coordinate system
                        # rotate super cell vectors
                        rot_cells = ASO.rotate_to_substrate_system(
                                                        sub_latt_a,
                                                        sub_latt_b,
                                                        R_fa_sa,
                                                        film_latt_a,
                                                        film_latt_b,
                                                        verbose=False)
                        r_film_latt_a, r_film_latt_b, r_sub_latt_a, r_sub_latt_b = rot_cells

                        # determine if extra rotations are necessary and
                        # and determine if translations can be applied
                        skip, apply_translation = ASO.determine_extra_rotations(
                                                r_sub_latt_a, r_sub_latt_b,
                                                r_film_latt_a, r_film_latt_b,
                                                max_angle_tol,
                                                verbose=False)
                        # transfer substrate atoms to ASE structure
                        s_str = ASO.get_substrate_ASE(substrate, r_sub_latt_a,
                                                      r_sub_latt_b)

                        # rotate atoms and output into ASE structure
                        f_str = ASO.get_film_ASE(mat2d, r_film_latt_a,
                                                 r_film_latt_b, R_fa_sa,
                                                 apply_translation)

                        # wrap atoms into their respective supercells
                        f_str.wrap()
                        s_str.wrap()

                        ########################################
                        # debugging functions
                        ASO.plot_replication(match, new_f_match, rpf1, rpf2,
                                             plot=False)
                        ASO.plot_transitions(match, new_f_match, new_s_match,
                                             film_latt_a, film_latt_b,
                                             sub_latt_a, sub_latt_b,
                                             r_film_latt_a, r_film_latt_b,
                                             r_sub_latt_a, r_sub_latt_b,
                                             substrate, mat2d, R_fa_sa,
                                             plot=False)

                        ASO.visualise_all_cells(f_str, s_str, viz=False)
                        ########################################

                        # if input('interested in MC?') is not 't':
                        #     continue

                        if skip is True:
                            print('skipped')
                            continue

                        # Sb must be number of film atoms
                        Sa = len(s_str)
                        Sb = len(f_str)
                        # make sure all atoms have Z = 0
                        for atom in f_str:
                            atom.z = 0
                        for atom in s_str:
                            atom.z = 0
                        f_cell_vectors = [f_str.cell[0, :],
                                          f_str.cell[1, :]]
                        s_cell_vectors = [s_str.cell[0, :],
                                          s_str.cell[1, :]]
                        cell_match_area = pmgsa_AT.vec_area(*f_cell_vectors)

                        # get new mismatch data
                        mismatch_dict = pmgsa_AT.calculate_mismatches(
                                film_sl_vect=f_cell_vectors,
                                sub_sl_vect=s_cell_vectors,
                                sub_UC_area=s_area,
                                film_UC_area=f_area,
                                max_length_tol=max_length_tol)
                        max_mismatch = max(
                                [mismatch_dict['v1_length'],
                                 mismatch_dict['v2_length'],
                                 mismatch_dict['angle_mismatch'],
                                 mismatch_dict['area_rat_mismatch']])
                        avg_mismatch = np.average(
                                [mismatch_dict['v1_length'],
                                 mismatch_dict['v2_length'],
                                 mismatch_dict['angle_mismatch'],
                                 mismatch_dict['area_rat_mismatch']])

                        # collect the number of bonds lost to build the
                        # interface
                        bonds_list_frame = pd.read_csv(film_bonds_name)
                        interface_bonds_lost = len(
                                bonds_list_frame['atom1_id'])

                        # MC ALGORITHM
                        print("===", surf, '--', sub_miller,
                              '--', film, '--', miller_name,
                              '--', str(config), '--',
                              '{0:.2f}'.format(m['match_area']), '--',
                              '{0:.2f}'.format(m['match_area']/f_area), '--',
                              '{0:.2f}'.format(new_match_area/f_area),
                              '--', str(Sa), '--', str(Sb),
                              "===")
                        if Sb > 0:
                            # write com transform array produces an array of
                            # starting positions for the MC algorithm that get
                            # chosen from at random.
                            x_moves = np.arange(-5.0, 5.1, 0.5)
                            y_moves = np.arange(-5.0, 5.1, 0.5)
                            indices = []
                            results = []
                            MMs = []
                            out_f = []
                            out_s = []
                            for trial in np.arange(trials[0]):
                                MC_res = MC.ASO_MC_alg(steps, x_moves, y_moves,
                                                       f_str, s_str, Sa, Sb,
                                                       beta, tolerance,
                                                       resolution,
                                                       f_cell_vectors)
                                t_res, m_scores, out_f_str, out_s_str, x_disps, y_disps = MC_res
                                MMs.append(m_scores)
                                out_f.append(out_f_str.copy())
                                out_s.append(out_s_str.copy())
                            max_max = max(MMs)
                            mm_indices = [i for i, x in enumerate(MMs)
                                          if x == max_max]
                            if max_max > 0:
                                # this currently selects the first max ASO
                                # option. This is for a single set of MC
                                # runs and therefore all of the options
                                # here should be the same interface.
                                xx = mm_indices[0]
                                RES = [str(surf), str(sub_miller),
                                       str(film), str(miller_name),
                                       str(config), str(max_max),
                                       str(round(m['match_area'], 3)),
                                       str(round(m['match_area']/f_area, 3)),
                                       str(round(new_match_area, 3)),
                                       str(round(new_match_area/f_area, 3)),
                                       str(round(cell_match_area, 3)),
                                       str(round(cell_match_area/f_area, 3)),
                                       str(round(max_mismatch, 3)),
                                       str(round(avg_mismatch, 3)),
                                       str(Sa), str(Sb)]
                                film_results.append([m, RES, out_f[xx],
                                                     out_s[xx]])
                                # save the data for this interface to
                                # the substrare-hkl-film-hkl-config file.
                                ASO_match_output_file = s_prefix+"_"+f_prefix+"_ASO_match.data"
                                ASO_match_data = [
                                        round(s_area, 3),
                                        round(m['match_area']/s_area, 3),
                                        round(new_match_area/s_area, 3),
                                        round(m['match_area'], 3),
                                        round(m['match_area']/f_area, 3),
                                        round(new_match_area, 3),
                                        round(new_match_area/f_area, 3),
                                        round(cell_match_area, 3),
                                        round(cell_match_area/f_area, 3),
                                        max_max, Sa, Sb, trials[0], steps,
                                        round(time.time()-match_start_time, 5),
                                        round(max_mismatch, 3),
                                        round(avg_mismatch, 3),
                                        fmatch_1, fmatch_2, fmatch_3, fmatch_4,
                                        smatch_1, smatch_2, smatch_3, smatch_4,
                                        frot_1, frot_2, frot_3, frot_4,
                                        srot_1, srot_2, srot_3, srot_4,
                                        multi_1, multi_2, interface_bonds_lost,
                                        apply_translation
                                        ]
                                aoo.ASO_match_line(ASO_match_output_file,
                                                   ASO_match_data)

        # output ASO data for specific film-hkl-substrate-hkl #

        # we have already saved the data for every match to
        # "ASO_match_output_file"

        # we want to save the interface associated with the max
        # ASO and minimum match area as a CIF file
        aoo.get_film_interface_structure_min_area(film_results,
                                                  surf,
                                                  film)

        # we want to save the interface associated with the max
        # ASO and minimum mismatch as a CIF file
        aoo.get_film_interface_structure_min_MM(film_results,
                                                surf,
                                                film)

        # output ASO data for this film-substrate #
        # plot ASO vs Area for all miller planes
        aoo.plt_aso_vs_area_per_film(film_results, film)
        aoo.plt_aso_vs_MM_per_film(film_results, film)

        # end of ASO
        status = 5
        print("=========== Completed ASO test ===========")
        end_time = time.time()
        print("=== time taken for ASO test was:",
              "{0:.2f}".format(end_time-ASO_start_time),
              "s ===")

    # update status in CIF_TODO
    row['status'] = status
    CIF_TODO.loc[index] = row
    # update file
    # use index=False to avoid adding column everytime you save the file
    CIF_TODO.to_csv(CIF_TODO_file, index=False)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
