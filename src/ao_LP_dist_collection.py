#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses the unit cell angles of MOF binding planes in all MOF databases.

Produces distribution figure in the supproting information.
"""

import time
import glob
import numpy as np
import os
import argparse
from ase.io import read
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import general_functions as genf
import analysis_output_functions as aoo
import ao_plot_fn as aoplt

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

curr_dir = os.getcwd()

# set standard plot values for interpen and non-interpen case
standard_plot_values = {'i':
                        {'s': 80, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 1.0, 'marker': 'o'}}

plt_val = standard_plot_values['i']

# define figures
fig = plt.figure()
# define sub plots of figure
ax1 = plt.subplot()

# define colour map
# set mid point based on DIB threshold of 0.67
DIB_thresh = 0.67

top_candidate_angles = []
other_angles = []

# hard code database dirs for this one.
DB_dirs = [
        '/home/atarzia/epit-mof/database/CIFs/CORE_full_db/max_results_11_07_18',
        '/home/atarzia/epit-mof/database/CIFs/hmof_full_db/max_results_11_07_18',
        '/home/atarzia/epit-mof/database/CIFs/tobacco/CuOH2/max_results_09_07_18']

for DB in DB_dirs:
    os.chdir(DB)
    # read in parameter dictionary
    param_file = args.paramfile[0]
    param_dict = genf.read_parameter_file(param_file)

    # output/result CSV name
    result_file = param_dict['output_csv_name'][0]

    # surface
    surface = param_dict['surfaces'][0]
    surface_hkl = param_dict['surf_millers'][0]

    # separate results into two files
    res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

    # list of interpenetrated CIFs
    interpen_films = []
    films = list(set(per_MOF_data['film']))
    for i in films:
        if 'i_'+i+'_0' in films:
            interpen_films.append(i)

    for idx, row in per_MOF_data.iterrows():
        film = row['film']
        print('film:', film)
        # want to ignore interpenetrated versions of CIFs
        if film in interpen_films:
            continue
        # check if any data was collected
        if row['1_max_DIB'] == 'None':
            continue
        # assign DIB values
        else:
            DIB1 = float(row['1_max_DIB'])
            if row['2_max_DIB'] == 'None':
                DIB2 = 0
            else:
                DIB2 = float(row['2_max_DIB'])

        # top candidate?
        if DIB1 >= DIB_thresh:
            if DIB2 >= DIB_thresh:
                alpha = 0.4
            else:
                print('top candidate:', DIB1)
                alpha = 1.0
        else:
            alpha = 0.4

        hkl = str(row['1_f_hkl'])
        y = DIB1
        y2 = DIB2
        config = str(row['1_config'])
        # get unit cell angles from binding plane
        BP_name = film+"_"+hkl+"_"+config+"_bind_plane.cif"

        if os.path.isfile(BP_name) is True:
            cif = read(BP_name)
            cell_angle = cif.get_cell_lengths_and_angles()[5]
        else:
            continue
        x = cell_angle

        if DIB1 >= DIB_thresh and DIB2 < DIB_thresh:
            top_candidate_angles.append(x)
        else:
            other_angles.append(x)

        # get all other LPs of binding planes not considered the max DIB and
        # add to lo_dib_angles - because they are not top candidate binding
        # planes
        other_BP = glob.glob(film+"*_bind_plane.cif")
        for BP in other_BP:
            if BP != BP_name:
                cif = read(BP)
                cell_angle = cif.get_cell_lengths_and_angles()[5]
                other_angles.append(cell_angle)

# plot distribution figure
ax1.hist(other_angles, bins=range(50, 130 + 5, 2),
         alpha=0.4, density=True, histtype='stepfilled',
         label='not aligned',
         color='r')
ax1.hist(top_candidate_angles, bins=range(50, 130 + 5, 2),
         alpha=0.4, density=True, histtype='stepfilled',
         label='aligned',
         color='b')

ax1.legend(loc=2, fancybox=True, ncol=1, fontsize=16)

aoplt.define_hist_variables(ax1,
                            title='',
                            ytitle='frequency',
                            xtitle="in-plane unit cell angle [$\degree$]",
                            xlim=(50, 130),
                            ylim=(0, 0.4))
ax1.set_xticks([60, 75, 90, 105, 120])

# plot inset
left, bottom, width, height = [0.6, 0.6, 0.4, 0.4]
ax1ins = fig.add_axes([left, bottom, width, height])
ax1ins.hist(other_angles, bins=range(0, 180 + 5, 2),
            alpha=0.4, density=True, histtype='stepfilled',
            label='not aligned',
            color='r')
ax1ins.hist(top_candidate_angles, bins=range(0, 180 + 5, 2),
            alpha=0.4, density=True, histtype='stepfilled',
            label='aligned',
            color='b')
aoplt.define_hist_variables(ax1ins,
                            title='',
                            ytitle='frequency',
                            xtitle="in-plane unit cell angle [$\degree$]",
                            xlim=(0, 180),
                            ylim=(0, 0.4))
ax1ins.set_xticks([0, 45, 90, 135, 180])

##############################################################################
# save fig
os.chdir(curr_dir)
fig.tight_layout()
fig.savefig("LP_all_DB_cf.pdf",
            bbox_inches='tight', dpi=720)


end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")

import sys
sys.exit()

for DB in DB_dirs:
    os.chdir(DB)
    # read in parameter dictionary
    param_file = args.paramfile[0]
    param_dict = genf.read_parameter_file(param_file)

    # output/result CSV name
    result_file = param_dict['output_csv_name'][0]

    # read in CIF TODO list as dataframe
    # read in CIF TODO list as dataframe
    CIF_TODO_files_step_1 = glob.glob(param_dict['TODO_name'][0]+"_step_1_*.csv")
    CIF_TODO_files_step_3 = glob.glob(param_dict['TODO_name'][0]+"_step_3_*.csv")

    # surface
    surface = param_dict['surfaces'][0]
    surface_hkl = param_dict['surf_millers'][0]
    # get the MOF list from all CIF_TODO files
    # move MOFs into status dict
    print("=================================================================")
    print("Step 3:")
    status_dict_step_3 = aoo.get_status_dict_step_3(CIF_TODO_files_step_3)

    ASO_completed_films = sorted(status_dict_step_3['5'])
    # list of interpenetrated CIFs
    interpen_films = []
    for i in ASO_completed_films:
        if 'i_'+i+'_0' in ASO_completed_films:
            interpen_films.append(i)

    max_angle_tol = float(param_dict['max_angle_tol'][0])
    # get tolerance from substrate binding atom and film
    # binding atom ionic radii
    film_radii = float(param_dict['film_radii'][0])
    sub_radii = float(param_dict['sub_radii'][0])
    # use Lorentz mixing rules for ionic radii
    # ij = (ii + jj) / 2
    # use radius!
    tolerance = (film_radii + sub_radii) / 2
    print("binding tolerance =", tolerance, "Angstrom")
    # define the tolerance from the distance between copper sites in Cu(OH)2
    # a distance of 0.5 Ansgtrom leads to an angle of rotation of the Cu
    # atoms of about 10 degrees
    tol_u = 0.5
    print("uniqueness tolerance =", tol_u, "Angstrom")
    print("uniqueness tolerance =",
          np.degrees(2 * np.arcsin(tol_u / (2 * 2.947))), "degrees")
    for film in ASO_completed_films:
#        if film not in ['i_NEJRUR_clean_0', 'i_NEJRUR_clean_0', 'i_CEHPIP_clean_0', 'i_NEJSOM_clean_0']:
#            continue
        print('film:', film)
        ASO_files = [i for i in glob.glob("*"+film+"*ASO*data") if "_"+film+"_" in i]
        print(ASO_files)
        if len(ASO_files) == 0:
            print("no Miller planes with appropriate binding sites were found")
            continue
        # want to ignore interpenetrated versions of CIFs
        if film in interpen_films:
            continue
        prefix = surface+"_"+surface_hkl+"_"+film+"_"
        print("----------------------------------------------------------")
        print("doing:", film)
        res_dict = aoo.collect_film_results(ASO_files, film,
                                            surface, surface_hkl,
                                            tol_u, max_angle_tol, prefix)
        if res_dict == 0:
            continue
        film_max_dib = res_dict['DIB1']
        film_2nd_dib = res_dict['DIB2']
        film_max_dib_int = res_dict['frame1']
        film_2nd_dib_int = res_dict['frame2']
        film_max_ASO = res_dict['ASO']
        film_MCIA = res_dict['MCIA']
        film_max_hkl = res_dict['f_m_hkl']
        film_max_config = res_dict['f_m_config']
        film_max_ASO_hkl = res_dict['f_m_ASO_hkl']
        film_MCIA_hkl = res_dict['f_MCIA_hkl']

        top_match_area = film_max_dib_int['area']
        top_film_uc_area = top_match_area / film_max_dib_int['rel.area']
        top_sub_uc_area = film_max_dib_int['sub_area']
        top_uc_ratio = top_film_uc_area / top_sub_uc_area
        top_rel_match_area = film_max_dib_int['rel.area']
        top_ASO = film_max_dib_int['aso']

        if film_2nd_dib_int is not None:
            sec_match_area = film_2nd_dib_int['area']
            sec_film_uc_area = sec_match_area / film_2nd_dib_int['rel.area']
            sec_sub_uc_area = film_2nd_dib_int['sub_area']
            sec_uc_ratio = sec_film_uc_area / sec_sub_uc_area
            sec_rel_match_area = film_2nd_dib_int['rel.area']
            sec_ASO = film_2nd_dib_int['aso']
        # apply scoring functions
        # function 1
        # max DIB - second best DIB
        # score_function_1 = film_max_dib - film_2nd_dib
        if film_max_dib >= DIB_thresh:
            if film_2nd_dib >= DIB_thresh:
                alpha = 0.4
            else:
                print('top candidate:', film_max_dib)
                alpha = 1.0
        else:
            alpha = 0.4

        # collect only h+k+l = 1 cases
        hkl = film_max_hkl
        config = film_max_config
        y = film_max_dib
        y2 = film_2nd_dib
        # get unit cell angles from binding plane
        BP_name = film+"_"+hkl+"_"+config+"_bind_plane.cif"

        # set plot values
        if film[:2] == 'i_':
            plt_val = standard_plot_values['i']
            no = film.split("_")[-1]
            short_name = film.replace('i_', '').replace("_"+no, '')
        else:
            plt_val = standard_plot_values['i']
            short_name = film

        if os.path.isfile(BP_name) is True:
            cif = read(BP_name)
            cell_angle = cif.get_cell_lengths_and_angles()[5]
        x = cell_angle

        if film_max_dib >= DIB_thresh and film_2nd_dib < DIB_thresh:
            top_candidate_angles.append(x)
        else:
            other_angles.append(x)
        # get all other LPs of binding planes not considered the max DIB and
        # add to lo_dib_angles - because they are not top candidate binding
        # planes
        other_BP = glob.glob(film+"*_bind_plane.cif")
        for BP in other_BP:
            if BP != BP_name:
                cif = read(BP)
                cell_angle = cif.get_cell_lengths_and_angles()[5]
                other_angles.append(cell_angle)

# plot distribution figure
ax1.hist(other_angles, bins=range(50, 130 + 5, 2),
         alpha=0.4, density=True, histtype='stepfilled',
         label='not aligned',
         color='r')
ax1.hist(top_candidate_angles, bins=range(50, 130 + 5, 2),
         alpha=0.4, density=True, histtype='stepfilled',
         label='aligned',
         color='b')

ax1.legend(loc=2, fancybox=True, ncol=1, fontsize=16)

aoplt.define_hist_variables(ax1,
                            title='',
                            ytitle='frequency',
                            xtitle="in-plane unit cell angle [$\degree$]",
                            xlim=(50, 130),
                            ylim=(0, 0.4))
ax1.set_xticks([60, 75, 90, 105, 120])

# plot inset
left, bottom, width, height = [0.6, 0.6, 0.4, 0.4]
ax1ins = fig.add_axes([left, bottom, width, height])
ax1ins.hist(other_angles, bins=range(0, 180 + 5, 2),
            alpha=0.4, density=True, histtype='stepfilled',
            label='not aligned',
            color='r')
ax1ins.hist(top_candidate_angles, bins=range(0, 180 + 5, 2),
            alpha=0.4, density=True, histtype='stepfilled',
            label='aligned',
            color='b')
aoplt.define_hist_variables(ax1ins,
                            title='',
                            ytitle='frequency',
                            xtitle="in-plane unit cell angle [$\degree$]",
                            xlim=(0, 180),
                            ylim=(0, 0.4))
ax1ins.set_xticks([0, 45, 90, 135, 180])

##############################################################################
# save fig
os.chdir(curr_dir)
fig.tight_layout()
fig.savefig("LP_all_DB_cf.pdf",
            bbox_inches='tight', dpi=720)


end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")

import sys
sys.exit()