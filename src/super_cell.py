#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used in to map unit cells into supercells.

This code was modified from/inspired by code in the MPInterfaces repo:
    https://github.com/henniggroup/MPInterfaces

Specifically code found in:
https://github.com/henniggroup/MPInterfaces/blob/master/mpinterfaces/transformations.py
"""

import numpy as np
import pymatgen as mg

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def get_opt_mapping(latt, other_lattice, ltol=1e-5, atol=1,
                    skip_rotation_matrix=False,
                    rotation_matrix=np.eye(N=3)):
    """
    This function is a variation of "find_mapping" in pymatgen.core.lattice.
    This version outputs a list of mappings such that we can test all possible
    mappings and optimize that choice. self.matrix is replaced with the input
    variable latt.

    Finds a mapping between current lattice and another lattice. There
    are an infinite number of choices of basis vectors for two entirely
    equivalent lattices. This method returns a mapping that maps
    other_lattice to this lattice.

    On 21/2/18:
        This was causing large memory issues by collating all of the matches
        and since we changed the mapping optimiser code. We can merge the two
        functions without the need for collating all of the mapping options.

    Args:
        other_lattice (Lattice): Another lattice that is equivalent to
            this one.
        am (Lattice): Aligned Matrix from previous run
        rm (Matrix): Rotation matrix from previous %run
        ltol (float): Tolerance for matching lengths. Defaults to 1e-5.
        atol (float): Tolerance for matching angles. Defaults to 1.

    Returns:
        (aligned_lattice, rotation_matrix, scale_matrix) if a mapping is
        found. aligned_lattice is a rotated version of other_lattice that
        has the same lattice parameters, but which is aligned in the
        coordinate system of this lattice so that translational points
        match up in 3D. rotation_matrix is the rotation that has to be
        applied to other_lattice to obtain aligned_lattice, i.e.,
        aligned_matrix = np.inner(other_lattice, rotation_matrix) and
        op = SymmOp.from_rotation_and_translation(rotation_matrix)
        aligned_matrix = op.operate_multi(latt.matrix)
        Finally, scale_matrix is the integer matrix that expresses
        aligned_matrix as a linear combination of this
        lattice, i.e., aligned_matrix = np.dot(scale_matrix, self.matrix)

        None is returned if no matches are found.
    """
    for x in latt.find_all_mappings(
            other_lattice, ltol, atol,
            skip_rotation_matrix=skip_rotation_matrix):
        # if rotation matrix is np.eye then accept
        if np.allclose(x[1], rotation_matrix, rtol=0, atol=0.01):
            opt_mapping = x
            return opt_mapping


def align_lattices(slab_sub, slab_film, uv_substrate, uv_film,
                   rotation_matrix=np.eye(N=3)):
    """Maps film and substrate unit cells to prescribed super cells.

    This no longer does the alignment step!

    This is a modified version of the function get_aligned_lattices in the
    MPInterfaces source (https://github.com/henniggroup/MPInterfaces)
    authored by Kiran Mathew, Arunima Singh.

    modified 14/9/17:
        added optimize_lattice_choice_v2 code to make the super-lattice from
        find_all_mappings choice better (minimizes chance of needing to flip
        and minimizes differences in match_area) as the original code just
        found the first option from pymatgen functon (find_all_mappings)
    """
    substrate_latt = mg.Lattice(np.array(
        [
            uv_substrate[0][:],
            uv_substrate[1][:],
            slab_sub.lattice.matrix[2, :]
        ]))
    film_latt = mg.Lattice(np.array(
        [
            uv_film[0][:],
            uv_film[1][:],
            slab_film.lattice.matrix[2, :]
        ]))

    # rotation matrices are not applied to substrate super-lattice vectors
    opt1 = get_opt_mapping(slab_sub.lattice, substrate_latt)
    opt2 = get_opt_mapping(slab_film.lattice, film_latt,
                           rotation_matrix=rotation_matrix)

    if opt1 is None or opt2 is None:
        return None, None, None, None

    am1, rm1, scell1 = opt1
    am2, rm2, scell2 = opt2
    # print(substrate_latt)
    # print(am1)
    # print(film_latt)
    # print(am2)

    slab_sub.make_supercell(scell1)
    slab_film.make_supercell(scell2)

    return slab_sub, slab_film, scell1, scell2
