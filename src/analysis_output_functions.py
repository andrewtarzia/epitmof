#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used for analysis, output and plotting of screening results.
"""

import pandas as pd
import numpy as np
import glob
import os
from scipy.spatial import distance
import pymatgen as mg
from ase.visualize import view
from ase.io.png import write_png
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import pmg_substrate_analyzer as pmgsa_AT
import ASO_functions as ASO

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def plt_aso_vs_area_per_film(film_results, film):
    """
    Plot the ASO for all miller planes and all interfaces of a given film
    as a function of RELATIVE MATCH AREA of the interface after replication.
    """
    fig, ax = plt.subplots()
    max_area = 0
    f_millers = list(set([i[1][3] for i in film_results]))
    s_millers = list(set([i[1][1] for i in film_results]))
    for s_m in s_millers:
        for f_m in f_millers:
            configs = list(set([i[1][4] for i in film_results
                                if i[1][3] == f_m and i[1][1] == s_m]))
            for c in configs:
                asos = [float(i[1][5]) for i in film_results
                        if i[1][3] == f_m and i[1][1] == s_m and i[1][4] == c]
                areas = [float(i[1][9]) for i in film_results
                         if i[1][3] == f_m and i[1][1] == s_m and i[1][4] == c]
                if max(areas) > max_area:
                    max_area = max(areas)
                ax.scatter(areas, asos,
                           label=str(s_m)+" : "+str(f_m)+" : "+str(c),
                           edgecolors='none',
                           alpha=0.5)
    ax.set_title(film)
    # plt.xlabel('match area [$\mathrm{\AA}^2$]')
    ax.set_xlabel('rel. match area')
    ax.set_ylabel('ASO')
    ax.set_ylim(0, 1)
    ax.set_xlim(0, max_area+5)
    ax.legend(loc=1)
    fig.savefig(film+"_ASO_vs_area.pdf", bbox_inches='tight')
    plt.close()


def plt_aso_vs_MM_per_film(film_results, film):
    """
    Plot the ASO for all miller planes and all interfaces of a given film
    as a function of MAXIMUM MISMATCH of the interface after replication.
    """
    fig, ax = plt.subplots()
    f_millers = list(set([i[1][3] for i in film_results]))
    s_millers = list(set([i[1][1] for i in film_results]))
    for s_m in s_millers:
        for f_m in f_millers:
            configs = list(set([i[1][4] for i in film_results
                                if i[1][3] == f_m and i[1][1] == s_m]))
            for c in configs:
                asos = [float(i[1][5]) for i in film_results
                        if i[1][3] == f_m and i[1][1] == s_m and i[1][4] == c]
                MMs = [float(i[1][12]) for i in film_results
                       if i[1][3] == f_m and i[1][1] == s_m and i[1][4] == c]
                ax.scatter(np.asarray(MMs)*100, asos,
                           label=str(s_m)+" : "+str(f_m)+" : "+str(c),
                           edgecolors='none',
                           alpha=0.5)
    ax.set_title(film)
    ax.set_xlabel('maximum mismatch [%]')
    ax.set_ylabel('ASO')
    ax.set_ylim(0, 1)
    ax.set_xlim(0, 15)
    ax.legend(loc=1)
    fig.savefig(film+"_ASO_vs_MM.pdf", bbox_inches='tight')
    plt.close()


def get_status_dict_step_1(CIF_TODO_files):
    """
    Read CIF_TODO files into status dict and print status information.
    """
    status_dict = {'0': [],
                   '1': [],
                   '2': [],
                   '3': [],
                   '4': [],
                   '4a': [],
                   '5': []}

    process_dict = {}

    count_total = 0
    for file in CIF_TODO_files:
        cif_file_progress = 0
        cif_file_total = 0
        NP = str(file.replace(".csv", "").split("_")[-1])
        if os.path.isfile(file) is True:
            data = pd.read_csv(file)
            for index, row in data.iterrows():
                stat = str(row['status'])
                film = row['film name']
                status_dict[stat].append(film)
                count_total += 1
                if stat != '0':
                    cif_file_progress += 1
                cif_file_total += 1
            process_dict[NP] = (cif_file_progress, cif_file_total)

    if count_total > 0:
        print("===",
              "No. films :",
              count_total)
        print("===",
              "No. untested :",
              int(len(status_dict['0'])),
              "--",
              round((len(status_dict['0'])/count_total)*100, 2),
              "%")
        print("===",
              "No. failed chemistry test :",
              int(len(status_dict['1'])),
              "--",
              round((len(status_dict['1'])/count_total)*100, 2),
              "%")
        print("===",
              "No. failed lattice test :",
              int(len(status_dict['3'])),
              "--",
              round((len(status_dict['3'])/count_total)*100, 2),
              "%")
        print("===",
              "No. passed step 1 :",
              int(len(status_dict['4'])),
              "--",
              round((len(status_dict['4'])/count_total)*100, 2),
              "%")
        print('-------------------------------------------------------------')
        for NP in sorted(process_dict.keys()):
            done, total = process_dict[NP]
            print("===",
                  done, 'of', total, 'CIFs done in Process', NP)

    return status_dict


def get_interpen_info(CIF_TODO_files):
    """
    Read CIF_TODO files and print statistics on interpenetration.
    """
    status_dict = {'0': [],
                   '1': [],
                   '2': [],
                   '3': [],
                   '4': [],
                   '4a': [],
                   '5': []}

    interpen_CIFs = []

    count_total = 0
    for file in CIF_TODO_files:
        if os.path.isfile(file) is True:
            data = pd.read_csv(file)
            for index, row in data.iterrows():
                stat = str(row['status'])
                film = row['film name']
                status_dict[stat].append(film)
                # firts two letters should be 'i_' if interpenetrated
                if film[:2] == 'i_':
                    interpen_CIFs.append(film)
                count_total += 1

    CIFS_added = len(interpen_CIFs)
    without_i_ = [i.replace('i_', '').split("_")[0] for i in interpen_CIFs]
    interpen_CIFs = len(set(without_i_))

    if count_total > 0:
        print("===",
              "total no. interpenetrated films :",
              interpen_CIFs)
        print("===",
              "Individual net CIFs added to analysis :",
              int(CIFS_added))


def get_status_dict_step_3(CIF_TODO_files):
    """
    Read CIF_TODO files into status dict and print status information.
    """
    status_dict = {'0': [],
                   '1': [],
                   '2': [],
                   '3': [],
                   '4': [],
                   '4a': [],
                   '5': []}

    process_dict = {}

    count_total = 0
    for file in CIF_TODO_files:
        cif_file_progress = 0
        cif_file_total = 0
        NP = str(file.replace(".csv", "").split("_")[-1])
        if os.path.isfile(file) is True:
            data = pd.read_csv(file)
            for index, row in data.iterrows():
                stat = str(row['status'])
                film = row['film name']
                status_dict[stat].append(film)
                count_total += 1
                if stat == '5':
                    cif_file_progress += 1
                cif_file_total += 1
            process_dict[NP] = (cif_file_progress, cif_file_total)

    if count_total > 0:
        print("===",
              "No. films :",
              count_total)
        print("===",
              "No. awaiting ASO test :",
              int(len(status_dict['4'])),
              "--",
              round((len(status_dict['4'])/count_total)*100, 2),
              "%")
        print("===",
              "No. with binding planes calculated :",
              int(len(status_dict['4a'])),
              "--",
              round((len(status_dict['4a'])/count_total)*100, 2),
              "%")
        print("===",
              "No. completed ASO test :",
              int(len(status_dict['5'])),
              "--",
              round((len(status_dict['5'])/count_total)*100, 2),
              "%")
        print('-------------------------------------------------------------')
        for NP in sorted(process_dict.keys()):
            done, total = process_dict[NP]
            print("===",
                  done, 'of', total, 'CIFs done in Process', NP)

    return status_dict


def get_max_limits(list_of_variables):
    """
    Function to get the maximum reported values of all the listed variables
    for limits of the plots.
    """
    for var in list_of_variables.keys():
        max_var = list_of_variables[var]
        for file in glob.glob("*_ASO_match.data"):
            data = pd.read_csv(file)
            if len(data[var]) > 0:
                new_max = max(data[var])
                if new_max > max_var:
                    max_var = new_max
        list_of_variables[var] = max_var
    return list_of_variables


def get_film_interface_structure_min_area(film_results, surf, film):
    """
    Obtain the interface structure with the max ASO and minimum Area for all
    miller planes of this film.
    """
    f_millers = list(set([i[1][3] for i in film_results]))
    s_millers = list(set([i[1][1] for i in film_results]))
    for s_m in s_millers:
        for f_m in f_millers:
            configs = list(set([i[1][4] for i in film_results
                                if i[1][3] == f_m
                                and i[1][1] == s_m]))
            for c in configs:
                results = np.asarray([k
                                      for k, i in enumerate(film_results)
                                      if i[1][3] == f_m
                                      and i[1][1] == s_m
                                      and i[1][4] == c])
                asos = np.asarray([round(float(i[1][5]), 3)
                                   for i in film_results
                                   if i[1][3] == f_m
                                   and i[1][1] == s_m
                                   and i[1][4] == c])
                # relative (replicated) match areas
                areas = np.asarray([round(float(i[1][9]), 3)
                                    for i in film_results
                                    if i[1][3] == f_m
                                    and i[1][1] == s_m
                                    and i[1][4] == c])
                # maximum mismatch of the interface
                MM = np.asarray([round(float(i[1][10]), 3)
                                 for i in film_results
                                 if i[1][3] == f_m
                                 and i[1][1] == s_m
                                 and i[1][4] == c])
                if len(asos) > 0:
                    # get arrays of max ASO
                    m_areas = areas[asos == max(asos)]
                    m_MM = MM[asos == max(asos)]
                    m_res = results[asos == max(asos)]
                    # get subset arrays of min area
                    min_area_MM = m_MM[m_areas == min(m_areas)]
                    min_area_res = m_res[m_areas == min(m_areas)]
                    # get the min "max mismatch" option from
                    # this subset
                    min_MM_res = min_area_res[min_area_MM == min(min_area_MM)]
                    # take the first index of min_MM_res and
                    # output the associated structures
                    index_out = min_MM_res[0]
                    f_struct = film_results[index_out][2]
                    s_struct = film_results[index_out][3]
                    f_struct.translate([0, 0, 1])
                    int_struct = f_struct + s_struct
                    # write PNG file + CIF files
                    int_file_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_area_interface.cif"
                    f_int_file_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_area_interface_film.cif"
                    s_int_file_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_area_interface_sub.cif"
                    int_png_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_area_interface.png"
                    output_ASE_PNG(int_struct, int_png_name)
                    int_struct.write(format='cif',
                                     filename=int_file_name)
                    s_struct.write(format='cif',
                                   filename=s_int_file_name)
                    f_struct.write(format='cif',
                                   filename=f_int_file_name)


def get_film_interface_structure_min_MM(film_results, surf, film):
    """
    Obtain the interface structure with the max ASO and minimum mismatch
    for all miller planes of this film.
    """
    f_millers = list(set([i[1][3] for i in film_results]))
    s_millers = list(set([i[1][1] for i in film_results]))
    for s_m in s_millers:
        for f_m in f_millers:
            configs = list(set([i[1][4] for i in film_results
                                if i[1][3] == f_m
                                and i[1][1] == s_m]))
            for c in configs:
                results = np.asarray([k
                                      for k, i in enumerate(film_results)
                                      if i[1][3] == f_m
                                      and i[1][1] == s_m
                                      and i[1][4] == c])
                asos = np.asarray([round(float(i[1][5]), 3)
                                   for i in film_results
                                   if i[1][3] == f_m
                                   and i[1][1] == s_m
                                   and i[1][4] == c])
                # relative (replicated) match areas
                areas = np.asarray([round(float(i[1][9]), 3)
                                    for i in film_results
                                    if i[1][3] == f_m
                                    and i[1][1] == s_m
                                    and i[1][4] == c])
                # maximum mismatch of the interface
                MM = np.asarray([round(float(i[1][10]), 3)
                                 for i in film_results
                                 if i[1][3] == f_m
                                 and i[1][1] == s_m
                                 and i[1][4] == c])
                if len(asos) > 0:
                    # get arrays of max ASO
                    m_areas = areas[asos == max(asos)]
                    m_MM = MM[asos == max(asos)]
                    m_res = results[asos == max(asos)]
                    # get subset arrays of min "max mismatch"
                    min_MM_area = m_areas[m_MM == min(m_MM)]
                    min_MM_res = m_res[m_MM == min(m_MM)]
                    # get the min match area option from
                    # this subset
                    min_area_res = min_MM_res[min_MM_area == min(min_MM_area)]
                    # take the first index of min_MM_res and
                    # output the associated structures
                    index_out = min_area_res[0]
                    f_struct = film_results[index_out][2]
                    s_struct = film_results[index_out][3]
                    f_struct.translate([0, 0, 1])
                    int_struct = f_struct + s_struct
                    int_file_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_MM_interface.cif"
                    f_int_file_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_MM_interface_film.cif"
                    s_int_file_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_MM_interface_sub.cif"
                    int_png_name = surf+"_"+s_m+"_"+film+"_"+f_m+"_"+str(c)+"_min_MM_interface.png"
                    output_ASE_PNG(int_struct, int_png_name)
                    int_struct.write(format='cif',
                                     filename=int_file_name)
                    s_struct.write(format='cif',
                                   filename=s_int_file_name)
                    f_struct.write(format='cif',
                                   filename=f_int_file_name)


def output_ASE_PNG(struct, filename):
    """
    Prepare and output PNG of ASE atoms (struct).

    """
    int_cell = struct.cell
    intsl = int_cell[0, :], int_cell[1, :]
    max_x = max([0, intsl[0][0], intsl[1][0], intsl[0][0] + intsl[1][0]])
    max_y = max([0, intsl[0][1], intsl[1][1], intsl[0][1] + intsl[1][1]])

    write_png(filename, struct, show_unit_cell=True, bbox=[-5, -5,
                                                           max_x+5,
                                                           max_y+5])


def ASO_match_line(file, data_list):
    """
    Write one comma separated line of data_list to file
    """

    with open(file, 'a') as f:
        for i in data_list[:-1]:
            f.write(str(i)+',')
        f.write(str(data_list[-1])+'\n')


def write_film_results(ASO_completed_films, surface, ignore_interpen=True):
    """
    Obtain the maximum DIB for each hkl for each film and output to film file.

    This was not used to produce final CSVs - it was replaced.
    """
    for film in ASO_completed_films:
        film_file = surface+"_"+film+"_max_dataframe.csv"
        film_db = pd.DataFrame(columns=['area', 'rel.area', 'new_area',
                                        'new_rel.area', 'cell_area',
                                        'cell_rel.area', 'aso', 'sa', 'sb',
                                        'trials', 'steps', 'time(s)', 'max_MM',
                                        'avg_MM', 'f1', 'f2', 'f3', 'f4', 's1',
                                        's2', 's3', 's4', 'fr1', 'fr2', 'fr3',
                                        'fr4', 'sr1', 'sr2', 'sr3', 'sr4',
                                        'm1', 'm2', 'bonds_list', 'f_miller',
                                        'config', 'dib'])

        # we want to ignore the data for interpenetrated CIFs
        # interpen CIFs will have non-interpen CIF counterparts with
        # i_filname_X
        if ignore_interpen:
            if len([i for i in ASO_completed_films if i != film and film in i]) > 0:
                continue

        for out_f in glob.glob(surface+"*"+film+"*_ASO_match.data"):
            # if i_ (implying interpen version) not in film, then it shouldnt
            # be in ASO_match.data
            if 'i_' not in film and 'i_' in out_f:
                continue
            data = pd.read_csv(out_f)
            f_miller = out_f.replace("_ASO_match.data", "").replace(film, "").split("_")[3]
            config = out_f.replace("_ASO_match.data", "").replace(film, "").split("_")[4]
            if len(data['aso']) == 0:
                continue
            # calculate max dib
            dib, max_aso, max_frame = calculate_delta_bonds_from_frame(
                    data,
                    film,
                    f_miller,
                    config)
            # in this plot we go for minimal match area over minimal mismatch
            # the following lines are swapped later on
            # minimise relative match area
            min_area = min(max_frame['new_rel.area'])
            min_area_db = max_frame[max_frame['new_rel.area'] == min_area]
            # minimise "max mismatch"
            min_MM = min(min_area_db['max_MM'])
            min_MM_db = min_area_db[min_area_db['max_MM'] == min_MM]

            # copy the first row of the current result dataframe for outputting
            final_db = min_MM_db.loc[min_MM_db.index[0]].copy()
            # add miller indices to row
            final_db['f_miller'] = [f_miller]
            final_db['config'] = [config]

            final_db['dib'] = dib
            # append final_db to film_db
            film_db = film_db.append(final_db)

        film_db.to_csv(film_file)  # , index=False)


def calculate_delta_bonds(max_frame, film, mill, config):
    """
    Calculate the change in bonding of the interface based on bonds lost and
    ASO.

    dib = # bonds formed / (# bonds formed + # dangling bonds)

    # dangling bonds = # bonds broken to form the interface - # bonds formed by
    the ASO

    """
    # aso
    aso_to_plot = float(max_frame['aso'])
    # print(aso_to_plot)
    # no binding sites in super-cell
    sb = float(max_frame['sb'])
    # print(sb)
    # number of bonds formed
    bonds_formed = aso_to_plot * sb
    # print(bonds_formed)
    # integer expansion from unit-cell to super-cell
    rel_area = int(max_frame['cell_rel.area'])
    # number of bonds broken to form interface
    # effectively the number of "dangling" bonds as we substract the bonds
    # formed (from the ASO) from the total number of broken bonds

    # get total no. broken bonds
    # we ignore any C-X or H-X broken bonds because they are often included
    # due our large skin distance in defining the neighbour list and
    # binding network.
    file = film+"_"+mill+"_"+config+"_bind_bonds.txt"
    bonds_data = pd.read_csv(file)
    broke_per_cell = 0
    for i, row in bonds_data.iterrows():
        a1 = row['atom1_type']
        a2 = row['atom2_type']
        if a1 == "H" or a2 == "H":
            continue
        if a1 == "C" or a2 == "C":
            continue
        broke_per_cell += 1

    broken_bonds = (broke_per_cell * rel_area) - bonds_formed
    # print(int(max_frame['bonds_list']))
    # print(rel_area)
    # print(rel_area*int(max_frame['bonds_list']))
    # print(broken_bonds)
    # area_to_use = float(max_frame['cell_area'])
    dib = bonds_formed / (broken_bonds + bonds_formed)
    # print(dib)
    if dib > 1:
        dib = 1.0
        print("the DIB for", film+"_"+mill+"_"+config, "was > 1.0.")
    # print(aso_to_plot, sb, bonds_formed, rel_area, broken_bonds,
    #        dib, int(max_frame['bonds_list']),
    #        int(max_frame['bonds_list']) * rel_area)
    return dib, aso_to_plot


def calculate_delta_bonds_from_frame(frame, film, mill, config):
    """
    Calculate the change in bonding of the interface based on bonds lost and
    ASO.

    dib = # bonds formed / (# bonds formed + # dangling bonds)

    # dangling bonds = # bonds broken to form the interface - # bonds formed by
    the ASO

    """
    # aso
    asos = np.array(frame['aso'])
    # no binding sites in super-cell
    sbs = np.array(frame['sb'])
    # number of bonds formed
    bonds_formed = asos * sbs
    # integer expansion from unit-cell to super-cell
    rel_areas = np.array(frame['cell_rel.area'])
    # number of bonds broken to form interface
    # effectively the number of "dangling" bonds as we substract the bonds
    # formed (fromt he ASO) from the total number of broken bonds

    # get total no. broken bonds
    # we ignore any C-X or H-X broken bonds because they are often included
    # due our large skin distance in defining the neighbour list and
    # binding network.
    file = film+"_"+mill+"_"+config+"_bind_bonds.txt"
    bonds_data = pd.read_csv(file)
    broke_per_cell = 0
    for i, row in bonds_data.iterrows():
        a1 = row['atom1_type']
        a2 = row['atom2_type']
        if a1 == "H" or a2 == "H":
            continue
        if a1 == "C" or a2 == "C":
            continue
        broke_per_cell += 1

    broken_bonds = (broke_per_cell * rel_areas) - bonds_formed
    # area_to_use = float(max_frame['cell_area'])
    dibs = bonds_formed / (broken_bonds + bonds_formed)
    frame['dib'] = dibs
    max_frame = frame[frame['dib'] == max(dibs)]
    dib = max(dibs)
    aso_to_plot = max(max_frame['aso'])
    if dib > 1:
        dib = 1.0
        print("the DIB for", film+"_"+mill+"_"+config, "was > 1.0.")
    # print(aso_to_plot, sb, bonds_formed, rel_area, broken_bonds,
    #        dib, int(max_frame['bonds_list']),
    #        int(max_frame['bonds_list']) * rel_area)
    return dib, aso_to_plot, max_frame


def calculate_append_DIB_frame(frame, film, mill, config):
    """
    Calculate the change in bonding of the interface based on bonds lost and
    ASO. Does calculation and appends result to Pandas DataFrame.

    dib = # bonds formed / (# bonds formed + # dangling bonds)

    # dangling bonds = # bonds broken to form the interface - # bonds formed by
    the ASO

    """
    # aso
    asos = np.array(frame['aso'])
    # no binding sites in super-cell
    sbs = np.array(frame['sb'])
    # number of bonds formed
    bonds_formed = asos * sbs
    # integer expansion from unit-cell to super-cell
    rel_areas = np.array(frame['cell_rel.area'])
    # number of bonds broken to form interface
    # effectively the number of "dangling" bonds as we substract the bonds
    # formed (fromt he ASO) from the total number of broken bonds

    # get total no. broken bonds
    # we ignore any C-X or H-X broken bonds because they are often included
    # due our large skin distance in defining the neighbour list and
    # binding network.
    file = film+"_"+mill+"_"+config+"_bind_bonds.txt"
    bonds_data = pd.read_csv(file)
    broke_per_cell = 0
    for i, row in bonds_data.iterrows():
        a1 = row['atom1_type']
        a2 = row['atom2_type']
        if a1 == "H" or a2 == "H":
            continue
        if a1 == "C" or a2 == "C":
            continue
        broke_per_cell += 1

    broken_bonds = (broke_per_cell * rel_areas) - bonds_formed
    # area_to_use = float(max_frame['cell_area'])
    dibs = bonds_formed / (broken_bonds + bonds_formed)
    frame['dib'] = dibs
    max_dib = max(dibs)
    max_aso = max(frame['aso'])
    if max_dib > 1:
        max_dib = 1.0
        print("the DIB for", film+"_"+mill+"_"+config, "was > 1.0.")
    # print(aso_to_plot, sb, bonds_formed, rel_area, broken_bonds,
    #        dib, int(max_frame['bonds_list']),
    #        int(max_frame['bonds_list']) * rel_area)
    return max_dib, max_aso, frame


def get_DOI(film, csv_df):
    """
    Get DOI from a CSV of DOIs (provided with CORE MOF database) given a film
    name
    """
    if 'i_' not in film:
        film_REFCODE = film.split("_")[0]
    else:
        film_REFCODE = film.split("_")[1]
    row = csv_df[csv_df['REFCODE'] == film_REFCODE]

    if row['DOI'] == '-':
        # use CCDC
        DOI = row['CCDC']
    else:
        DOI = row['DOI']

    return DOI


def calc_area_ratio(data, s_UC_area):
    """
    Calculate area ratio from ASO_match.data DataFrame

    Arguments:
        data (Pandas DataFrame) - Dataframe of ASO_match.data
        s_UC_area (float) - unit cell area of substrate in Angstrom^2


    returns:
        area_rat_DF (Pandas DataFrame) - DF with column of area ratios for all
            rows of input frame.

    """
    area_rat = []
    for i, row in data.iterrows():
        f_UC_area = row['area'] / row['rel.area']
        f_i = row['rel.area']
        s_j = round(row['area'] / s_UC_area)
        area_rat.append(abs(f_UC_area / s_UC_area - s_j / f_i))

    area_rat_DF = data.copy()
    area_rat_DF['area_rat'] = area_rat
    return area_rat_DF


def get_top_interface(frame):
    """Return Dataframe of top interface in a DataFrame.

    Top interface defined by:
        1 - highest dib
        2 - lowest match areas
        3 - lowest maximum mismatch
        4 - lowest average mismatch

    """
    # filter by max DIB
    max_dib = max(frame['dib'])
    max_dib_frame = frame[frame['dib'] == max_dib]
    # filter by min area
    min_area = min(max_dib_frame['area'])
    min_area_frame = max_dib_frame[max_dib_frame['area'] == min_area]
    # filter by min max MM
    min_max_MM = min(min_area_frame['max_MM'])
    min_max_MM_frame = min_area_frame[min_area_frame['max_MM'] == min_max_MM]
    # filter by min average MM
    min_avg_MM = min(min_max_MM_frame['avg_MM'])
    min_avg_MM_frame = min_max_MM_frame[min_max_MM_frame['avg_MM'] == min_avg_MM]
    # collect the first entry of this dataframe
    top_interface = min_avg_MM_frame.loc[min_avg_MM_frame.index[0]].copy()
    return top_interface


def get_transforms_frame(frame):
    """Get the initial Zur transform matrix from ASO frame.

    Given by columns fi and si for the film and substrate, respectively.

    """
    film_transform = np.array([(frame['f1'], frame['f2']),
                               (frame['f3'], frame['f4'])])
    subs_transform = np.array([(frame['s1'], frame['s2']),
                               (frame['s3'], frame['s4'])])

    return film_transform, subs_transform


def get_UC_vectors(CIF):
    """Get reduced unit cell vectors from the XY cell of a given CIF.

    """
    slab = mg.Structure.from_file(CIF)
    r1, r2 = pmgsa_AT.reduce_vectors(
                slab.lattice.matrix[0],
                slab.lattice.matrix[1])
    return slab.lattice.matrix[0], slab.lattice.matrix[1], r1, r2, slab


def get_rotated_UCs(frame_row, f_prefix, s_prefix, max_angle_tol,
                    verbose=False):
    """Given a row of a DataFrame, get the rotated unit cell of the film with
       respect to its matching substrate supercell.

    Arguments:
        frame_row (DataFrame) - row of interest from ASO_match.data DF
        f_prefix (str) - prefix for film file name
        s_prefix (str) - prefix for substrate file name
        max_angle_tol (float) - tolerance on angle between supercells
        verbose (bool) - switch if you want to output the information for each
            interface
    Returns:
        struct_to_test (list) - list of ASE Atoms objects that contain film
            unit cell positions after rotation

    """
    # fig, ax = plt.subplots(figsize=(5, 5))
    # get film and substrate supercell
    film_transform, subs_transform = get_transforms_frame(frame_row)

    # get film and substrate unitcell vectors
    f_bp_cif = f_prefix+"_bind_plane.cif"
    s_bp_cif = s_prefix+"_surf_out.cif"
    f1, f2, rpf1, rpf2, f_slab = get_UC_vectors(f_bp_cif)
    s1, s2, rps1, rps2, s_slab = get_UC_vectors(s_bp_cif)

    # for a in f_slab:
    #     ax.scatter(a.x, a.y, c='purple', s=40,
    #                edgecolor='none', alpha=1.0)
    # for a in s_slab:
    #     ax.scatter(a.x, a.y, c='green', s=40,
    #                edgecolor='none', alpha=1.0, marker='o')

    # # cells
    # plot_cell(f1,
    #           f2,
    #           ax=ax,
    #           face='r', face_a=0.0, lines='r--',
    #           lines_a=0.2)
    # plot_cell(s1,
    #           s2,
    #           ax=ax,
    #           face='b', face_a=0.0, lines='b--',
    #           lines_a=0.2)

    # transform unit cell vectors to supercell vectors and reduce
    f_SC = pmgsa_AT.reduce_vectors(*np.dot(film_transform, [f1, f2]))
    s_SC = pmgsa_AT.reduce_vectors(*np.dot(subs_transform, [s1, s2]))

    # plot_cell(f_SC[0],
    #           f_SC[1],
    #           ax=ax,
    #           face='r', face_a=0.0, lines='r-',
    #           lines_a=0.8)
    # plot_cell(s_SC[0],
    #           s_SC[1],
    #           ax=ax,
    #           face='b', face_a=0.0, lines='b-',
    #           lines_a=0.8)

    # do not apply replication to film and substrate supercell
    if verbose is True:
        print('transform matrices')
        print(film_transform)
        print(subs_transform)
        print("UC vectors:")
        print(f1, rpf1)
        print(f2, rpf2)
        print(s1, rps1)
        print(s2, rps2)
        print("SC vectors:")
        print(f_SC)
        print(s_SC)
        print('translate?')
        print(frame_row['translated'])
        print('mismatches:')
        vec_set1 = f_SC
        vec_set2 = s_SC
        print(np.absolute(pmgsa_AT.rel_strain(vec_set1[0], vec_set2[0])))
        print(np.absolute(pmgsa_AT.rel_strain(vec_set1[1], vec_set2[1])))
        print(np.absolute(pmgsa_AT.rel_angle(vec_set1, vec_set2)))

    # get angle and direction of rotation necessary to
    # make a vector (shortest before replication)
    # of film SC parallel with a vector (shortest before
    # replication) of substrate SC
    sub_latt_a = s_SC[0]
    sub_latt_b = s_SC[1]
    film_latt_a = f_SC[0]
    film_latt_b = f_SC[1]
    R_fa_sa, fa_sa_angle = ASO.get_supercell_rot_matrix(sub_latt_a,
                                                        film_latt_a,
                                                        verbose=verbose)

    # do rotation into substrate coordinate system
    # rotate unit cell vectors - this == to main_step_3 code
    rot_cells = ASO.rotate_to_substrate_system(sub_latt_a,
                                               sub_latt_b,
                                               R_fa_sa,
                                               film_latt_a,
                                               film_latt_b,
                                               verbose=verbose)
    r_film_latt_a, r_film_latt_b, r_sub_latt_a, r_sub_latt_b = rot_cells

    # determine if extra rotations are necessary and
    # and determine if translations can be applied
    skip, apply_translation = ASO.determine_extra_rotations(
                            r_sub_latt_a, r_sub_latt_b,
                            r_film_latt_a, r_film_latt_b,
                            max_angle_tol,
                            verbose=verbose)

    # do rotation into substrate coordinate system
    # rotate unit cell vectors - this differs to main_step_3 code
    rot_cells = ASO.rotate_to_substrate_system(sub_latt_a,
                                               sub_latt_b,
                                               R_fa_sa,
                                               f1,
                                               f2,
                                               verbose=verbose)
    r_f1, r_f2, r_sub_latt_a, r_sub_latt_b = rot_cells

    # plot_cell(r_f1,
    #           r_f2,
    #           ax=ax,
    #           face='r', face_a=0.0, lines='r--',
    #           lines_a=0.8)

    # rotate atoms and output into ASE structure
    f_str = ASO.get_film_ASE(f_slab, r_f1,
                             r_f2, R_fa_sa,
                             apply_translation)

    # print(f_str.cell)
    # if input('show f_str') is 't':
    #     view(f_str)

    # wrap atoms into their respective supercells
    f_str.wrap()

    # place the structures COP at (x,y) == (0,0)
    # print(f_str.positions)
    COP_atoms = [np.average(f_str.positions[:, 0]),
                 np.average(f_str.positions[:, 1])]
    # print(COP_atoms)
    # translate from COP to (0,0)
    f_str.translate([-COP_atoms[0], -COP_atoms[1], 0])
    # print(f_str.positions)
    # print([np.average(f_str.positions[:, 0]),
    #              np.average(f_str.positions[:, 1])])
    # if input('show structs') == 't':
    #     view(f_str)
    struct_to_test = []
    struct_to_test.append(f_str.copy())

    # for a in f_str:
    #     ax.scatter(a.x, a.y, c='purple', s=40,
    #                edgecolor='none', alpha=1.0, marker='x')

    # manually set limits for each figure - as long as they are all
    # the same size space
    # ax.set_xlim(-15, 15)
    # ax.set_ylim(-15, 15)
    # ax.set_xticks([])
    # ax.set_yticks([])
    # ax.tick_params(axis='both', which='major', labelsize=16)
    # ax.set_xlabel('$x~[\mathrm{\AA}]$', fontsize=16)
    # ax.set_ylabel('$y~[\mathrm{\AA}]$', fontsize=16)
    # fig.savefig("temporary_cell_figure.pdf",
    #             bbox_inches='tight', dpi=360)
    # plt.close()
    # if input('top?') is 't':
    #     os.system('cp temporary_cell_figure.pdf top_cell_fig.pdf')

    return struct_to_test


def get_distance_bet_structs(struct_1, struct_2, verbose=False):
    """Get the min, max and avg pair distance between atoms in two structures.

    Keywords:
        struct_1 (ASE Atoms) - structure to be compared to
        struct_2 (ASE Atoms) - structure being tested for uniqueness
        verbose (bool) - switch if you want to output the information for each
            interface
    Returns:
        min_dist (float) - minimum pair wise distance between structures
        avg_dist (float) - average pair wise distance between structures
        max_dist (float) - maximum pair wise distance between structures
    """
    # get pairwise distances
    dist_arr = distance.cdist(np.asarray(struct_1.get_positions()),
                              np.asarray(struct_2.get_positions()),
                              'euclidean')
    if verbose is True:
        print(dist_arr)
    s1_counted = []
    s2_counted = []
    min_dists = []
    # want the min, avg and max distance for each atom
    # make sure no atoms are double counted
    # flatten and sort dist array
    flat_dist = np.sort(dist_arr.flatten())
    if verbose is True:
        print(flat_dist)
    for di in flat_dist:
        if len(s1_counted) == len(struct_1):
            if len(s2_counted) == len(struct_2):
                continue
        a1_id, a2_id = np.where(dist_arr == di)
        for a1 in a1_id:
            for a2 in a2_id:
                # print(a1, a2)
                if a1 not in s1_counted and a2 not in s2_counted:
                    s1_counted.append(a1)
                    s2_counted.append(a2)
                    min_dists.append(di)
                    if verbose is True:
                        print('added', a1, a2)
    if verbose is True:
        print(min_dists)
        print(s1_counted)
        print(s2_counted)

    min_dist = min(min_dists)
    avg_dist = np.average(min_dists)
    max_dist = max(min_dists)

    return min_dist, avg_dist, max_dist


def get_miller_DIBS(file_name, film, f_hkl, config,
                    surface, surface_hkl, tol_u,
                    max_angle_tol,
                    viz=False, verbose=False,
                    plot=0):
    """Obtain the 1st and 2nd best DIB for a given Miller plane of a MOF.

    Keywords:
        file_name (str) - ASO_match.data file to read
        film (str) - name of film CIF
        f_hkl (str) - string of film hkl
        config (str) - configuration of slab being tested
        surface (str) - name of substrate CIF
        surface_hkl (str) - string of substrate hkl
        tol_u (float) - tolerance on interface uniqueness (angstrom)
        max_angle_tol (float) - tolerance on angle between supercells
        viz (bool) - switch if you want to visulize each structure
        verbose (bool) - switch if you want to output the information for each
            interface
        plot (int - default=0) - switch if you want to plot the rotated UCs for all
            interfaces (0 = no, 1 = only unique, 2 = all UCs (much slower))
    Returns:
        max_dib (float) - maximum DIB
        top_interface (DataFrame) - Row from ASO_match.data file
        second_best_dib (float) - second highest DIB of unique interface
        second_best_interface (DataFrame) - Row from ASO_match.data file
    """
    data = pd.read_csv(file_name)
    if len(data) == 0:
        return 0, 0, 0, 0
    # add a column for the DIB of each interface
    max_dib, max_aso, frame = calculate_append_DIB_frame(
                        data,
                        film,
                        f_hkl,
                        config)

    if plot == 1 or plot == 2:
        fig, ax = plt.subplots(figsize=(5, 5))
        ax.scatter(0, 0, c='k', alpha=0.2, s=40, edgecolors='none')

    # get supercell with maximum DIB and minimum match area
    # if there exists multiple interfaces with the same match area and dib
    # collect the interface with the smallest maximum mismatch value
    top_interface = get_top_interface(frame)

    # get top interface rotated UCs
    top_int_structs = get_rotated_UCs(top_interface,
                                      film+"_"+f_hkl+"_"+str(config),
                                      surface+"_"+surface_hkl,
                                      max_angle_tol, verbose=verbose)

    if plot == 1 or plot == 2:
        # get limits
        min_x, max_x = 0, 0
        min_y, max_y = 0, 0
        for atom in top_int_structs[0]:
            ax.scatter(atom.x, atom.y, c='k', alpha=1.0,
                       s=40, edgecolors='none',
                       marker='x')
            min_x = min(atom.x, min_x)
            max_x = max(atom.x, max_x)
            min_y = min(atom.y, min_y)
            max_y = max(atom.y, max_y)

    # output image of top interface
    top_int_name = film+"_"+f_hkl+"_"+config+"_top_int_UC.png"
    top_int_structs[0].write(top_int_name, format='png')

    if viz is True:
        view(top_int_structs[0])
    if verbose is True:
        print("max DIB =", max_dib)
        print("-------------------")
        print("searching for second best...")
    # find the second highest DIB associated with a unique supercell pair
    second_best_dib = 0
    second_best_interface = pd.DataFrame()
    for i, row in frame.iterrows():
        if plot != 2:
            # wanna see them all!
            if float(row['dib']) <= second_best_dib:
                # save time by not testing all
                continue
        int_structs = get_rotated_UCs(row,
                                      film+"_"+f_hkl+"_"+str(config),
                                      surface+"_"+surface_hkl,
                                      max_angle_tol, verbose=verbose)
        if viz is True:
            view(int_structs[0])
            input("int_structs - done?")
            continue
        is_unique = True
        max_dists = []
        for j, s1 in enumerate(top_int_structs):
            if is_unique is False:
                continue
            for k, s2 in enumerate(int_structs):
                if is_unique is False:
                    continue
                # if input('show all?') is 't':
                #     view(s1+ s2)
                #     input("ready?")
                dists = get_distance_bet_structs(s1, s2,
                                                 verbose=verbose)
                min_dist, avg_dist, max_dist = dists
                if viz is True:
                    view(s1)
                    view(s2)
                if verbose is True:
                    print(max_dist)
                    print(i, j, k, min_dist, avg_dist, max_dist)
                # a supercell is considered a replicate if the max_dist
                # between atoms in the two unit cells is less than some
                # tolerance
                if max_dist ** 2 < tol_u ** 2:
                    is_unique = False
                max_dists.append(max_dist)
        min_max_dist = min(max_dists)
        if is_unique is True:
            C = 'r'
            A = 0.2
            M = 'x'
            if verbose is True:
                print('unique w', min_max_dist)
                print('has dib:', row['dib'])
            if row['dib'] > second_best_dib:
                second_best_dib = float(row['dib'])
                second_best_interface = row
                sec_int_name = film+"_"+f_hkl+"_"+config+"_2nd_int_UC.png"
                int_structs[0].write(sec_int_name, format='png')
                if verbose is True:
                    print(float(row['dib']))
                    print("update 2nd DIB.")
                    print('top =', max_dib, '2nd:', second_best_dib)
                # if input('show?') is 't':
                #     view(s1 + s2)
                #     view(s1)
                #     view(s2)
                #     view(top_int_structs[0])
                #     input('top structs - done?')
                #     view(int_structs[0])
                #     input("int_structs - done?")
                # input("done?")

        else:
            C = 'k'
            A = 0.2
            M = 'o'
            if verbose is True:
                print('not unique w', min_max_dist)
                print('has dib:', row['dib'])
                if row['dib'] > second_best_dib:
                    print('top =', max_dib, '2nd:', second_best_dib)

        if plot == 1 or plot == 2:
            for atom in int_structs[0]:
                ax.scatter(atom.x, atom.y, c=C, alpha=A,
                           s=40, edgecolors='none',
                           marker=M)
                min_x = min(atom.x, min_x)
                max_x = max(atom.x, max_x)
                min_y = min(atom.y, min_y)
                max_y = max(atom.y, max_y)
            ax.plot(int_structs[0].positions[:, 0],
                    int_structs[0].positions[:, 1],
                    c=C, alpha=0.1)
        if verbose is True:
            print('top =', max_dib, '2nd:', second_best_dib)

    if plot == 1 or plot == 2:
        ax.set_xlim(min(min_y, min_y)-5, max(max_x, max_y)+5)
        ax.set_ylim(min(min_y, min_y)-5, max(max_x, max_y)+5)
        ax.set_xticks([])
        ax.set_yticks([])
        # save fig
        fig.tight_layout()
        fig.savefig(film+"_"+f_hkl+"_"+config+"_int_UC.pdf",
                    bbox_inches='tight')
        plt.close()
    return max_dib, top_interface, second_best_dib, second_best_interface


def get_miller_ASO(file_name):
    """Obtain the maximum ASO for a given Miller plane.

    Keywords:
        file_name (str) - ASO_match.data file to read
    Returns:
        max_aso (float) - maximum ASO
    """
    data = pd.read_csv(file_name)
    if len(data) == 0:
        return 0
    asos = np.array(data['aso'])
    max_aso = max(asos)
    return max_aso


def get_miller_UCA(file_name):
    """Obtain the film and substrate unit cell areas for a given Miller plane.

    Keywords:
        file_name (str) - ASO_match.data file to read
        film (str) - name of film CIF
        f_hkl (str) - string of film hkl
        config (str) - configuration of slab being tested
    Returns:
        max_aso (float) - maximum ASO
    """
    data = pd.read_csv(file_name)
    if len(data) == 0:
        return 0, 0
    first_row = data.iloc[0]
    s_uc = float(first_row['sub_area'])
    f_uc = float(first_row['area']) / float(first_row['rel.area'])

    return s_uc, f_uc


def get_miller_MCIA(file_name):
    """Obtain the minimum coincident match area for a given Miller plane.

    Keywords:
        file_name (str) - ASO_match.data file to read
        film (str) - name of film CIF
        f_hkl (str) - string of film hkl
        config (str) - configuration of slab being tested
    Returns:
        max_aso (float) - maximum ASO
    """
    data = pd.read_csv(file_name)
    if len(data) == 0:
        return 0
    match_areas = np.array(data['area'])  # these are pre replication
    mcia = min(match_areas)

    return mcia


def collect_film_results(ASO_files, film, surface, surface_hkl,
                         tol_u, max_angle_tol, prefix):
    """Collect the results for all Miller planes of a given film.

    Collects max and second best DIB + properties of those interfaces.
    Collects max ASO and minimium coincident match area.

    Keywords:

    Returns:
        res_dict (dict) - dictionary of all properties collected.
    """
    film_max_dib_int = pd.DataFrame()
    film_max_dib = 0
    film_max_hkl = 0
    film_max_config = 0
    film_2nd_dib_int = pd.DataFrame()
    film_2nd_dib = 0
    film_2nd_hkl = 0
    film_2nd_config = 0
    film_MCIA = 100000000
    film_MCIA_hkl = 0
    film_MCIA_config = 0
    film_max_ASO_hkl = 0
    film_max_ASO_config = 0
    film_max_ASO = 0
    film_min_ratio = 10000000
    film_min_ratio_hkl = 0
    film_min_ratio_config = 0
    for int_f in ASO_files:
        if os.path.isfile(int_f) is False:
            continue
        if 'i_' in int_f and 'i_' not in film:
            # implies we are checking the non-interpen version of the film
            continue
        name = int_f.replace("_ASO_match.data", "")
        name = name.replace(prefix, "")
        f_hkl = name.split("_")[-2]
        # if f_hkl != '11-1':
        #     continue
        config = name.split("_")[-1]
        print("--------------------------------------------------")
        print("doing hkl:", f_hkl, "config:", config)
        print("--------------------------------------------------")
        # print(name.split("_"))
        # get UC area ratio
        s_UC, f_UC = get_miller_UCA(int_f)
        if s_UC != 0:
            ratio = f_UC / s_UC
        else:
            ratio = 10000000
        if ratio < film_min_ratio:
            film_min_ratio = ratio
            film_min_ratio_hkl = f_hkl
            film_min_ratio_config = config
        # get maximum ASO
        max_ASO = get_miller_ASO(int_f)
        # check if max DIB needs updating for film
        if max_ASO > film_max_ASO:
            film_max_ASO = max_ASO
            film_max_ASO_hkl = f_hkl
            film_max_ASO_config = config
        # check if MCIA needs updating
        MCIA = get_miller_MCIA(int_f)
        print("MCIA =", MCIA)
        if MCIA < film_MCIA and MCIA > 0:
            film_MCIA = MCIA
            film_MCIA_hkl = f_hkl
            film_MCIA_config = config
        DIB1, int1, DIB2, int2 = get_miller_DIBS(int_f, film, f_hkl,
                                                 config, surface,
                                                 surface_hkl, tol_u,
                                                 max_angle_tol,
                                                 viz=False,
                                                 verbose=False)
        # check if max DIB needs updating for film
        if DIB1 > film_max_dib:
            film_2nd_dib_int = film_max_dib_int
            film_2nd_dib = film_max_dib
            film_2nd_hkl = film_max_hkl
            film_2nd_config = film_max_config
            film_max_dib_int = int1
            film_max_dib = DIB1
            film_max_hkl = f_hkl
            film_max_config = config
            # get minimum coincident match area of miller plane with max DIB
            # # update the MCIA of this miller plane
            # MCIA = aoo.get_miller_MCIA(int_f)
            # film_MCIA = MCIA
            # film_MCIA_hkl = f_hkl
            # film_MCIA_config = config
        # check if second best DIB also needs updating for film
        elif DIB1 > film_2nd_dib:
                film_2nd_dib = DIB1
                film_2nd_dib_int = int1
                film_2nd_hkl = f_hkl
                film_2nd_config = config
        if DIB2 > film_2nd_dib:
            film_2nd_dib = DIB2
            film_2nd_dib_int = int2
            film_2nd_hkl = f_hkl
            film_2nd_config = config
        print("max DIB:", DIB1, "2nd DIB:", DIB2)
    # if the max DIB and 2nd DIB are both 0, it implies that no
    # matching supercells were tested for Miller planes with binding planes
    if film_max_dib == 0 and film_2nd_dib == 0:
        print('no matching lattices were found for Miller planes')
        print('with desired binding sites')
        return 0
    if film_2nd_dib == 0:
        film_2nd_dib_int = None
    print("------------------------------------")
    print("film max DIB:", film_max_dib, 'for hkl =', film_max_hkl)
    print("second best DIB:", film_2nd_dib, 'for hkl =', film_2nd_hkl)
    print("----------------------------------------------------------")

    res_dict = {}
    res_dict['DIB1'] = film_max_dib
    res_dict['DIB2'] = film_2nd_dib
    res_dict['frame1'] = film_max_dib_int
    res_dict['frame2'] = film_2nd_dib_int
    res_dict['ASO'] = film_max_ASO
    res_dict['MCIA'] = film_MCIA
    res_dict['f_m_hkl'] = film_max_hkl
    res_dict['f_m_config'] = film_max_config
    res_dict['f_m_ASO_hkl'] = film_max_ASO_hkl
    res_dict['f_MCIA_hkl'] = film_MCIA_hkl
    res_dict['f_min_ratio'] = film_min_ratio
    res_dict['f_min_ratio_hkl'] = film_min_ratio_hkl

    return res_dict


def plot_cell(v1, v2, face, face_a, lines, lines_a, ax):
    """
    Plot a cell of vectors v1 and v2 with matplotlib.

    Arguments:
        v1 (array) - vector 1 of cell
        v2 (array) - vector 2 of cell
        face (str) - colour for face of cell
        face_a (float) - alpha for face of cell
        lines (str) - line properties for cell
        lines_a (float) - alpha for lines of cell
    """
    Path = mpath.Path
    path_data = [
        (Path.MOVETO, (0, 0)),
        (Path.LINETO, (v1[0], v1[1])),
        (Path.LINETO, (v1[0] + v2[0], v1[1] + v2[1])),
        (Path.LINETO, (v2[0], v2[1])),
        (Path.CLOSEPOLY, (0, 0)),
        ]
    codes, verts = zip(*path_data)
    path = mpath.Path(verts, codes)
    patch = mpatches.PathPatch(path, facecolor=face, alpha=face_a)
    ax.add_patch(patch)
    # plot control points and connecting lines
    x, y = zip(*path.vertices)
    line, = ax.plot(x, y, lines, alpha=lines_a, lw=3)


def get_ASO_files(film):
    """Get list of ASO files associated with a single film.

    """
    init_list = glob.glob("*"+film+"*ASO*data")
    ASO_files = [i for i in init_list if "_"+film+"_" in i]
    if len(ASO_files) == 0:
        print("no Miller planes with appropriate binding sites were found")
        return []
    return ASO_files


def init_res_file_per_BP(filename):
    """Creates and writes header to CSV with 1 line per binding plane.

    """
    with open(filename, 'w') as f:
        f.write('substrate,')
        f.write('s_hkl,')
        f.write('s_UC_area,')
        f.write('film,')
        f.write('f_hkl,')
        f.write('f_UC_area,')
        f.write('config,')
        f.write('aso,')
        f.write('sa,')
        f.write('sb,')
        f.write('area,')
        f.write('rel.area,')
        f.write('new_area,')
        f.write('new_rel.area,')
        f.write('max_MM,')
        f.write('avg_MM,')
        f.write('DIB1,')
        f.write('DIB2,')
        f.write('UC_ratio,')
        f.write('MCIA')
        f.write('\n')


def write_per_BP_line(data, filename):
    """Write data to CSV file with 1 line per BP.

    """
    with open(filename, 'a') as f:
        f.write(data['surface']+',')
        f.write(data['surface_hkl']+',')
        f.write(str(data['surface_UC_area'])+',')
        f.write(data['film']+',')
        f.write(data['film_hkl']+',')
        f.write(str(data['film_UC_area'])+',')
        f.write(data['config']+',')
        f.write(str(data['max_ASO'])+',')
        f.write(str(data['int1']['sa'])+',')
        f.write(str(data['int1']['sb'])+',')
        f.write(str(data['int1']['area'])+',')
        f.write(str(data['int1']['rel.area'])+',')
        f.write(str(data['int1']['new_area'])+',')
        f.write(str(data['int1']['new_rel.area'])+',')
        f.write(str(data['int1']['max_MM'])+',')
        f.write(str(data['int1']['avg_MM'])+',')
        f.write(str(data['DIB1'])+',')
        f.write(str(data['DIB2'])+',')
        f.write(str(data['ratio'])+',')
        f.write(str(data['MCIA']))
        f.write('\n')


def init_BP_dict(surface, surface_hkl, film, film_hkl, config):
    """Initialise a dictionary for binding plane results from some initial
        values.

    """
    b_dict = {}
    b_dict['surface'] = surface
    b_dict['surface_hkl'] = surface_hkl
    b_dict['film'] = film
    b_dict['film_hkl'] = film_hkl
    b_dict['config'] = config
    return b_dict


def init_film_dict(surface, surface_hkl, film):
    """Initialise a dictionary for film results from some initial values.

    """
    f_dict = {}
    # known initial values
    f_dict['surface'] = surface
    f_dict['surface_hkl'] = surface_hkl
    f_dict['film'] = film

    # unknown - set to None
    f_dict['s_UC_area'] = None
    # '1_' => max DIB interface
    f_dict['1_f_hkl'] = None
    f_dict['1_config'] = None
    f_dict['1_f_UC_area'] = None
    f_dict['1_aso'] = None
    f_dict['1_area'] = None
    f_dict['1_rel.area'] = None
    f_dict['1_new_area'] = None
    f_dict['1_new_rel.area'] = None
    f_dict['1_max_MM'] = None
    f_dict['1_avg_MM'] = None
    f_dict['1_max_DIB'] = None
    f_dict['1_bind_prefix'] = None
    # '2_' => max DIB interface
    f_dict['2_f_hkl'] = None
    f_dict['2_config'] = None
    f_dict['2_f_UC_area'] = None
    f_dict['2_aso'] = None
    f_dict['2_area'] = None
    f_dict['2_rel.area'] = None
    f_dict['2_new_area'] = None
    f_dict['2_new_rel.area'] = None
    f_dict['2_max_MM'] = None
    f_dict['2_avg_MM'] = None
    f_dict['2_max_DIB'] = None
    f_dict['2_bind_prefix'] = None
    # 'O_' => other film properties
    f_dict['O_min_ratio'] = None
    f_dict['O_min_ratio_hkl'] = None
    f_dict['O_MCIA'] = None
    f_dict['O_MCIA_hkl'] = None
    f_dict['O_max_ASO'] = None
    f_dict['O_max_ASO_hkl'] = None

    return f_dict


def move_top_to_2nd_DIB(film_data):
    """Function that updates second best DIB film_data with values from
        top DIB film data - that is about to be replaced.

    """
    film_data['2_max_DIB'] = film_data['1_max_DIB']
    film_data['2_f_hkl'] = film_data['1_f_hkl']
    film_data['2_config'] = film_data['1_config']
    film_data['2_f_UC_area'] = film_data['1_f_UC_area']
    film_data['2_aso'] = film_data['1_aso']
    film_data['2_area'] = film_data['1_area']
    film_data['2_rel.area'] = film_data['1_rel.area']
    film_data['2_new_area'] = film_data['1_new_area']
    film_data['2_new_rel.area'] = film_data['1_new_rel.area']
    film_data['2_max_MM'] = film_data['1_max_MM']
    film_data['2_avg_MM'] = film_data['1_avg_MM']
    film_data['2_bind_prefix'] = film_data['1_bind_prefix']

    return film_data


def update_top_DIB(film_data, BP_data, DIB, DF):
    """Function that updates top DIB film_data with values in BP_data.

    """
    film_data['1_max_DIB'] = BP_data['DIB1']
    film_data['1_f_hkl'] = BP_data['film_hkl']
    film_data['1_config'] = BP_data['config']
    film_data['1_f_UC_area'] = BP_data['film_UC_area']
    film_data['1_aso'] = BP_data[DF]['aso']
    film_data['1_area'] = BP_data[DF]['area']
    film_data['1_rel.area'] = BP_data[DF]['rel.area']
    film_data['1_new_area'] = BP_data[DF]['new_area']
    film_data['1_new_rel.area'] = BP_data[DF]['new_rel.area']
    film_data['1_max_MM'] = BP_data[DF]['max_MM']
    film_data['1_avg_MM'] = BP_data[DF]['avg_MM']
    film_data['1_bind_prefix'] = BP_data['film']+"_"+str(BP_data['film_hkl'])+"_"+str(BP_data['config'])

    return film_data


def update_2nd_DIB(film_data, BP_data, DIB, DF):
    """Function that updates second best DIB film_data with values in BP_data.

    """
    film_data['2_max_DIB'] = BP_data[DIB]
    film_data['2_f_hkl'] = BP_data['film_hkl']
    film_data['2_config'] = BP_data['config']
    film_data['2_f_UC_area'] = BP_data['film_UC_area']
    film_data['2_aso'] = BP_data[DF]['aso']
    film_data['2_area'] = BP_data[DF]['area']
    film_data['2_rel.area'] = BP_data[DF]['rel.area']
    film_data['2_new_area'] = BP_data[DF]['new_area']
    film_data['2_new_rel.area'] = BP_data[DF]['new_rel.area']
    film_data['2_max_MM'] = BP_data[DF]['max_MM']
    film_data['2_avg_MM'] = BP_data[DF]['avg_MM']
    film_data['2_bind_prefix'] = BP_data['film']+"_"+str(BP_data['film_hkl'])+"_"+str(BP_data['config'])

    return film_data


def init_res_file_per_MOF(filename):
    """Creates and writes header to CSV with 1 line per MOF.

    """
    with open(filename, 'w') as f:
        f.write('substrate,')
        f.write('s_hkl,')
        f.write('s_UC_area,')
        f.write('film,')
        # '1_' => max DIB interface
        f.write('1_f_hkl,')
        f.write('1_config,')
        f.write('1_f_UC_area,')
        f.write('1_aso,')
        f.write('1_area,')
        f.write('1_rel.area,')
        f.write('1_new_area,')
        f.write('1_new_rel.area,')
        f.write('1_max_MM,')
        f.write('1_avg_MM,')
        f.write('1_max_DIB,')
        f.write('1_bind_prefix,')
        # '2_' => max DIB interface
        f.write('2_f_hkl,')
        f.write('2_config,')
        f.write('2_f_UC_area,')
        f.write('2_aso,')
        f.write('2_area,')
        f.write('2_rel.area,')
        f.write('2_new_area,')
        f.write('2_new_rel.area,')
        f.write('2_max_MM,')
        f.write('2_avg_MM,')
        f.write('2_max_DIB,')
        f.write('2_bind_prefix,')
        # 'O_' => other film properties
        f.write('O_min_ratio,')
        f.write('O_min_ratio_hkl,')
        f.write('O_MCIA,')
        f.write('O_MCIA_hkl,')
        f.write('O_max_ASO,')
        f.write('O_max_ASO_hkl')
        f.write('\n')


def write_per_MOF_line(data, filename):
    """Write data to CSV file with 1 line per MOF.

    """
    with open(filename, 'a') as f:
        f.write(str(data['surface'])+',')
        f.write(str(data['surface_hkl'])+',')
        f.write(str(data['s_UC_area'])+',')
        f.write(str(data['film'])+',')
        # '1_' => max DIB interface
        f.write(str(data['1_f_hkl'])+',')
        f.write(str(data['1_config'])+',')
        f.write(str(data['1_f_UC_area'])+',')
        f.write(str(data['1_aso'])+',')
        f.write(str(data['1_area'])+',')
        f.write(str(data['1_rel.area'])+',')
        f.write(str(data['1_new_area'])+',')
        f.write(str(data['1_new_rel.area'])+',')
        f.write(str(data['1_max_MM'])+',')
        f.write(str(data['1_avg_MM'])+',')
        f.write(str(data['1_max_DIB'])+',')
        f.write(str(data['1_bind_prefix'])+',')
        # '2_' => max DIB interface
        f.write(str(data['2_f_hkl'])+',')
        f.write(str(data['2_config'])+',')
        f.write(str(data['2_f_UC_area'])+',')
        f.write(str(data['2_aso'])+',')
        f.write(str(data['2_area'])+',')
        f.write(str(data['2_rel.area'])+',')
        f.write(str(data['2_new_area'])+',')
        f.write(str(data['2_new_rel.area'])+',')
        f.write(str(data['2_max_MM'])+',')
        f.write(str(data['2_avg_MM'])+',')
        f.write(str(data['2_max_DIB'])+',')
        f.write(str(data['2_bind_prefix'])+',')
        # 'O_' => other film properties
        f.write(str(data['O_min_ratio'])+',')
        f.write(str(data['O_min_ratio_hkl'])+',')
        f.write(str(data['O_MCIA'])+',')
        f.write(str(data['O_MCIA_hkl'])+',')
        f.write(str(data['O_max_ASO'])+',')
        f.write(str(data['O_max_ASO_hkl']))
        f.write('\n')


def collect_write_film_results(ASO_files, surface, surface_hkl, film,
                               tol_u, max_angle_tol, prefix,
                               res_file_BP,
                               res_file_MOF):
    """Collect + write the results for all Miller planes of a given film.

    Collects max and second best DIB + properties of those interfaces.
    Collects max ASO and minimium coincident match area.

    Keywords:

    Returns:

    """
    film_data = init_film_dict(surface, surface_hkl, film)

    for int_f in ASO_files:
        if os.path.isfile(int_f) is False:
            continue
        if 'i_' in int_f and 'i_' not in film:
            # implies we are checking the non-interpen version of the film
            continue
        name = int_f.replace("_ASO_match.data", "")
        name = name.replace(prefix, "")
        f_hkl = name.split("_")[-2]
        config = name.split("_")[-1]
        BP_data = init_BP_dict(surface, surface_hkl, film,
                               f_hkl, config)
        print("--------------------------------------------------")
        print("doing hkl:", f_hkl, "config:", config)
        print("--------------------------------------------------")
        # get UC area ratio of binding plane
        s_UC, f_UC = get_miller_UCA(int_f)
        BP_data['surface_UC_area'] = s_UC
        BP_data['film_UC_area'] = f_UC
        if s_UC == 0:
            continue
        ratio = f_UC / s_UC
        BP_data['ratio'] = ratio
        # get maximum ASO of binding plane
        max_ASO = get_miller_ASO(int_f)
        BP_data['max_ASO'] = max_ASO
        # get MCIA of binding plane
        MCIA = get_miller_MCIA(int_f)
        BP_data['MCIA'] = MCIA
        # get max and second best DIB of binding plane
        DIB1, int1, DIB2, int2 = get_miller_DIBS(int_f, film, f_hkl,
                                                 config, surface,
                                                 surface_hkl, tol_u,
                                                 max_angle_tol,
                                                 viz=False,
                                                 verbose=False)
        # output binding plane specific data
        BP_data['DIB1'] = DIB1
        BP_data['int1'] = int1
        BP_data['DIB2'] = DIB2
        BP_data['int2'] = int2
        if MCIA > 0:
            write_per_BP_line(BP_data, res_file_BP)
        if DIB2 > DIB1:
            print("FATAL ERROR - DIB2 > DIB1 -- EXITTING.")
            import sys
            sys.exit()

        film_data['s_UC_area'] = s_UC

        # check other film properties
        if film_data['O_min_ratio'] is None:
            # no entry -- update
            film_data['O_min_ratio'] = BP_data['ratio']
            film_data['O_min_ratio_hkl'] = BP_data['film_hkl']
        elif film_data['O_min_ratio'] > BP_data['ratio']:
            # improved entry -- update
            film_data['O_min_ratio'] = BP_data['ratio']
            film_data['O_min_ratio_hkl'] = BP_data['film_hkl']
        if film_data['O_MCIA'] is None:
            # no entry -- update
            film_data['O_MCIA'] = BP_data['MCIA']
            film_data['O_MCIA_hkl'] = BP_data['film_hkl']
        elif film_data['O_MCIA'] > BP_data['MCIA']:
            # improved entry -- update
            film_data['O_MCIA'] = BP_data['MCIA']
            film_data['O_MCIA_hkl'] = BP_data['film_hkl']
        if film_data['O_max_ASO'] is None:
            # no entry -- update
            film_data['O_max_ASO'] = BP_data['max_ASO']
            film_data['O_max_ASO_hkl'] = BP_data['film_hkl']
        elif film_data['O_max_ASO'] < BP_data['max_ASO']:
            # improved entry -- update
            film_data['O_max_ASO'] = BP_data['max_ASO']
            film_data['O_max_ASO_hkl'] = BP_data['film_hkl']

        # check if the max DIB of this binding plane should be kept
        # any entries for max interface?
        if BP_data['int1'].empty is False:
            if film_data['1_max_DIB'] is None:
                # write DIB1 to top spot
                update_top_DIB(film_data, BP_data, DIB='DIB1', DF='int1')
            # there are entries for max interface
            # is this an improved result?
            elif film_data['1_max_DIB'] < BP_data['DIB1']:
                # move top DIB to second spot
                move_top_to_2nd_DIB(film_data)
                # update top DIB
                update_top_DIB(film_data, BP_data, DIB='DIB1', DF='int1')
            # any entries for second interface?
            elif film_data['2_max_DIB'] is None:
                # write DIB1 to second spot
                update_2nd_DIB(film_data, BP_data, DIB='DIB1', DF='int1')
            # is this an improved result for second spot?
            elif film_data['2_max_DIB'] < BP_data['DIB1']:
                # update second best DIB
                update_2nd_DIB(film_data, BP_data, DIB='DIB1', DF='int1')

        # check if the second best DIB of this binding plane should be kept
        # we can assume that DIB2 can not possibly replace top spot!
        # any entries for second interface?
        if BP_data['int2'].empty is False:
            if film_data['2_max_DIB'] is None:
                # write DIB1 to second spot
                update_2nd_DIB(film_data, BP_data, DIB='DIB2', DF='int2')
            # is this an improved result for second spot?
            elif film_data['2_max_DIB'] < BP_data['DIB2']:
                # update second best DIB
                update_2nd_DIB(film_data, BP_data, DIB='DIB2', DF='int2')

    # if the max DIB and 2nd DIB are both 0, it implies that no
    # matching supercells existed for all Miller planes and all binding planes
    if film_data['1_max_DIB'] is None:
        if film_data['2_max_DIB'] is None:
            print("------------------------------------")
            print('no matching lattices were found for Miller planes')
            print('with desired binding sites')

    print("--------------------------------------------------")
    print("film max DIB:", film_data['1_max_DIB'],
          'for hkl =', film_data['1_f_hkl'])
    print("film second best DIB:", film_data['2_max_DIB'],
          'for hkl =', film_data['2_f_hkl'])
    print("--------------------------------------------------")
    print("----------------------------------------------------------")
    # write film line
    write_per_MOF_line(film_data, res_file_MOF)


def check_for_two_res_files(result_file):
    """Get two result files from parameter set in param file.

    """
    # the results for each binding plane for all MOFs
    res_file_BP = result_file.replace(".csv", "_per_BP.csv")
    # the final results (considering all binding planes) for each MOF
    res_file_MOF = result_file.replace(".csv", "_per_MOF.csv")
    # read in data
    if os.path.isfile(res_file_MOF) is True:
        per_MOF_data = pd.read_csv(res_file_MOF)
    else:
        print("run ao_rep_write.py to get the result files.")
        import sys.exit
        exit("Exitting")
    if os.path.isfile(res_file_BP) is True:
        per_BP_data = pd.read_csv(res_file_BP)
    else:
        print("run ao_rep_write.py to get the result files.")
        import sys.exit
        exit("Exitting")

    return res_file_BP, per_BP_data, res_file_MOF, per_MOF_data
