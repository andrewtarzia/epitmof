#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Set of functions used throughout the screening algorithm.
"""

import os
import numpy as np
from collections import Counter

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def convert_ASE_to_XYZ(ASE_struct, xyz_name):
    """Obtain .xyz file from ASE Atoms object.

    """
    ASE_struct.write(xyz_name, format='xyz')


def convert_ASE_to_PDB(ASE_struct, pdb_name):
    """Obtain .pdb file from ASE Atoms object.

    """
    ASE_struct.write(pdb_name)


def run_ovito_script(script_name, ASE_struct, pdb_name,
                     ovito_loc, script_loc,
                     out_name, curr_dir):
    """Runs a pre written OVITO script on ASE Atoms object.

    written 17/9/17:
        converts ASE_struct to xyz (xyz_name)
        runs an ovito script given by its name
        outputs figures as out_name
        removes xyz file + settings file
    modified 18/9/17:
        converts to pdb not xyz.
        added curr_dir
    """
    convert_ASE_to_PDB(ASE_struct, pdb_name)
    # write settings file
    with open('temp_settings.txt', 'w') as f:
        f.write(pdb_name+' '+out_name)
    # move ovito script into working directory
    os.system('cp '+script_loc+'/'+script_name+' '+curr_dir)
    # run ovito
    os.system(ovito_loc+" "+script_name)
    # remove pdb and settings file
    # os.system('rm '+pdb_name)
    # os.system('rm temp_settings.txt')
    # os.system('rm '+script_name)


def comb_reader(file):
    """
    Reads supercell combination from file.

    Arguments:
        file (str) - file name

    Returns:
        final_dict (dict) - combination dictionary

    Depracated.
    """
    with open(file, 'r') as f:
        lines = f.readlines()

    for line in lines:
        l = line.rstrip().split(",")
        if l[0] == 'match_area':
            match_area = float(l[1])
        if l[0] == 'film_sl_vecs':
            film_sl_vecs = [[float(l[1].replace('[', '')),
                             float(l[2]),
                             float(l[3].replace(']', ''))],
                            [float(l[4].replace('[', '')),
                             float(l[5]),
                             float(l[6].replace(']', ''))]]
        if l[0] == 'sub_sl_vecs':
            sub_sl_vecs = [[float(l[1].replace('[', '')),
                            float(l[2]),
                            float(l[3].replace(']', ''))],
                           [float(l[4].replace('[', '')),
                            float(l[5]),
                            float(l[6].replace(']', ''))]]
        if l[0] == 'sub_vecs':
            sub_vecs = [[float(l[1].replace('[', '')),
                         float(l[2]),
                         float(l[3].replace(']', ''))],
                        [float(l[4].replace('[', '')),
                         float(l[5]),
                         float(l[6].replace(']', ''))]]
        if l[0] == 'film_vecs':
            film_vecs = [[float(l[1].replace('[', '')),
                          float(l[2]),
                          float(l[3].replace(']', ''))],
                         [float(l[4].replace('[', '')),
                          float(l[5]),
                          float(l[6].replace(']', ''))]]

    final_dict = {}
    final_dict['sub_vecs'] = sub_vecs
    final_dict['sub_sl_vecs'] = sub_sl_vecs
    final_dict['film_vecs'] = film_vecs
    final_dict['film_sl_vecs'] = film_sl_vecs
    final_dict['match_area'] = match_area
    return final_dict


def miller_reader(miller_string):
    """Reads hkl string and applies formatting rules to output [h, k, l].

    Takes a miller string and confirms that a frontward 0 is replaced if
    removed can assume that if len millers isn't 3 then a frontward 0 was
    removed
    """
    if '-' not in miller_string:
        if len(miller_string) == 3:
            # miller_string
            return [int(miller_string[0]), int(miller_string[1]),
                    int(miller_string[2])]
        elif len(miller_string) == 2:
            # '0'+miller_string
            return [0, int(miller_string[0]), int(miller_string[1])]
        elif len(miller_string) == 1:
            # '00'+miller_string
            return [0, 0, int(miller_string[0])]
    else:
        if Counter(miller_string)['-'] == 1:
            mill_list = miller_string.split('-')
            neg_index = miller_string.index('-')
            mill_list_sep = [int(i) for i in ''.join(mill_list)]
            # implies the negative is within a full set
            if len(''.join(mill_list)) == 3:
                mill_list_sep[neg_index] = '-'+str(mill_list_sep[neg_index])
                return [int(mill_list_sep[0]), int(mill_list_sep[1]),
                        int(mill_list_sep[2])]
            elif len(''.join(mill_list)) == 2:
                mill_list_sep[neg_index] = '-'+str(mill_list_sep[neg_index])
                return [0, int(mill_list_sep[0]), int(mill_list_sep[1])]
            elif len(''.join(mill_list)) == 1:
                mill_list_sep[neg_index] = '-'+str(mill_list_sep[neg_index])
                return [0, 0, int(mill_list_sep[0])]
        else:
            mill_list = miller_string.split('-')
            neg_index = []
            mill_list_sep = [int(i) for i in ''.join(mill_list)]
            for I in np.arange(len(miller_string)):
                if miller_string[I] == '-':
                    neg_index.append(I)
            # implies the negative is within a full set
            if len(''.join(mill_list)) == 3:
                for i in np.arange(len(neg_index)):
                    mill_list_sep[neg_index[i]-i] = '-'+str(
                            mill_list_sep[neg_index[i]-i])
                return [int(mill_list_sep[0]), int(mill_list_sep[1]),
                        int(mill_list_sep[2])]
            elif len(''.join(mill_list)) == 2:
                for i in np.arange(len(neg_index)):
                    mill_list_sep[neg_index[i]-i] = '-'+str(
                            mill_list_sep[neg_index[i]-i])
                return [0, int(mill_list_sep[0]), int(mill_list_sep[1])]
            elif len(''.join(mill_list)) == 1:
                for i in np.arange(len(neg_index)):
                    mill_list_sep[neg_index[i]-i] = '-'+str(
                            mill_list_sep[neg_index[i]-i])
                return [0, 0, int(mill_list_sep[0])]


def read_parameter_file(filename):
    """
    Read in user made parameter file to dictionary.

    Arguments:
        filename (str) - file name

    Returns:
        param_dict (dict) - dictionary of parameters

    """
    param_dict = {}
    with open(filename, 'r') as f:
        for line in f:
            # comment lines!
            if '#' not in line and len(line) > 0:
                (key, val) = line.split(':')
                # if val is a list
                if ',' in val:
                    new_val = [i.rstrip() for i in val.split(",")]
                else:
                    new_val = [val.rstrip()]
                param_dict[key] = new_val
    return param_dict


def print_welcome_message(cifs=None):
    """Print the welcome message.

    """
    print("==================================================================")
    print("Welcome to epitCIF screening!")
    print("==================================================================")
    if cifs is not None:
        if len(cifs) > 10:
            print("There are", len(cifs), "CIFs")
        else:
            print("CIFs:")
            print(cifs)
