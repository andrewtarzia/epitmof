#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses screening result for CORE MOF database specifically.

Produces Figure 5 in manuscript.
"""

import pandas as pd
import time
import glob
import numpy as np
from collections import Counter
import argparse
import matplotlib
matplotlib.use('agg')
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import general_functions as genf
import analysis_output_functions as aoo
import ao_plot_fn as aoplt

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# read in CIF TODO list as dataframe
CIF_TODO_files_step_1 = glob.glob(param_dict['TODO_name'][0]+"_step_1_*.csv")
CIF_TODO_files_step_3 = glob.glob(param_dict['TODO_name'][0]+"_step_3_*.csv")

# porosity file
porosity_file = param_dict['porosity_csv'][0]

# topology file
topo_file = param_dict['topology_csv'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

# plot with topology set features?
plot_topo = False
if param_dict['topology_switch'][0] == 'T':
    plot_topo = True
if plot_topo is True:
    topo_data = pd.read_csv(topo_file)
    # ignoring unknown topologies ("-")
    counter = Counter([i for i in list(topo_data['topology']) if i != '-'])
    unknown_count = len([i for i in list(topo_data['topology']) if i == '-'])
    total_count = len(list(topo_data['topology']))
    # set colours for selected topologies only
    topo_select_names = ['tbo', 'pcu', 'dia', 'rob', 'fof', 'nbo', 'tfc']

    # ['dia', 'pcu', 'tbo', 'rob', 'fof', 'nbo', 'tfc']
    topo_select = [(i, counter[i]) for i in topo_select_names]
    # print(topo_select_names)
    # print(topo_select)
    # topo_select = counter.most_common()[:6]
    topo_select_names = [i[0] for i in topo_select]
    topo_select_counts = [i[1] for i in topo_select]
    other_count = len([i for i in list(topo_data['topology'])
                       if i != '-' and i not in topo_select_names])
    if unknown_count == 0:
        topo_colours = cm.Paired(np.linspace(0, 0.5, len(topo_select) + 1))
        topo_markers = ['o', 'X', 'P', 'v', '^', '<', '>', 's', 'p', 'D']
    else:
        topo_colours = cm.Paired(np.linspace(0, 0.5, len(topo_select) + 2))
        topo_markers = ['o', 'X', 'P', 'v', '^', '<', '>', 's', 'p', 'D']

# set standard plot values
standard_plot_values = {'i':
                        {'s': 80, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 0.8, 'marker': 'o'}}

# separate results into two files
res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

# list of interpenetrated CIFs
interpen_films = []
films = list(set(per_MOF_data['film']))
for i in films:
    if 'i_'+i+'_0' in films:
        interpen_films.append(i)

# define figures
fig = plt.figure(figsize=(8, 5))
# define sub plots of figure
ax1 = plt.subplot()  # 211)
# ax2 = plt.subplot(212)

# define colour map
# set mid point based on DIB threshold of 0.67
DIB_thresh = 0.67

new_cmap = aoplt.define_plot_cmap(fig, ax1, DIB_thresh, cm.RdBu,
                                  ticks=[0, 0.25, 0.5, DIB_thresh, 0.75, 1.0],
                                  labels=['0', '0.25', '0.5',
                                          str(DIB_thresh), '0.75', '1.0'],
                                  cmap_label='$\Delta$IB$_2$')

fig1, ax11 = plt.subplots(figsize=(5, 5))

# add a band for DIB figure
ax1.axhline(0.67, c='r', linestyle='--', alpha=0.5)
ax1.axhspan(0.6, 0.77, facecolor='grey', alpha=0.3)

for idx, row in per_MOF_data.iterrows():
    film = row['film']
    print('film:', film)
    # want to ignore interpenetrated versions of CIFs
    if film in interpen_films:
        continue
    # check if any data was collected
    if row['1_max_DIB'] == 'None':
        continue
    # assign DIB values
    else:
        DIB1 = float(row['1_max_DIB'])
        if row['2_max_DIB'] == 'None':
            DIB2 = 0
        else:
            DIB2 = float(row['2_max_DIB'])

    # top candidate?
    if DIB1 >= DIB_thresh:
        if DIB2 >= DIB_thresh:
            alpha = 0.6
        else:
            print('top candidate:', DIB1)
            alpha = 1.0
    else:
        alpha = 0.6

    # set plot values
    if film[:2] == 'i_':
        plt_val = standard_plot_values['i']
        no = film.split("_")[-1]
        short_name = film.replace('i_', '').replace("_"+no, '')
    else:
        plt_val = standard_plot_values['i']
        short_name = film
    if plot_topo is False:
        C = plt_val['c']
        M = plt_val['marker']
    elif plot_topo is True:
        # change marker to match topologies
        f_topo = topo_data[topo_data['film'] == short_name]
        topo = f_topo['topology'].iloc[0]
        if topo in topo_select_names:
            C = topo_colours[topo_select_names.index(topo)]
            M = topo_markers[topo_select_names.index(topo)]
        elif topo == '-':
            # unknown
            C = topo_colours[-1]
            M = topo_markers[-1]
        else:
            # other
            if unknown_count == 0:
                C = topo_colours[-1]
                M = topo_markers[-1]
            else:
                C = topo_colours[-2]
                M = topo_markers[-2]

    ax1.scatter(float(row['1_f_UC_area'])/float(row['s_UC_area']),
                DIB1,
                c=new_cmap(DIB2),
                marker=M,
                s=plt_val['s'],
                alpha=alpha,
                edgecolors=plt_val['edgecolors'],
                cmap=new_cmap)
    ax11.scatter(float(row['O_max_ASO']),
                 DIB1,
                 c=C,
                 marker=plt_val['marker'],
                 s=plt_val['s'],
                 alpha=alpha,
                 edgecolors=plt_val['edgecolors'])

aoplt.define_plot_variables(ax1,
                            title='',
                            xtitle='ratio of unit cell areas',
                            ytitle='$\Delta$IB',
                            xlim=(0, 100),
                            ylim=(0, 1.15))
aoplt.define_plot_variables(ax11,
                            title='',
                            ytitle='$\Delta$IB',
                            xtitle="ASO",
                            xlim=(0, 1.1),
                            ylim=(0, 1.1))
x = np.linspace(0, 1.5, 5)
ax11.plot(x, x, c='k', alpha=0.5)

# decoy legend
for i, j in enumerate(topo_select):
    M = topo_markers[i]
    perc = (j[1] / total_count) * 100
    ax1.scatter(-1000,
                -1000,
                c='none',
                marker=M,
                s=plt_val['s'],
                alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'],
                label=j[0]+' ('+str(round(perc, 1))+'%)')
M = topo_markers[-2]
perc = (other_count / total_count) * 100
ax1.scatter(-1000,
            -1000,
            c='none',
            marker=M,
            s=plt_val['s'],
            alpha=plt_val['alpha'],
            edgecolors=plt_val['edgecolors'],
            label='other ('+str(round(perc, 1))+'%)')
M = topo_markers[-1]
perc = (unknown_count / total_count) * 100
ax1.scatter(-1000,
            -1000,
            c='none',
            marker=M,
            s=plt_val['s'],
            alpha=plt_val['alpha'],
            edgecolors=plt_val['edgecolors'],
            label='unknown ('+str(round(perc, 1))+'%)')
ax1.legend(fancybox=True, ncol=2, bbox_to_anchor=(0.265, 0.655),
           fontsize=12)

##############################################################################
# save fig
fig.tight_layout()
fig.savefig("core_main_figure.pdf",
            bbox_inches='tight', dpi=720)
fig1.tight_layout()
fig1.savefig("core_ASO_DIB_parity.pdf",
             bbox_inches='tight')

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
