#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used for the MC simulations to calculate the maximum ASO of a super
cell pair.
"""

import numpy as np
from scipy.spatial import distance
from ase import Atoms

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def ASE_scoring_function_opt_v1(struct_f, struct_s, tol, Sa, Sb):
    """Output ASO score of interface.

    Keyword arguments:
        struct_f (ASE Atoms) - film binding plane
        struct_s (ASE Atoms) - substrate binding plane
        tol (float) - binding tolerance used in MC simulation
        Sa (int) - number of atoms in substrate binding plane
        Sb (int) - number of atoms in film binding plane

    Returns:
        ASO (float) - ASO for this MC step
    """
    f_coords = struct_f.get_positions()
    s_coords = struct_s.get_positions()
    Sc = ASE_Sc_determ_vec(f_coords, s_coords, tol)
    ASO = Sc / (Sb)
    return ASO


def ASE_stepper(resolution, x_displacement, y_displacement, x_CW, y_CW):
    """Determine step in X and Y direction for MC algorithm.

    Not used in manuscript.

    Keyword arguments:
        resolution (float) - current maximum displacement value
        x_displacement (float) cumulative displace in x dimension up to this
            step
        y_displacement (float) cumulative displace in y dimension up to this
            step
        x_CW (float) - approximate cell width in x dimension
        y_CW (float) - approximate cell width in y dimension

    Returns:
        step (list) - step in [x, y, z] dimensions
    """
    step_x = (resolution[0] * ((np.random.random() - 0.5) * 2))
    if x_displacement > x_CW:
        # want to use this step but to bring the displacement within the CW
        if x_displacement > 0 and step_x > 0:
            step_x = -step_x
        elif x_displacement < 0 and step_x < 0:
            step_x = -step_x
    step_y = (resolution[1] * ((np.random.random() - 0.5) * 2))
    if y_displacement > y_CW:
        # want to use this step but to bring the displacement within the CW
        if y_displacement > 0 and step_y > 0:
            step_y = -step_y
        elif y_displacement < 0 and step_y < 0:
            step_y = -step_y
    step = [step_x, step_y, 0]
    return step


def ASE_stepper_cell(resolution, x_displacement, y_displacement, x_CW, y_CW,
                     f_cell_vectors):
    """Determine step in supercell unit vector directions for MC algorithm.

    Keyword arguments:
        resolution (float) - current maximum displacement value
        x_displacement (float) cumulative displace in x dimension up to this
            step
        y_displacement (float) cumulative displace in y dimension up to this
            step
        x_CW (float) - approximate cell width in x dimension
        y_CW (float) - approximate cell width in y dimension
        film_cell_vectors (2x3 array) - supercell lattice vectors of film

    Returns:
        step (list) - step in [x, y, z] dimensions
    """

    # get unit vectors of cell vectors a and b
    a_v, b_v = np.asarray(f_cell_vectors[0]), np.asarray(f_cell_vectors[1])
    a_uv = a_v / np.linalg.norm(a_v)
    b_uv = b_v / np.linalg.norm(b_v)

    # step = reoluation * random number between -1 and 1 * unit vectors of cell
    step_a = (resolution[0] * ((np.random.random() - 0.5) * 2)) * a_uv
    step_b = (resolution[0] * ((np.random.random() - 0.5) * 2)) * b_uv

    # move both vectors into X and Y components and check for displacement
    # past CW
    step_x = step_a[0] + step_b[0]
    if x_displacement > x_CW:
        # want to use this step to bring the displacement within the CW
        if x_displacement > 0 and step_x > 0:
            step_x = -step_x
        elif x_displacement < 0 and step_x < 0:
            step_x = -step_x
    step_y = step_a[1] + step_b[1]
    if y_displacement > y_CW:
        # want to use this step to bring the displacement within the CW
        if y_displacement > 0 and step_y > 0:
            step_y = -step_y
        elif y_displacement < 0 and step_y < 0:
            step_y = -step_y
    step = [step_x, step_y, 0]
    return step


def ASE_Sc_determ_vec(film_coords, substrate_coords, tol):
    """Determine cooincident atoms using scipy distance array.

    Keyword arguments:
        film_coords (list) - list of coordinates of atom positions in film
            structure
        substrate_coords (list) - list of coordinates of atom positions in
            substrate structure
        tol (float) - binding tolerance used in MC simulation

    Returns:
        Sc (float) - number of coincident film atoms
    """
    dist_array = distance.cdist(np.asarray(film_coords),
                                np.asarray(substrate_coords), 'euclidean')
    # use np.where to obtain the ROWS then COLUMNS of matches
    # ROWS = FILM INDICES
    # COLUMNS = SUBSTRATE INDICES
    f_ind, s_ind = np.where(dist_array**2 <= tol**2)
    # HERE WE COULD APPLY ANY FURTHER ANALYSIS DESIRED AND EVEN
    # OUTPUT THESE ATOM IDS BUT FOR NOW (18/9/17) I WILL JUST OUTPUT THE COUNT!
    # NEW CODE IS SLOWER BUT CONSIDERS DOUBLE COUNTING WITH
    # RESPECT TO SUBSTRATE!
    counted_s = []
    counted_f = []
    count_coinc = 0
    for f, s in zip(f_ind, s_ind):
        if f not in counted_f and s not in counted_s:
            count_coinc += 1
            counted_f.append(f)
            counted_s.append(s)
    Sc = count_coinc
    return Sc


def ASO_MC_alg(steps, x_moves, y_moves,
               struct_f, struct_s, Sa, Sb, beta, tol,
               resolution, film_cell_vectors, output_steps=False):
    """Calculate the maximum ASO with a Metropolis MC algorithm.

    Keyword arguments:
        steps (int) - number of MC steps to run
        x_moves (list(floats)) - possible translations for initial randomized
            starting move
        y_moves (list(floats)) - possible translations for initial randomized
            starting move
        struct_f (ASE Atoms) - film binding plane
        struct_s (ASE Atoms) - substrate binding plane
        Sa (int) - number of atoms in substrate binding plane
        Sb (int) - number of atoms in film binding plane
        beta (float) - inverse temperature parameter used for MC simulation
        tol (float) - binding tolerance used in MC simulation
        resolution (float) - initial maximum displacement value
        film_cell_vectors (2x3 array) - supercell lattice vectors of film
        output_steps (bool) - output extended information (default False)

    Returns:
        res (list) - ASO at each step of MC simulation
        max_ASO (float) - maximum ASO obtained in all steps of MC simulation
        out_f_struct (ASE Atoms) - final film binding plane
        out_s_struct (ASE Atoms) - final substrate binding plane
        x_disps (list) - cumultative displacement in x dimension for each step
        y_disps (list) - cumultative displacement in y dimension for each step

    """
    if output_steps is True:
        from ase.visualize import view
    out_f_struct = Atoms()
    out_s_struct = Atoms()
    res = []
    xys = []  # these are the steps taken each loop
    acc_50 = []
    step_size = []
    # collect the overall displacement for the MC run
    x_disps = []
    y_disps = []
    x_disp = 0
    y_disp = 0

    # set resolution as tuple of input number
    resolution = [resolution, resolution]
    # set half cell width (HCW) as max resolution to be used
    HCW = [(1/2) * (film_cell_vectors[0][0]+film_cell_vectors[1][0]),
           (1/2) * (film_cell_vectors[0][1]+film_cell_vectors[1][1])]

    # randomize the starting point
    ith = np.random.randint(len(x_moves))
    jth = np.random.randint(len(y_moves))
    pos = [x_moves[ith], y_moves[jth], 0]
    if output_steps is True:
        print(pos)
    struct_f.translate(pos)
    if output_steps is True:
        print("initial move")
        view(struct_f + struct_s)
        input("done?")
    # wrap the starting point - therefore cumulative movement at i = 0 is (0,0)
    # note we do not wrap at any other stage throughout this algorithm
    # as we do not want discrepancies between the film and substrate super
    # cell to play any role.
    struct_f.wrap()
    # make sure all Z coodinates are 0 after wrap
    for atom in struct_f:
        atom.z = 0
    for atom in struct_s:
        atom.z = 0
    if output_steps is True:
        print("wrap film")
        view(struct_f + struct_s)
        input("done?")
    xys.append(pos)
    _ASO = ASE_scoring_function_opt_v1(struct_f, struct_s, tol, Sa, Sb)
    if output_steps is True:
        print(_ASO)
    no_accepted = 0
    # every 50 steps (no matter the total number of steps)
    # we want to check if the acceptance ratio is too high or
    # too low and adjust the resolution accordingly
    target_acc_rat = 0.2
    step_counter = 0
    temp_acc_rat = 0
    max_ASO = 0
    for tt in np.arange(steps):
        # get new position
        step = ASE_stepper_cell(resolution, x_disp, y_disp, HCW[0]*2, HCW[1]*2,
                                film_cell_vectors)
        struct_f.translate(step)
        if output_steps is True:
            print("step", tt, ':', step)
            view(struct_f + struct_s)
            input("done?")

        new_ASO = ASE_scoring_function_opt_v1(struct_f, struct_s, tol, Sa, Sb)
        if output_steps is True:
            print('new ASO:', new_ASO, 'old ASO:', _ASO)
        if new_ASO > max_ASO:
            # update max ASO output structure
            out_f_struct = struct_f.copy()
            out_s_struct = struct_s.copy()
            max_ASO = new_ASO
        if new_ASO > _ASO:
            # always accept downhill step
            _ASO = new_ASO
            no_accepted += 1
            temp_acc_rat += 1
            x_disp += step[0]
            y_disp += step[1]
            x_disps.append(x_disp)
            y_disps.append(y_disp)
        else:
            # assume energy proportional to inverse ASO
            # calculate psuedo boltzmann weight with beta
            b_w = np.exp(-beta*(-new_ASO+_ASO))
            rr = np.random.random()
            if b_w > rr:
                # accept uphill step with random probability
                _ASO = new_ASO
                no_accepted += 1
                temp_acc_rat += 1
                x_disp += step[0]
                y_disp += step[1]
                x_disps.append(x_disp)
                y_disps.append(y_disp)
            else:
                if output_steps is True:
                    print('step reversed')
                # we need to reverse the move that has been under taken
                # before trying the next move
                struct_f.translate([-step[0], -step[1], 0])
                x_disps.append(x_disp)
                y_disps.append(y_disp)
        res.append(_ASO)
        xys.append(step)
        step_size.append(resolution)
        step_counter += 1
        if step_counter == 50:
            temp_acc_rat = temp_acc_rat / 50

            if temp_acc_rat > target_acc_rat:
                # do not let resolution in each dimension be > half cell width
                if resolution[0] * 2 <= HCW[0]:
                    resolution[0] *= 2
                if resolution[1] * 2 <= HCW[1]:
                    resolution[1] *= 2
            elif temp_acc_rat < target_acc_rat:
                resolution[0] /= 2
                resolution[1] /= 2
            acc_50.append(temp_acc_rat)
            step_counter = 0
            temp_acc_rat = 0

    max_ASO = max(res)
    return res, max_ASO, out_f_struct, out_s_struct, x_disps, y_disps
