#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses screening result for parameterisation database specifically.

Produces Figure2 in manuscript.
"""

import time
import sys
import numpy as np
import argparse
import general_functions as genf
import analysis_output_functions as aoo
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()


def define_plot_variables(ax, title, ytitle, xtitle, xlim, ylim):
    """
    Series of matplotlib pyplot settings to make all plots unitform.
    """
    # Set number of ticks for x-axis
    ax.tick_params(axis='both', which='major', labelsize=16)

    ax.set_ylabel(ytitle, fontsize=16)
    # ax.legend([y, n], ['aligned', 'not aligned'], loc=4, fancybox=True)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xticklabels(['aligned', 'not aligned'])
    ax.set_xticks([0.25, 0.75])


def define_parity_plot_variables(ax, title, ytitle, xtitle, xlim, ylim):
    """
    Series of matplotlib pyplot settings to make all plots unitform.
    """
    # Set number of ticks for x-axis
    ax.tick_params(axis='both', which='major', labelsize=16)

    ax.set_xlabel(xtitle, fontsize=16)
    ax.set_ylabel(ytitle, fontsize=16)
    # ax.legend([y, n], ['aligned', 'not aligned'], loc=4, fancybox=True)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)


# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

# list of MOFS that grow aligned on Cu(OH)2
# and the miller plane they are shown to grow from
experimental_mofs = {'CuNH2BDC_DABCO': '100',
                     'CuBPDC': '100',
                     'CuBDC_DABCO': '100',
                     #'CuBDC_BPY': '100',
                     'CuNDC_DABCO': '100',
                     'CuBDC2': '100',
                     'CuBDCa': '100',
                     'CuNDC': '100',
                     'CuBDC4': '100',
                     'CuBDCb': '100',
                     'CuBDC_DABCO_clean_out': '100',
                     'CuBDC_DABCO2': '100',
                     'CuBDC_DABCOa': '100',
                     'CuBDC_DABCOf': '100',
                     'CuBDC_DABCOb': '100',
                     'CuBDC_DABCO3': '100',
                     #'NEJRUR_clean': '100',
                     'i_NEJRUR_clean_0': '100',
                     'i_NEJRUR_clean_1': '100'
                     }

# colour per mof
colors = cm.tab20(np.linspace(0, 1, 10))

name_conversion_2 = {
        'CuBDC2': ('CuBDC-a', colors[0], 'o'),
        # 'CuBDCa': ('CuBDC-a', colors[0], 'o'),
        'CuBDC4': ('CuBDC-b', colors[1], 'o'),
        # 'CuBDCb': ('CuBDC-b', colors[1], 'o'),
        'CuBPDC': ('CuBPDC', colors[0], 'X'),
        'CuNDC': ('CuNDC', colors[0], 'P'),
        # 'CuBDC_DABCO_clean_out': ('CuBDC-DABCO-a', colors[0], 'v'),
        # 'CuBDC_DABCOa': ('CuBDC-DABCO-a', colors[0], 'v'),
        'CuBDC_DABCOf': ('CuBDC-DABCO-a', colors[0], 'v'),
        'CuBDC_DABCO2': ('CuBDC-DABCO-b', colors[1], 'v'),
        # 'CuBDC_DABCOb': ('CuBDC-DABCO-b', colors[1], 'v'),
        'i_NEJRUR_clean_0': ('CuBDC-BPY-i-1', colors[0], '^'),
        'i_NEJRUR_clean_1': ('CuBDC-BPY-i-2', colors[1], '^'),
        'CuTDC': ('CuTDC', colors[0], '<'),
        'CuBTC': ('CuBTC-a', colors[0], 'D'),
        # 'CuBTCa': ('CuBTC-a', colors[1], 'D'),
        'COD2300380_mod': ('CuBTC-b', colors[1], 'D'),
        'XAMDUM_clean': ('CuBTC-c', colors[2], 'D'),
        'XAMDUM01_clean': ('CuBTC-d', colors[3], 'D'),
        'XAMDUM02_clean': ('CuBTC-e', colors[4], 'D'),
        'XAMDUM03_clean': ('CuBTC-f', colors[5], 'D'),
        'XAMDUM04_clean': ('CuBTC-g', colors[6], 'D'),
        'XAMDUM05_clean': ('CuBTC-h', colors[7], 'D'),
        'XAMDUM06_clean': ('CuBTC-i', colors[8], 'D'),
        'XAMDUM07_clean': ('CuBTC-j', colors[9], 'D')
                     }

# set standard plot values for interpen and non-interpen case
standard_plot_values = {'i':
                        {'s': 180, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 1.0, 'marker': 'o'}}

# separate results into two files
res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

# spacing in X direction
dx = 0.15

# define figures
fig = plt.figure(figsize=(11, 12))
# define sub plots of figure
ax1 = plt.subplot(221)
ax2 = plt.subplot(222)
ax3 = plt.subplot(223)
ax4 = plt.subplot(224)

fig1, ax11 = plt.subplots(figsize=(5, 5))
fig2, ax21 = plt.subplots()  # figsize=(10, 10))
fig3, ax31 = plt.subplots()  # figsize=(10, 10))

# label
ax1.text(0.01, 0.92, '(a)',
         verticalalignment='bottom', horizontalalignment='left',
         transform=ax1.transAxes,
         fontsize=18)
ax2.text(0.01, 0.92, '(b)',
         verticalalignment='bottom', horizontalalignment='left',
         transform=ax2.transAxes,
         fontsize=18)
ax3.text(0.01, 0.92, '(c)',
         verticalalignment='bottom', horizontalalignment='left',
         transform=ax3.transAxes,
         fontsize=18)
ax4.text(0.01, 0.92, '(d)',
         verticalalignment='bottom', horizontalalignment='left',
         transform=ax4.transAxes,
         fontsize=18)

# add a band for DIB figure
ax4.axhline(0.67, c='r', linestyle='--', alpha=0.5)
ax4.axhspan(0.6, 0.77, facecolor='grey', alpha=0.3)

plt_val = standard_plot_values['i']

for idx, row in per_MOF_data.iterrows():
    film = row['film']
    print("film:", film)
    # only desired parameterization/expt films
    if film not in name_conversion_2.keys():
        print(film, 'not here')
        continue

    experimental_MCIA = False
    experimental_ratio = False
    experimental_DIB = False
    experimental_ASO = False
    if film in experimental_mofs.keys():
        if experimental_mofs[film] == str(row['1_f_hkl']):
            experimental_DIB = True
        if experimental_mofs[film] == str(row['O_max_ASO_hkl']):
            experimental_ASO = True
        if experimental_mofs[film] == str(row['O_MCIA_hkl']):
            experimental_MCIA = True
        if experimental_mofs[film] == str(row['O_min_ratio_hkl']):
            experimental_ratio = True
    try:
        film_converted, C, M = name_conversion_2[film]
    except KeyError:
        continue

    if experimental_DIB is True:
        D_DIB = 0.25
    else:
        D_DIB = 0.75
    if experimental_MCIA is True:
        D_MCIA = 0.25
    else:
        D_MCIA = 0.75
    if experimental_ratio is True:
        D_ratio = 0.25
    else:
        D_ratio = 0.75
    if experimental_ASO is True:
        D_ASO = 0.25
    else:
        D_ASO = 0.75

    # do all plots
    ax1.scatter(D_MCIA+(dx*(np.random.random() - 0.5) * 2),
                float(row['O_MCIA']), c=C, marker=M,
                s=plt_val['s'],
                alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

    ax2.scatter(D_ratio+(dx*(np.random.random() - 0.5) * 2),
                float(row['O_min_ratio']), c=C, marker=M,
                s=plt_val['s'],
                alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

    ax4.scatter(D_DIB+(dx*(np.random.random() - 0.5) * 2),
                float(row['1_max_DIB']), c=C, marker=M,
                s=plt_val['s'],
                alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

    ax3.scatter(D_ASO+(dx*(np.random.random() - 0.5) * 2),
                float(row['O_max_ASO']), c=C, marker=M,
                s=plt_val['s'],
                alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

    ax11.scatter(D_MCIA+(dx*(np.random.random() - 0.5) * 2),
                 float(row['O_MCIA']), c=C, marker=M,
                 s=plt_val['s'],
                 alpha=plt_val['alpha'],
                 edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

    ax21.scatter(float(row['1_area'])/float(row['1_f_UC_area']),
                 float(row['1_max_DIB']), c=C, marker=M,
                 s=plt_val['s'],
                 alpha=plt_val['alpha'],
                 edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

    ax31.scatter(float(row['1_area'])/float(row['s_UC_area']),
                 float(row['1_max_DIB']), c=C, marker=M,
                 s=plt_val['s'],
                 alpha=plt_val['alpha'],
                 edgecolors=plt_val['edgecolors'])  # ,label=film_converted)

# decoy legend
for i in name_conversion_2.values():
    label, c, m = i
    ax3.scatter(-1000, -1000, c=c,
                label=label, s=plt_val['s'], alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'],
                marker=m)

ax3.legend(fancybox=True, ncol=4, loc='lower left',
           bbox_to_anchor=(-0.25, -0.55),
           fontsize=16,
           # bbox_transform=fig.transFigure
           )

define_plot_variables(ax1,
                      title='',
                      xtitle='',
                      ytitle='minimum match area [$\mathrm{\AA^2}$]',
                      xlim=(0, 1),
                      ylim=(0, 1200))

define_plot_variables(ax2,
                      title='',
                      xtitle='',
                      # ytitle='MOF unit cell area / substrate unit cell area',
                      ytitle='ratio of unit cell areas',
                      xlim=(0, 1),
                      ylim=(0, 25))

define_plot_variables(ax4,
                      title='',
                      xtitle='',
                      ytitle='$\Delta$IB',
                      xlim=(0, 1),
                      ylim=(0, 1.15))

define_plot_variables(ax3,
                      title='',
                      xtitle='',
                      ytitle='ASO',
                      xlim=(0, 1),
                      ylim=(0, 1.15))

define_plot_variables(ax11,
                      title='',
                      ytitle='minimum match area [$\mathrm{\AA^2}$]',
                      xtitle='',
                      xlim=(0, 1),
                      ylim=(0, 1200))

define_parity_plot_variables(ax21,
                             title='',
                             # xtitle='match area [$\mathrm{\AA^2}$]',
                             xtitle='multiples of film unit cell',
                             ytitle='$\Delta$IB',
                             xlim=(0, 10),
                             ylim=(0, 1.1))

define_parity_plot_variables(ax31,
                             title='',
                             # xtitle='match area [$\mathrm{\AA^2}$]',
                             xtitle='multiples of substrate unit cell',
                             ytitle='$\Delta$IB',
                             xlim=(0, 100),
                             ylim=(0, 1.1))

##############################################################################
# save figures
# fig.tight_layout()
fig.savefig("param_figure_main.pdf",
            bbox_inches='tight', dpi=720)
fig1.tight_layout()
# fig1.savefig("param_figure_match_area.eps",
#              bbox_inches='tight', dpi=720)
fig1.savefig("param_figure_MCIA.pdf",
             bbox_inches='tight', dpi=720)
fig2.tight_layout()
fig2.savefig("param_figure_rel_film_area.pdf",
             bbox_inches='tight', dpi=720)
fig3.tight_layout()
fig3.savefig("param_figure_rel_sub_area.pdf",
             bbox_inches='tight', dpi=720)
sys.exit()
