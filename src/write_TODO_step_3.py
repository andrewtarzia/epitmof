#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Write the CIF_TODO.csv file for step 3 of the screening code
"""

import glob
import pandas as pd
from sys import exit
import argparse
import general_functions as genf

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

# parse CL arguments
parser = argparse.ArgumentParser(
        description="Write list of CIFs for screening algorithm")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# get number of CPUs
if param_dict['N_CPUs'][0] == 'None':
    print("please set the number of processes")
    print('exiting...')
    exit()
else:
    max_CPUS = int(param_dict['N_CPUs'][0])

# status meaning
# status = 0 : not tested
# status = 1 : did not pass chemistry test
# status = 2 : passed chemistry test
# status = 3 : did not pass lattice test
# status = 4 : passed lattice test
# status = 4a : binding planes built
# status = 5 : completed ASO screening

# read in surfaces and films from CIF_TODO_step_3 files and use the status
# to determine if it gets added
cifs = []
surfaces = []
for file in glob.glob(param_dict['TODO_name'][0]+"_step_1_*.csv"):
    data = pd.read_csv(file)
    for i, line in data.iterrows():
        surf = line['surface name']
        film = line['film name']
        status = line['status']
        if status == 4:
            if surf not in surfaces:
                surfaces.append(surf)
            if film not in cifs:
                cifs.append(film)

genf.print_welcome_message(cifs)

cifs_sep = {}
# separate all CIFs into different processors
for NP in range(max_CPUS):
    cifs_sep[str(NP)] = []

NP = 0
for m in cifs:
    cifs_sep[str(NP)].append(m)
    if NP+1 == max_CPUS:
        NP = 0
    else:
        NP += 1

print("======================================================================")
print("No. Processes:", max_CPUS)
print("CIFs per Process:", [len(cifs_sep[i]) for i in cifs_sep.keys()])
print("======================================================================")

for NP in range(max_CPUS):
    CSV_name = param_dict['TODO_name'][0]+"_step_3_"+str(NP)+".csv"

    with open(CSV_name, 'w') as f:
        f.write('surface name,film name,status\n')
        for n in surfaces:
            try:
                for m in cifs_sep[str(NP)]:
                    # write status = 4
                    f.write(str(n)+','+str(m)+',4\n')
            except KeyError:
                pass
