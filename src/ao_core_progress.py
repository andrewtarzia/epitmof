#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Produces Figure 4 in Manuscript - which plots the simulated porosity of MOFs in
the CORE MOF database as a function of their progress through the screening
algorithm.
"""

import pandas as pd
import time
import glob
import argparse
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import analysis_output_functions as aoo
import general_functions as genf

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# read in CIF TODO list as dataframe
CIF_TODO_files_step_1 = glob.glob(param_dict['TODO_name'][0]+"_step_1_*.csv")
CIF_TODO_files_step_3 = glob.glob(param_dict['TODO_name'][0]+"_step_3_*.csv")

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# porosity file
porosity_file = param_dict['porosity_csv'][0]

# making one figure for all CIFs in all DBs
fig, ax = plt.subplots()

Y_ = 'AV_frac [%]'
X_ = 'Free Sphere [Ang]'

standard_plot_values = {'5':
                        {'s': 40, 'edgecolors': 'k',
                         'alpha': 1.0, 'marker': 'o'},
                        '1':
                        {'s': 40, 'c': 'grey', 'edgecolors': 'none',
                         'alpha': 0.3, 'marker': 'o'},
                        '3':
                        {'s': 40, 'c': 'k', 'edgecolors': 'k',
                         'alpha': 0.6, 'marker': 'o'}}

print("Step 1:")
status_dict_step_1 = aoo.get_status_dict_step_1(CIF_TODO_files_step_1)
print("=================================================================")
print("Step 3:")
status_dict_step_3 = aoo.get_status_dict_step_3(CIF_TODO_files_step_3)
ASO_completed_films = sorted(status_dict_step_3['5'])

poro_ = pd.read_csv(porosity_file)

for stat in status_dict_step_1.keys():
    for cif in status_dict_step_1[stat]:
        # no interpen cases
        if cif[:2] != 'i_':
            if stat == '1' and cif not in ASO_completed_films:
                # get x value
                x = float(poro_[poro_['mof'] == cif][X_])
                # get y value
                y = float(poro_[poro_['mof'] == cif][Y_])
                # plot
                plt_val = standard_plot_values['1']
                ax.scatter(x, y,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'], c=plt_val['c'],
                           edgecolors=plt_val['edgecolors'],
                           marker=plt_val['marker'])
            elif stat == '3' and cif not in ASO_completed_films:
                # get x value
                x = float(poro_[poro_['mof'] == cif][X_])
                # get y value
                y = float(poro_[poro_['mof'] == cif][Y_])
                # plot
                plt_val = standard_plot_values['3']
                ax.scatter(x, y,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'], c=plt_val['c'],
                           edgecolors=plt_val['edgecolors'],
                           marker=plt_val['marker'])
            elif stat == '4' and cif in ASO_completed_films:
                # get x value
                x = float(poro_[poro_['mof'] == cif][X_])
                # get y value
                y = float(poro_[poro_['mof'] == cif][Y_])
                # plot
                plt_val = standard_plot_values['5']
                ax.scatter(x, y,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'], c='skyblue',
                           edgecolors=plt_val['edgecolors'],
                           marker=plt_val['marker'])

# decoy for legend
plt_val = standard_plot_values['1']
one = ax.scatter(-1000, -1000, c=plt_val['c'],
                 label='failed chemistry test',
                 s=plt_val['s'], alpha=plt_val['alpha'],
                 edgecolors=plt_val['edgecolors'],
                 marker=plt_val['marker'])

plt_val = standard_plot_values['3']
three = ax.scatter(-1000, -1000, c=plt_val['c'],
                   label='failed lattice test',
                   s=plt_val['s'], alpha=plt_val['alpha'],
                   edgecolors=plt_val['edgecolors'],
                   marker=plt_val['marker'])

plt_val = standard_plot_values['5']

fivec = ax.scatter(-1000, -1000, c='skyblue',
                   label='CORE - passed all tests',
                   s=plt_val['s'], alpha=plt_val['alpha'],
                   edgecolors=plt_val['edgecolors'],
                   marker=plt_val['marker'])

# get percentages
count_total = sum([len(status_dict_step_1[i]) for i in status_dict_step_1.keys()])
# print(count_total)
p1 = round((len(status_dict_step_1['1'])/count_total)*100, 1)
# print(p1)
p3 = round((len(status_dict_step_1['3'])/count_total)*100, 1)
# print(p3)
p5 = round((len(status_dict_step_3['5'])/count_total)*100, 1)
# print(p5)


ax.legend([one, three, fivec],
          ['failed chemistry test ('+str(p1)+'%)',
           'failed lattice test ('+str(p3)+'%)',
           'passed all tests ('+str(p5)+'%)'],
          loc=4, fancybox=True, fontsize=14)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_xlabel("pore limiting diameter [$\mathrm{\AA}$]", fontsize=16)
ax.set_ylabel("N$_2$ accessible void fraction [%]", fontsize=16)
# ax.legend([y, n], ['aligned', 'not aligned'], loc=4, fancybox=True)
ax.set_xlim(0, 80)
ax.set_ylim(0, 1)

# save fig
fig.tight_layout()
fig.savefig("db_comp_1.pdf",
            bbox_inches='tight', dpi=360)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
#########################
