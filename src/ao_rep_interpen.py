#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses screening result for CORE Database specifically.

Produces interpentration parity plot in supp info.
"""

import time
import numpy as np
import argparse
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import analysis_output_functions as aoo
import general_functions as genf
import ao_plot_fn as aoplt

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

# set standard plot values
standard_plot_values = {'i':
                        {'s': 80, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 0.8, 'marker': 'o'}}

# separate results into two files
res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

# list of interpenetrated CIFs
interpen_films = []
films = list(set(per_MOF_data['film']))
for i in films:
    if 'i_'+i+'_0' in films:
        interpen_films.append(i)


# collect films that are non-interpen versions of interpen_films
pairs = {}
for i in interpen_films:
    for j in films:
        if i in j and 'i_' in j:
            pairs[j] = i

# set standard plot values for interpen and non-interpen case
standard_plot_values = {'i':
                        {'s': 80, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 0.8, 'marker': 'o'}}
plt_val = standard_plot_values['i']


fig, ax1 = plt.subplots(figsize=(5, 5))

for p in pairs.items():
    print(p)
    film1 = p[0]  # non interpen
    film2 = p[1]  # interpen
    ########################

    # get film1 data
    f1_data = per_MOF_data[per_MOF_data['film'] == film1]
    # get film1 data
    f2_data = per_MOF_data[per_MOF_data['film'] == film2]

    if str(f1_data['1_max_DIB'].iloc[0]) == 'None':
        print('no matching lattices found for one of these binding planes')
        continue
    if str(f2_data['1_max_DIB'].iloc[0]) == 'None':
        print('no matching lattices found for one of these binding planes')
        continue

    # plot the max DIB of both films
    film1_DIB = float(f1_data['1_max_DIB'])
    film2_DIB = float(f2_data['1_max_DIB'])

    ax1.scatter(film2_DIB,
                film1_DIB,
                c=plt_val['c'],
                marker=plt_val['marker'],
                s=plt_val['s'],
                alpha=plt_val['alpha'],
                edgecolors=plt_val['edgecolors'])

aoplt.define_plot_variables(ax1,
                            title='',
                            xtitle='$\Delta\mathrm{IB}_\mathrm{interpenetrated}$',
                            ytitle='$\Delta$IB',
                            xlim=(0, 1),
                            ylim=(0, 1))

x = np.linspace(0, 1.5, 5)
ax1.plot(x, x, c='k', alpha=0.5)

##############################################################################
# save fig
fig.tight_layout()
fig.savefig("iDIB_vs_DIB_max_hkl.pdf",
            bbox_inches='tight')

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
