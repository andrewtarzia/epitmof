#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Compare Zeo++ results with -res and -resex.

Sanity Check.
"""

import numpy as np
import pandas as pd
import sys
import glob

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('Usage: aniso_zeo_run_analysis.py iso_csv aniso_csv \n')
    print('    iso_csv: csv with isotropic results\n')
    print('    aniso_csv: csv with anisotropic results\n')
    sys.exit()
else:
    csv1 = sys.argv[1]
    csv2 = sys.argv[2]

iso_data = pd.read_csv(csv1)
ani_data = pd.read_csv(csv2)

# add column to ani_data with hkl removed from mof name
new_names = [i[:-2] for i in list(ani_data['mof'])]

for cif in glob.glob("*.cif"):
    mof = cif.replace(".cif", "")
    iso_ = iso_data[iso_data['mof'] == mof]
    ani_ = ani_data[ani_data['mof'] == mof]

    iso_pld = float(iso_['Free Sphere [Ang]'])
    x_pld = float(ani_['Free Sphere [Ang]_1'])
    y_pld = float(ani_['Free Sphere [Ang]_2'])
    z_pld = float(ani_['Free Sphere [Ang]_3'])

    comp = np.array((np.isclose(iso_pld, x_pld, rtol=0.05, atol=0),
                     np.isclose(iso_pld, y_pld, rtol=0.05, atol=0),
                     np.isclose(iso_pld, z_pld, rtol=0.05, atol=0)))

    if np.any(comp):
        pass
        # print(cif)
        # print('all good')
    else:
        print(cif)
        print(iso_pld, x_pld, y_pld, z_pld)
        print(comp)
