#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Collect a list of CIFs given their REFCODES from CSD Python API.

This code is Python 2!
"""

from ccdc import io
import os

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

csd_reader = io.MoleculeReader('CSD')
# move to usb folder
os.chdir("/media/csds/NANO PRO/CSD_CIFs/")

# file name with REFCODES
with open('unmodified_REFCODES.txt', 'r') as f:
    lines = f.readlines()

f.close()

for line in lines:
    l = line.rstrip()
    c = csd_reader.crystal(l)
    # print c.spacegroup_symbol
    f_name = l+'.cif'
    with io.CrystalWriter(f_name) as c_writer:
        c_writer.write(c)
