#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script that checks for any errors in the result file.

Specifically searches for cases where ASO < DIB (should not occur and does not
in results associated with manuscript.)
"""

import sys
import pandas as pd

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

# python script to collect CIFs from parent directory


if (not len(sys.argv) == 3):
    print('Usage: collect_cifs.py dib_lim res_file\n')
    print('    dib_lim: DIB cutoff\n')
    print('    res_file: result file to read\n')
    sys.exit()
else:
    dib_lim = float(sys.argv[1])
    res_file = sys.argv[2]

data = pd.read_csv(res_file)

top_ = data[data['DIB1'] >= dib_lim]

for i, row in top_.iterrows():
    if round(row['aso'], 3) < round(row['DIB1'], 3):
        print(row['film'], "ASO < DIB ERROR!")
        print(row['film'], row['f_hkl'], row['aso'], row['DIB1'])
