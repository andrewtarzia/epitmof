#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script used for debugging the binding_plane.py code.

Will output changes in the binding planes produced in different test
directories.
"""

import glob
import sys
from ase.visualize import view
from ase.io import read

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


if (not len(sys.argv) == 5):
    print('Usage: check_bp_occurences.py DB vis_l vis_g\n')
    print('    DB: database - sets hardcoded DIRs\n')
    print('    options: core, hmof, tob\n')
    print('    vis_l: set if you want to viz all lost BPs')
    print('    vis_g: set if you want to viz all gained BPs')
    print('    vis_a: set if you want to viz all BPs')
    sys.exit()
else:
    DB = sys.argv[1]
    vis_l = sys.argv[2]
    vis_g = sys.argv[3]
    vis_a = sys.argv[4]

new_list = glob.glob("*bind*inter*")
if DB == 'core':
    prefix = '../max_results_27_06_18/'
elif DB == 'core2':
    prefix = '../results_bp_18_06_18/'
elif DB == 'hmof':
    prefix = '../max_results_29_06_18/'
elif DB == 'tob':
    prefix = '../max_results_28_06_18/'
elif DB == 'orig_bp':
    prefix = '../changes_29_06_18/'
elif DB == 'temp':
    # prefix = '../experimental_25_06_18/'
    prefix = '../test40/'
old_list = glob.glob(prefix+"*bind*inter*")
print("new list length:", len(new_list))
print("old list length:", len(old_list))
old_list_r = [i.replace(prefix, "") for i in old_list]

count_lost = 0
count_new = 0
with open("lost_bps.txt", 'w') as f:
    for i in sorted(old_list_r):
        if i not in new_list:
            f.write(i+'\n')
            count_lost += 1
            if vis_l == "t":
                s = read(prefix+i)
                view(s)
                print(count_lost, i)
                input("done?")
    print("number of BP lost:", count_lost)

with open("new_bps.txt", 'w') as f:
    for i in sorted(new_list):
        if i not in old_list_r:
            f.write(i+'\n')
            if vis_g == "t":
                s = read(i)
                view(s)
                print(count_new, i)
                input("done?")
            count_new += 1

    print("number of BP gained:", count_new)

count_new = 0
for i in new_list:
    if vis_a == "t":
        print(count_new, 'of', len(new_list))
        s = read(i)
        view(s)
        print(i)
        s = read(i.replace("inter", "plane"))
        view(s)
        print('plane has ', len(s), 'atoms')
        input("done?")
        count_new += 1
