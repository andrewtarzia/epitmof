#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Run Zeo++ on a series of CIFs.

Runs four Zeo++ cmd line commands:
    - SA: NETWORK -ha -sa probe_radius probe_radius 5000
    - Three spheres (pore diameters): NETWORK -ha -res *.cif
    - Pore volume: NETWORK -ha -vol probe_radius probe_radius 50000
    - Dimensionality of Pores: NETWORK -ha -chan probe_radius *.cif
"""

import os
import glob
import pandas as pd

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 13:47:51 2017

@author: atarzia
"""


def run_zeo_options(mof, probe_radius, z_dir):
    """
    Runs Zeo++ options:
        - SA: NETWORK -ha -sa probe_radius probe_radius 5000
        - Three spheres (pore diameters): NETWORK -ha -res *.cif
        - Pore volume: NETWORK -ha -vol probe_radius probe_radius 50000
        - Dimensionality of Pores: NETWORK -ha -chan probe_radius *.cif
    For a given MOF, which has a CIF.
    """
    # set output file names based on zeo++ defaults
    chan_file = mof+'.chan'
    res_file = mof+'.res'
    vol_file = mof+'.vol'
    sa_file = mof+'.sa'
    # SA
    # skip if the output file already exists and has a size > 0
    if os.path.isfile(sa_file) is False or os.path.getsize(sa_file) == 0.0:
        os.system(zeo_dir+" -ha -sa "+str(probe_radius)+" "+str(probe_radius)+" 5000 "+mof+".cif")
    # AV - Pore Volume
    if os.path.isfile(vol_file) is False or os.path.getsize(vol_file) == 0.0:
        os.system(zeo_dir+" -ha -vol "+str(probe_radius)+" "+str(probe_radius)+" 50000 "+mof+".cif")
    # Chan - Dimenstionality
    if os.path.isfile(chan_file) is False or os.path.getsize(chan_file) == 0.0:
        os.system(zeo_dir+" -ha -chan "+str(probe_radius)+" "+mof+".cif")
    # RES - Pore Diameters
    if os.path.isfile(res_file) is False or os.path.getsize(res_file) == 0.0:
        os.system(zeo_dir+" -ha -res "+mof+".cif")
    if os.path.isfile(chan_file) is True and os.path.isfile(res_file) is True and os.path.isfile(sa_file) is True and os.path.isfile(vol_file) is True:
        if os.path.getsize(chan_file) > 0  and os.path.getsize(res_file) > 0  and os.path.getsize(sa_file) > 0  and os.path.getsize(vol_file) > 0:
            return True
        else:
            return False


def read_zeo_runs(mof):
    """
    Reads Zeo++ output files for all simulation types:
        - SA: mof.sa
        - Three spheres (pore diameters): mof.res
        - Pore volume: mof.chan
        - Dimensionality of Pores: mof.vol

    Then cleans up output files.
    grep "@" *.sa > final_summary_file.txt
    """
    chan_file = mof+'.chan'
    with open(chan_file, 'r') as f:
        lines = f.readlines()
    data = lines[0].rsplit()
    idx_word = data.index('dimensionality')
    dims = [int(i) for i in data[idx_word+1:]]
    if len(dims) > 0:
        max_dim = max(dims)
    else:
        max_dim = 0

    res_file = mof+'.res'
    with open(res_file, 'r') as f:
        lines = f.readlines()
    data = lines[0].rsplit()
    IS, FS, IFS = [float(i) for i in data[1:]]

    vol_file = mof+'.vol'
    with open(vol_file, 'r') as f:
        lines = f.readlines()
    data = lines[0].rsplit()
    idx_AVF = data.index('AV_Volume_fraction:')+1
    idx_AVU = data.index('AV_cm^3/g:')+1
    AVF = float(data[idx_AVF])
    AVU = float(data[idx_AVU])

    sa_file = mof+'.sa'
    with open(sa_file, 'r') as f:
        lines = f.readlines()
    data = lines[0].rsplit()
    idx_density = data.index('Density:')+1
    idx_ASA = data.index('ASA_m^2/g:')+1
    idx_VSA = data.index('ASA_m^2/cm^3:')+1
    density = float(data[idx_density])
    ASA = float(data[idx_ASA])
    VSA = float(data[idx_VSA])

    results = [mof, ASA, VSA, density, AVF, AVU, max_dim, IS, FS, IFS]
    return results


def list_to_res_string(l):
    string = ''
    for i in l:
        string += str(i)+" "
    string += '\n'
    return string


# directory to Zeo++ executable
zeo_dir = "/home/atarzia/zeo++/zeo++-0.3/network"

# settings for porosity calculations using Zeo++
# output all data to CSV file for easy reading
output_csv = 'survey_porosity.csv'
# temp results file
temp_results_file = 'temp_porosity.csv'
# probe radius for simulations
probe_radius = 1.82  # N2

# create library of mofs to calculate porosity for
mofs = []
for cif in glob.glob("*cif"):
    title = cif.replace(".cif", "")
    if "_slab" not in title and "_int" not in title and "surf" not in title:
        mofs.append(title)

# a file that keeps progress in case for need of restart
progress_file = 'porosity_survey.progress'
# do you want to start from scratch?
wipe = 'y'
if wipe == 'y':
    configurations_results = []
    # temp output file
    with open(progress_file, 'w') as f:
        f.write("config result"+'\n')

# initialise results pandas dataframe
results = pd.DataFrame(columns=["mof", "SA [m2/g]", "Vol. SA [m2/cm3]",
                                "density [g/cm3]", "AV_frac [%]",
                                "AV_unit [cm3/g]", "Max Channel Dim.",
                                "Incl. Sphere [Ang]", "Free Sphere [Ang]",
                                "Incl. Free Sphere [Ang]"])

# write to temp csv
results.to_csv(temp_results_file)

# for each mof
count = 0
for mof in mofs:
    count += 1
    print('Doing', mof, '====', len(mofs)-count, 'to go')
    done_mofs = []
    with open(progress_file, 'r') as f:
        lines = f.readlines()
    for line in lines[1:]:
        l = line.rstrip().split(" ")
        done_mofs.append(l[0])
    if mof not in done_mofs:
        temp_results = []
        # Run Zeo++ code
        status = run_zeo_options(mof, probe_radius, z_dir=zeo_dir)
        if status is True:
            temp_results.append(mof)
            temp_results.append(status)
            # collect Zeo++ output into results DB
            m_res = read_zeo_runs(mof)
            # append to full results and temp results file
            results.loc[count] = m_res
            with open(temp_results_file, 'a') as f:
                f.write(','+",".join([str(i) for i in m_res])+'\n')
        elif status is False:
            temp_results.append(mof)
            temp_results.append(status)
        with open(progress_file, 'a') as f:
            f.write(list_to_res_string(temp_results))

# write full results to csv
results = pd.read_csv(temp_results_file)
results.to_csv(output_csv)

#  plot volumetric SA for example:
#  data = pd.read_csv(output_csv)
#  fig, (ax0) = plt.subplots(nrows=1, ncols=1)
#  ax0.scatter(data['density [g/cm3]'], data['Vol. SA [m2/cm3]'],
#            edgecolor='none')
#
#  ax0.set_xlim(0, max(data['density [g/cm3]']))
#  ax0.set_ylim(0, max(data['Vol. SA [m2/cm3]']))
#  ax0.set_xlabel('Density [g/cm3]', fontsize=12)
#  ax0.set_ylabel('Vol. SA [m2/cm3]', fontsize=12)
#  fig.show()
