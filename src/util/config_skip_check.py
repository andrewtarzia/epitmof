#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Checks that there are N+1 configurations for any files that have a config
number = N

A sanity check.
"""

import glob

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

for N in [1, 2, 3, 4, 5, 6, 7, 8]:
    print("Checking sets with", str(N+1), "configs")
    print(len(glob.glob("*_"+str(N)+"_*bind*bonds*")), "miller planes have",
          str(N+1), "configs")
    for file in glob.glob("*_"+str(N)+"_*bind*bonds*"):
        mof = "_".join(file.split("_")[0:2])
        fm = file.split("_")[2]
        if len(glob.glob(mof+"*"+fm+"*bind*bonds*")) < N+1:
            print(glob.glob(mof+"*"+fm+"*bind*bonds*"))
