#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Use BASH 'diff' command to compare the collected binding structures in two
different directories.

This was useful for debugging any changes made to the binding_plane.py code.
"""

import os
import glob

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

print('usage: diff_bind.py\n')
print('requires manual setting of directories of interest.')

old_list = glob.glob("../../CORE_full_db/max_results_27_06_18/*bind*")
old_list_s = [i.replace("../../CORE_full_db/max_results_27_06_18/", "") for i in old_list]
old_list = glob.glob("/home/atarzia/epit-mof/prod_repo/epitmof/examples/results/dell/*bind*")
old_list_s = [i.replace("/home/atarzia/epit-mof/prod_repo/epitmof/examples/results/dell/", "") for i in old_list]
new_list = glob.glob("*bind*")

count = 0
for i, j in enumerate(old_list_s):
    if j in new_list:
        count += 1
        os.system('diff '+old_list[i]+' '+j+' > temp.txt')
        with open('temp.txt', 'r') as f:
            lines = f.readlines()
        if len(lines) > 0:
            print(j)
            print(lines)
            break
