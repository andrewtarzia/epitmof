#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script to print all MOF binding planes with DIB > threshold.
"""

import sys
import pandas as pd

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('Usage: collect_cifs.py dib_lim res_file\n')
    print('    dib_lim: DIB cutoff\n')
    print('    res_file: result file to read\n')
    sys.exit()
else:
    dib_lim = float(sys.argv[1])
    res_file = sys.argv[2]

data = pd.read_csv(res_file)

top_ = data[data['DIB1'] >= dib_lim]

count_ = 0
mofs_counted = []
print('film - hkl - max DIB - 2nd DIB')
for i, row in top_.iterrows():
    print(row['film'], '-', row['f_hkl'], '-', row['DIB1'], '-', row['DIB2'])
    if row['film'] not in mofs_counted:
        count_ += 1
        mofs_counted.append(row['film'])
    if row['aso'] < row['DIB1']:
        print(row['film'], "ASO < DIB ERROR!")

print(count_, 'top candidates')
