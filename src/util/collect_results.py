#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Collect all releveant output files after a complete run of screening code.
"""

import sys
import glob
import os

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('script to collect results and clean run directory.')
    print('Usage: collect_resuls.py run_dir result_dir\n')
    print('    run_dir: location of data to be cleaned\n')
    print('    result_dir: location where results are to be moved to\n')
    sys.exit()
else:
    run_dir = sys.argv[1]
    res_dir = sys.argv[2]

if run_dir[-1] != '/':
    run_dir += '/'
if res_dir[-1] != '/':
    res_dir += '/'

print('run directory:', run_dir)
print('result directory:', res_dir)

file_t = "*ASO*data"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "*bind*"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "*surf*"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "*interface*cif"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "*png"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "*pdf"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "epitcif*txt"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "CIF_TODO*"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')

file_t = "screening_parameters.txt"
files = glob.glob(run_dir+file_t)
print('file type:', file_t, '--', len(files), 'files')
if len(files) > 0:
    os.system('mv '+run_dir+file_t+' '+res_dir)
else:
    print("already moved", file_t)
files = glob.glob(res_dir+file_t)
print('copied --', len(files), 'files')
files = glob.glob(run_dir+file_t)
print('cleaned --', len(files), 'files remaining')
