#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
This script contains a loop that will visualise all CIFs using ASE
visualization with a waiting call for user so it doesnt overload.
there are multiple options to visualise what you are interested in

# OPTIONS
1: visualise the original CIF of all binding planes with DIB > dib_lim
    (incurs double ups)
2: visualise all interpenetrated CIFs in a DB + their separate nets
3: determine the distribution of bonds broken to build planes
"""

from ase.io import read
from ase.visualize import view
import pandas as pd
import sys
import glob

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('Usage: visualize_all_CIFS_script.py dib_lim res_file\n')
    print('    dib_lim: DIB cutoff\n')
    print('    res_file: result file to read\n')
    sys.exit()
else:
    dib_lim = float(sys.argv[1])
    res_file = sys.argv[2]

# read in data
data = pd.read_csv(res_file)

# collect top_ candidates
top_ = data[data['DIB1'] >= dib_lim]

# select an option
print('options: \n')
print('    1: visualise the original CIF of all binding planes with DIB > dib_lim')
print('    2: visualise all interpenetrated CIFs + their separate nets')
print('    3: determine the distribution of bonds broken to build planes')
option = input('Choice?')

# iterate
if option == '1':
    count = 0
    films_done = []
    for i, row in top_.iterrows():
        count += 1
        film = row['film']
        hkl = row['f_hkl']
        config = row['config']
        line = str(count)+' of '+str(len(top_))+' done'
        line += ' -- film = '+film+'.cif -- done?'
        if input("wanna see?"+line) == 't':
            if film not in films_done:
                a = read("../"+film+".cif")
                films_done.append(film)
            view(a)
            input(line)
elif option == '2':
    count = 0
    done = []
    for i, row in data.iterrows():
        count += 1
        film = row['film']
        hkl = row['f_hkl']
        config = row['config']
        if film[:2] == 'i_' and film not in done:
            no = film.split("_")[-1]
            short_name = film.replace('i_', '').replace("_"+no, '')
            # this net
            a = read("../"+film+".cif")
            view(a)
            done.append(film)
            done.append(short_name)
            # other nets
            for on in glob.glob("../i_"+short_name+"*.cif"):
                if on.replace("../", "") != film+".cif":
                    a = read(on)
                    view(a)
                    done.append(on.replace("../", "").replace(".cif", ""))
            # inter
            a = read("../"+short_name+".cif")
            view(a)
            done.append(short_name)
            line = str(count)+' of '+str(len(data))+' done'
            line += ' -- film = '+film+'.cif -- done?'
            input(line)
elif option == '3':
    counts = {'non_ML': 0,
              'Cu-C': 0}
    for file in glob.glob("*bind*bonds*"):
        bonds = pd.read_csv(file)
        for i, row in bonds.iterrows():
            a1 = row['atom1_type']
            a2 = row['atom2_type']
            if a1 in counts.keys():
                counts[a1] += 1
            else:
                counts[a1] = 1
            if a2 in counts.keys():
                counts[a2] += 1
            else:
                counts[a2] = 1
            # report all non Metal-Ligand broken bonds
            if a1 != "Cu" and a2 != "Cu":
                counts['non_ML'] += 1
            # report all C-Cu bonds
            if a1 == "C" and a2 == "Cu":
                # print(file)
                counts['Cu-C'] += 1
            elif a1 == "Cu" and a2 == "C":
                counts['Cu-C'] += 1
    print(counts)
