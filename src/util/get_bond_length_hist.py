#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Crude script to collect and analyse the distribution of bonds between two
atom types.
"""

import sys
from ase.io import read
import glob
import matplotlib.pyplot as plt
import numpy as np

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 4):
    print('Usage: get_bond_length_hist.py a1 a2\n')
    print('    a1: symbol of atom 1\n')
    print('    a2: symbol of atom 2\n')
    print('    rerun: T = rerun calculation. F = analysis.\n')
    sys.exit()
else:
    a1 = sys.argv[1]
    a2 = sys.argv[2]
    rerun = sys.argv[3]


cifs = glob.glob("*cif")
cifs = [i for i in cifs if 'slab' not in i]
cifs = [i for i in cifs if 'bind' not in i]

if rerun == 'T':
    print('get data...')
    dists = []
    cifs_d = []
    count = 0
    for mof in cifs:
        count += 1
        print(mof, '-', count, 'done out of', len(cifs))
        a = read(mof)
        if a1 not in a.get_chemical_formula():
            continue
        if a2 not in a.get_chemical_formula():
            continue
        for atom in a:
            for atom2 in a:
                if atom.symbol == a1 and atom2.symbol == a2:
                    dists.append(a.get_distance(atom.index, atom2.index,
                                                mic=True))
                    cifs_d.append(mof)
    print('done.')
    with open('dists_'+a1+'_'+a2+'.txt', 'w') as f:
        for i, j in enumerate(dists):
            f.write(cifs_d[i]+','+str(j)+'\n')
else:
    dists = []
    cifs_d = []
    with open('dists_'+a1+'_'+a2+'.txt', 'r') as f:
        for line in f:
            l = line.rstrip().split(',')
            cifs_d.append(l[0])
            dists.append(float(l[1]))

print('plot histogram')
print('min dist =', min(dists))
print('max dist =', max(dists))

plt.hist(dists, bins=np.arange(1.0, max(dists)+1, 0.1))
plt.savefig('all_dist.pdf')
plt.close()

small_d = (2, 2.8)

small_dist = np.asarray(dists)[np.asarray(dists) < small_d[0]]
small_cifs = np.asarray(cifs_d)[np.asarray(dists) < small_d[0]]
plt.hist(small_dist, bins=np.arange(1.0, small_d[1], 0.1))
plt.savefig('small_dist.pdf')
plt.close()

print(len(set(small_cifs)))
print(set(small_cifs))

if False:
    from ase.visualize import view
    for i in list(set(small_cifs)):
        print(i)
        a = read(i)
        view(a)
        input('done?')
