#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions and script for running an example MC simulation on a test case for
debugging.

To be run on any XYZ file containing O film and Cu sub and plots the trajectory
 of the film atoms.
"""

import numpy as np
import argparse
# from ase.visualize import view
from ase.io import read
from ase import Atoms
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import monte_carlo as MC

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def eg_MC_alg(steps, x_moves, y_moves,
              struct_f, struct_s, Sa, Sb, beta, tol,
              resolution, film_cell_vectors, output_steps=False):
    """Calculate the maximum ASO with a Metropolis MC algorithm.

    Slightly modified version of function in monte_carlo.py

    Keyword arguments:
        steps (int) - number of MC steps to run
        x_moves (list(floats)) - possible translations for initial randomized
            starting move
        y_moves (list(floats)) - possible translations for initial randomized
            starting move
        struct_f (ASE Atoms) - film binding plane
        struct_s (ASE Atoms) - substrate binding plane
        Sa (int) - number of atoms in substrate binding plane
        Sb (int) - number of atoms in film binding plane
        beta (float) - inverse temperature parameter used for MC simulation
        tol (float) - binding tolerance used in MC simulation
        resolution (float) - initial maximum displacement value
        film_cell_vectors (2x3 array) - supercell lattice vectors of film
        output_steps (bool) - output extended information (default False)

    Returns:
        res (list) - ASO at each step of MC simulation
        max_ASO (float) - maximum ASO obtained in all steps of MC simulation
        out_f_struct (ASE Atoms) - final film binding plane
        out_s_struct (ASE Atoms) - final substrate binding plane
        x_disps (list) - cumultative displacement in x dimension for each step
        y_disps (list) - cumultative displacement in y dimension for each step

    """
    res = []
    xys = []  # these are the steps taken each loop
    acc_50 = []
    step_size = []
    # collect the overall displacement for the MC run
    x_disps = []
    y_disps = []
    x_disp = 0
    y_disp = 0
    x_positions = []
    y_positions = []

    # set resolution as tuple of input number
    resolution = [resolution, resolution]
    # set half cell width (HCW) as max resolution to be used
    HCW = [(1/2) * (film_cell_vectors[0][0]+film_cell_vectors[1][0]),
           (1/2) * (film_cell_vectors[0][1]+film_cell_vectors[1][1])]

    # randomize the starting point
    ith = np.random.randint(len(x_moves))
    jth = np.random.randint(len(y_moves))
    pos = [x_moves[ith], y_moves[jth], 0]
    struct_f.translate(pos)
    # wrap the starting point - therefore cumulative movement at i = 0 is (0,0)
    # note we do not wrap at any other stage throughout this algorithm
    # as we do not want discrepancies between the film and substrate super
    # cell to play any role.
    struct_f.wrap()
    curr_pos = struct_f.copy().positions
    x_positions.append(curr_pos[:, 0])
    y_positions.append(curr_pos[:, 1])
    xys.append(pos)
    _ASO = MC.ASE_scoring_function_opt_v1(struct_f, struct_s, tol, Sa, Sb)
    res.append(_ASO)
    no_accepted = 0
    # every 50 steps (no matter the total number of steps)
    # we want to check if the acceptance ratio is too high or
    # too low and adjust the resolution accordingly
    target_acc_rat = 0.2
    step_counter = 0
    temp_acc_rat = 0
    for tt in np.arange(steps):
        # get new position
        step = MC.ASE_stepper_cell(resolution, x_disp, y_disp, HCW[0]*2,
                                   HCW[1]*2,
                                   film_cell_vectors)
        struct_f.translate(step)
        new_ASO = MC.ASE_scoring_function_opt_v1(struct_f, struct_s,
                                                 tol, Sa, Sb)
        if new_ASO > _ASO:
            # always accept downhill step
            _ASO = new_ASO
            no_accepted += 1
            temp_acc_rat += 1
            x_disp += step[0]
            y_disp += step[1]
            x_disps.append(x_disp)
            y_disps.append(y_disp)
        else:
            # assume energy proportional to inverse ASO
            # calculate psuedo boltzmann weight with beta
            b_w = np.exp(-beta*(-new_ASO+_ASO))
            rr = np.random.random()
            if b_w > rr:
                # accept uphill step with random probability
                _ASO = new_ASO
                no_accepted += 1
                temp_acc_rat += 1
                x_disp += step[0]
                y_disp += step[1]
                x_disps.append(x_disp)
                y_disps.append(y_disp)
            else:
                # we need to reverse the move that has been under taken
                # before trying the next move
                struct_f.translate([-step[0], -step[1], 0])
                x_disps.append(x_disp)
                y_disps.append(y_disp)
        res.append(_ASO)
        xys.append(step)
        curr_pos = struct_f.copy().positions
        x_positions.append(curr_pos[:, 0])
        y_positions.append(curr_pos[:, 1])
        step_size.append(resolution)
        step_counter += 1
        if step_counter == 50:
            temp_acc_rat = temp_acc_rat / 50

            if temp_acc_rat > target_acc_rat:
                # do not let resolution in each dimension be > half cell width
                if resolution[0] * 2 <= HCW[0]:
                    resolution[0] *= 2
                if resolution[1] * 2 <= HCW[1]:
                    resolution[1] *= 2
            elif temp_acc_rat < target_acc_rat:
                resolution[0] /= 2
                resolution[1] /= 2
            acc_50.append(temp_acc_rat)
            step_counter = 0
            temp_acc_rat = 0

    return res, x_positions, y_positions


trials = 10
steps = 100
tolerance = 0.96
# tolerance = 1.9
beta = 50
resolution = 0.5

# parse CL arguments
parser = argparse.ArgumentParser(description="Run MC algorithm")
parser.add_argument('XYZfile', metavar='XYZfile', type=str, nargs='+',
                    help='XYZ file.')

args = parser.parse_args()
XYZ_file = args.XYZfile[0]

fig, ax = plt.subplots(figsize=(5, 5))

# get film and substrate ASE structures from XYZ file
int_struct = read(XYZ_file)
# view(int_struct)
count_O = len([i for i in int_struct if i.symbol == 'O'])
count_Cu = len([i for i in int_struct if i.symbol == 'Cu'])
len(int_struct)
O_s = Atoms()
for i in int_struct:
    if i.symbol == 'O':
        O_s.append(i)
O_s.cell = int_struct.cell

Cu_s = Atoms()
for i in int_struct:
    if i.symbol == 'Cu':
        Cu_s.append(i)
Cu_s.cell = int_struct.cell

O_s.set_pbc([True, True, True])

f_coords = O_s.get_positions()
s_coords = Cu_s.get_positions()

patches = []
for atom in Cu_s:
    circle = Circle((atom.x, atom.y), tolerance, color='orange', alpha=0.4)
    ax.add_artist(circle)

f_str = O_s
s_str = Cu_s
Sa = len(s_str)
Sb = len(f_str)
f_cell_vectors = [f_str.cell[0, :],
                  f_str.cell[1, :]]
# write com transform array produces an array of starting positions for the
# MC algorithm that get chosen from at random
x_moves = np.arange(-1.0, 1.1, 0.1)
y_moves = x_moves
indices = []
results = []
MMs = []
out_f = []
out_s = []
for T in np.arange(trials):
    print('trial :', T, 'of', trials)
    MC_res = eg_MC_alg(steps,
                       x_moves,
                       y_moves,
                       f_str,
                       s_str,
                       Sa, Sb,
                       beta,
                       tolerance,
                       resolution,
                       f_cell_vectors, output_steps=False)
    asos, x_positions, y_positions = MC_res
    X = [i[0] for i in x_positions]
    Y = [i[0] for i in y_positions]
    ax.plot(X, Y, linestyle='-', marker='x')
    ax.scatter(X[0], Y[0], c='b')
    ax.scatter(X[-1], Y[-1], c='r')


ax.plot([0, 10, 10, 0, 0], [0, 0, 10, 10, 0], c='k', lw=2)
ax.set_xlim(-6, 16)
ax.set_ylim(-6, 16)
ax.tick_params(axis='both', which='major', labelsize=16)

ax.set_xlabel('x', fontsize=16)
ax.set_ylabel('y', fontsize=16)
fig.tight_layout()
fig.savefig("mc_example.pdf", bbox_inches='tight', dpi=720)
