#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
This script uses a csv file with known duplicates in a database to move all but
one of the duplicates into a duplicate directory (a child directory of
the database)

It should be run from the database directory.

This was written based on the list of duplicates in the CORE MOF database (v1)
reported by David Sholl et. al. in DOI: 10.1021/acs.chemmater.5b03836

NOTE: This was not used for the results in the manuscript associated with this
code
"""

import os
import sys
import glob

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

# read in command line arguments
try:
    filename = sys.argv[1]
except:
    print('You need to set a filename for the CSV')
    print('- usage: python duplicate_removalist.py filename')
    sys.exit()

# make the duplicate directory
try:
    os.mkdir('duplicates')
except:
    pass

# read in csv file
# the formatting provided in the original Sholl CSV was such that each row had
# all duplicates of one MOF - therefore I have read it in a 'brute force' way
# always take the first in the list is the parent - and ignore all children
# of it
dup_db = {}

with open(filename, 'r') as f:
    lines = f.readlines()

for line in lines:
    l = line.rstrip().split(",")  # CSV!
    if len(l) > 1:
        dup_list = l[1:]
    else:
        dup_list = []
    # clean list of empty items
    dup_list = list(filter(None, dup_list))
    dup_db[l[0]] = dup_list

# move duplicates into duplicate dir!
count_moved = 0
for key, value in dup_db.items():
    if len(value) > 0:
        for cif in value:
            if os.path.isfile(cif+'.cif'):
                os.system('mv '+cif+'.cif duplicates/')
            count_moved += 1

total_cifs = len(glob.glob("*.cif"))

print(count_moved, 'CIFs have been moved out of', total_cifs, 'CIFs')
