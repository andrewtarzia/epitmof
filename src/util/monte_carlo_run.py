#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions and script for running an example MC simulation on a test case for
debugging.

To be run on any XYZ file containing O film and Cu sub and outputs the ASE
Atoms objects at each step if desired.
"""

import numpy as np
import argparse
from ase.visualize import view
from ase.io import read
from ase import Atoms
import monte_carlo as MC

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

trials = 10
steps = 400
tolerance = 0.96
# tolerance = 1.9
beta = 50
resolution = 0.5

# parse CL arguments
parser = argparse.ArgumentParser(description="Run MC algorithm")
parser.add_argument('XYZfile', metavar='XYZfile', type=str, nargs='+',
                    help='XYZ file.')

args = parser.parse_args()
XYZ_file = args.XYZfile[0]

# get film and substrate ASE structures from XYZ file
int_struct = read(XYZ_file)
view(int_struct)
count_O = len([i for i in int_struct if i.symbol == 'O'])
count_Cu = len([i for i in int_struct if i.symbol == 'Cu'])
len(int_struct)
O_s = Atoms()
for i in int_struct:
    if i.symbol == 'O':
        O_s.append(i)
O_s.cell = int_struct.cell

Cu_s = Atoms()
for i in int_struct:
    if i.symbol == 'Cu':
        Cu_s.append(i)
Cu_s.cell = int_struct.cell

O_s.set_pbc([True, True, True])

f_coords = O_s.get_positions()
s_coords = Cu_s.get_positions()

f_str = O_s
s_str = Cu_s
Sa = len(s_str)
Sb = len(f_str)
f_cell_vectors = [f_str.cell[0, :],
                  f_str.cell[1, :]]
# write com transform array produces an array of starting positions for the
# MC algorithm that get chosen from at random
x_moves = np.arange(-5.0, 5.1, 0.5)
y_moves = x_moves
indices = []
results = []
MMs = []
out_f = []
out_s = []
for T in np.arange(trials):
    print('trial :', T, 'of', trials)
    MC_res = MC.ASO_MC_alg(steps,
                           x_moves,
                           y_moves,
                           f_str,
                           s_str,
                           Sa, Sb,
                           beta,
                           tolerance,
                           resolution,
                           f_cell_vectors, output_steps=False)
    t_res, m_scores, out_f_str, out_s_str, x_disps, y_disps = MC_res
    MMs.append(m_scores)
    out_f.append(out_f_str.copy())
    out_s.append(out_s_str.copy())
    max_max = max(MMs)
    mm_indices = [i for i, x in enumerate(MMs)
                  if x == max_max]
    if max_max > 0:
        # this currently selects the first max ASO
        # option. This is for a single set of MC
        # runs and therefore all of the options
        # here should be the same interface.
        xx = mm_indices[0]
        print(max_max)
        print("max ASO struct")
        view(out_f[xx] + out_s[xx])
