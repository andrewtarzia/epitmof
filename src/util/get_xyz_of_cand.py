#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Convert desired CIF into XYZ coordinates for visusalisation in OVITO.
"""

import sys
from ase.io import read

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('Usage: get_xyz_of_cand.py cif trans\n')
    print('    cif: name of cif file to convert\n')
    print('    trans: angstrom to translate in XY plane\n')
    sys.exit()
else:
    cif = sys.argv[1]
    trans = float(sys.argv[2])

struct = read(cif)
struct.translate([trans, trans, 0])
struct.wrap()
struct.write(cif.replace(".cif", "_image.xyz"), format='xyz')
