#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Run Zeo++ on a series of CIFs. Specifically using 'resex' command to check
for anisotropic pores.

Runs four Zeo++ cmd line commands:
    - Three spheres (pore diameters): NETWORK -ha -resex *.cif
"""

import pandas as pd
import os
import glob

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def list_to_res_string(l):
    string = ''
    for i in l:
        string += str(i)+" "
    string += '\n'
    return string


def run_zeo_options(mof, z_dir):
    """
    Runs Zeo++ options:
        - Three spheres (pore diameters): NETWORK -ha -resex *.cif

    """
    # set output file names based on zeo++ defaults
    res_file = mof+'.res'
    # RES - Pore Diameters
    if os.path.isfile(res_file) is False or os.path.getsize(res_file) == 0.0:
        # -resex outputs DI_ DI_ DI_ DF_ DF_ DF_ DIF_ DIF_ DIF for all
        # crystallographic dimensions
        os.system(zeo_dir+" -ha -resex "+mof+".cif")
    if os.path.isfile(res_file) is True:
        if os.path.getsize(res_file) > 0:
            return True
        else:
            return False


def read_zeo_runs(mof):
    """
    Reads Zeo++ output files for all simulation types:
        - Three spheres (pore diameters): mof.res

    Then cleans up output files.
    grep "@" *.sa > final_summary_file.txt
    """

    res_file = mof+'.res'
    with open(res_file, 'r') as f:
        lines = f.readlines()
    data = lines[0].rsplit()
    IS1, __, _, FS1, FS2, FS3, IFS1, IFS2, IFS3 = [float(i) for i in data[1:]]

    results = [mof, IS1, FS1, FS2, FS3, IFS1, IFS2, IFS3]
    return results


# directory to Zeo++ executable
zeo_dir = "/home/atarzia/zeo++/zeo++-0.3/network"

# settings for porosity calculations using Zeo++
# output all data to CSV file for easy reading
output_csv = 'survey_porosity_aniso.csv'
# temp results file
temp_results_file = 'temp_porosity.csv'

# create library of mofs to calculate porosity for
cifs = []
for cif in glob.glob("*cif"):
    # if "_x." in cif or "_y." in cif or "_z." in cif:
    cifs.append(cif)

# a file that keeps progress in case for need of restart
progress_file = 'porosity_survey.progress'
# do you want to start from scratch?
wipe = 'y'
if wipe == 'y':
    configurations_results = []
    # temp output file
    with open(progress_file, 'w') as f:
        f.write("config result"+'\n')

# initialise results pandas dataframe
results = pd.DataFrame(columns=["mof",
                                "Incl. Sphere [Ang]_1",
                                "Free Sphere [Ang]_1",
                                "Free Sphere [Ang]_2",
                                "Free Sphere [Ang]_3",
                                "Incl. Free Sphere [Ang]_1",
                                "Incl. Free Sphere [Ang]_2",
                                "Incl. Free Sphere [Ang]_3",
                                ])

# write to temp csv
results.to_csv(temp_results_file)

# for each mof
count = 0
for cif in cifs:
    mof = cif.replace(".cif", "")
    count += 1
    print('------------------------------------------------------------')
    print('Doing', mof, '====', len(cifs)-count, 'to go')
    print('------------------------------------------------------------')
    done_mofs = []
    with open(progress_file, 'r') as f:
        lines = f.readlines()
    for line in lines[1:]:
        li = line.rstrip().split(" ")
        done_mofs.append(li[0])
    if mof not in done_mofs:
        temp_results = []
        # Run Zeo++ code
        status = run_zeo_options(mof, z_dir=zeo_dir)
        if status is True:
            temp_results.append(mof)
            temp_results.append(status)
            # collect Zeo++ output into results DB
            m_res = read_zeo_runs(mof)
            # append to full results and temp results file
            results.loc[count] = m_res
            with open(temp_results_file, 'a') as f:
                f.write(','+",".join([str(i) for i in m_res])+'\n')
        elif status is False:
            temp_results.append(mof)
            temp_results.append(status)
        with open(progress_file, 'a') as f:
            f.write(list_to_res_string(temp_results))

# write full results to csv
results = pd.read_csv(temp_results_file)
results.to_csv(output_csv)
