#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Copy all CIFs in a given CSV file from a parent directory to current dir.
"""

import sys
import pandas as pd
import glob
from shutil import copyfile
import os

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('Usage: collect_cifs.py parent_dir file_string\n')
    print('    parent_dir: location of CIFs\n')
    print('    file_string: string in CSV files of CIF list\n')
    sys.exit()
else:
    parent_dir = sys.argv[1]
    file_string = sys.argv[2]

for file in glob.glob(file_string+"*"):
    data = pd.read_csv(file)
    for film in list(data['film name']):
        # bring cif file here
        copyfile(parent_dir+film+".cif",  os.getcwd()+'/'+film+".cif")
