#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Standalone script to check if two binding planes in a single MOF Miller plane
are equivalent. This code is effectively the same as that in binding_plane.py
and should not be necessary for normal usage of screening code.
"""

import sys
import pandas as pd
import glob
import numpy as np
from ase.io import read
from analysis_output_functions import get_distance_bet_structs
from general_functions import read_parameter_file

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

if (not len(sys.argv) == 3):
    print('Usage: check_config_duplication.py CIF_TODO param_file\n')
    print('    CIF_TODO: CSV file naming CIFs\n')
    print('    param_file: screening parameter file\n')
    sys.exit()
else:
    CIF_TODO = sys.argv[1]
    param_file = sys.argv[2]
    pass

param_dict = read_parameter_file(param_file)
max_angle_tol = float(param_dict['max_angle_tol'][0])
# get tolerance from substrate binding atom and film
# binding atom ionic radii
film_radii = float(param_dict['film_radii'][0])
sub_radii = float(param_dict['sub_radii'][0])
# use Lorentz mixing rules for ionic radii
# ij = (ii + jj) / 2
# use radius!
tolerance = (film_radii + sub_radii) / 2
print("binding tolerance =", tolerance, "Angstrom")
# define the tolerance from the distance between copper sites in Cu(OH)2
# a distance of 0.5 Ansgtrom leads to an angle of rotation of the Cu
# atoms of about 10 degrees
tol_u = 0.25
print("uniqueness tolerance =", tol_u, "Angstrom")
print("uniqueness tolerance =",
      np.degrees(2 * np.arcsin(tol_u / (2 * 2.947))), "degrees")

to_rm_file = 'bp_to_remove.txt'
with open(to_rm_file, 'w') as f:
    f.write('# binding plane configs to remove!\n')
for file in glob.glob(CIF_TODO+"*"):
    data = pd.read_csv(file)
    for film in list(data['film name']):
        print(film)
        plane_files = glob.glob(film+"*bind*plane*")
        hkls = set([i.replace(film, '').replace('_bind_plane.cif', '').split("_")[1] for i in plane_files])
        print(plane_files)
        print(hkls)
        for hkl in list(hkls):
            print(hkl)
            to_remove = []
            checked_pairs = {}
            for plane1 in plane_files:
                if hkl not in plane1:
                    continue
                if plane1 in to_remove:
                    continue
                # read in config
                config1 = plane1.replace(film, '').replace('_bind_plane.cif', '').split("_")[2]
                checked_pairs[config1] = []
                case1 = read(plane1)
                print(plane1)
                for atom in case1:
                    atom.z = 0
                # place the structures COP at (x,y) == (0,0)
                # print(f_str.positions)
                COP_atoms = [np.average(case1.positions[:, 0]),
                             np.average(case1.positions[:, 1])]
                # print(COP_atoms)
                # translate from COP to (0,0)
                case1.translate([-COP_atoms[0], -COP_atoms[1], 0])
                # view(case1)
                # input('done?')
                for plane2 in plane_files:
                    if hkl not in plane2:
                        continue
                    if plane2 in to_remove:
                        continue
                    if plane2 == plane1:
                        continue
                    print(plane2)
                    config2 = plane2.replace(film, '').replace('_bind_plane.cif', '').split("_")[2]
                    if config1 in checked_pairs.keys():
                        if config2 in checked_pairs[config1]:
                            continue
                    if config2 in checked_pairs.keys():
                        if config1 in checked_pairs[config2]:
                            continue
                    print(config2)
                    checked_pairs[config1].append(config2)
                    # checked_pairs[config2].append(config1)
                    case2 = read(plane2)
                    for atom in case2:
                        atom.z = 0
                    # place the structures COP at (x,y) == (0,0)
                    # print(f_str.positions)
                    COP_atoms = [np.average(case2.positions[:, 0]),
                                 np.average(case2.positions[:, 1])]
                    # print(COP_atoms)
                    # translate from COP to (0,0)
                    case2.translate([-COP_atoms[0], -COP_atoms[1], 0])
                    # view(case2)
                    # input('done?')
                    if len(case1) == len(case2):
                        dists = get_distance_bet_structs(case1, case2,
                                                         verbose=False)
                        min_dist, avg_dist, max_dist = dists
                        print('max pair distance =', max_dist)
                        if max_dist ** 2 < tol_u ** 2:
                            print('not unique!')
                            if int(config1) > int(config2):
                                print('remove:', plane1)
                                to_remove.append(plane1)
                            else:
                                print('remove:', plane2)
                                to_remove.append(plane2)
                    else:
                        print('different no atoms')
            print("binding planes to remove:")
            print(set(to_remove))
            with open(to_rm_file, 'a') as f:
                for i in set(to_remove):
                    f.write(i)
                    f.write('\n')
