#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses screening results for hMOF database specifically.

Uses Zeo++ results to obtain the anisotropic pore characteristics of MOFs in
the hMOF database.

Produces Figure 8 in manuscript and multiple components of figures in
supporting information.
"""

import pandas as pd
import time
import argparse
import matplotlib
matplotlib.use('agg')
import matplotlib.cm as cm
import matplotlib.pyplot as plt
# My module IMPORTS #
import general_functions as genf
import analysis_output_functions as aoo
import ao_plot_fn as aoplt

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# porosity file
porosity_file = param_dict['porosity_csv'][0]
porosity_data = pd.read_csv(porosity_file)
aniso_poro = 'survey_porosity_aniso.csv'
aniso_poro_data = pd.read_csv(aniso_poro)

# topology file
topo_file = param_dict['topology_csv'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

# set standard plot values
standard_plot_values = {'i':
                        {'s': 80, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 1.0, 'marker': 'o'}}

plt_val = standard_plot_values['i']
# get porosity info for the desired property (des_prop)
# if the CIF represent a seperated net for an interpenetrated structure
# then show the values for the interpenetrated CIF
X_ = 'Free Sphere [Ang]'

# separate results into two files
res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

# list of interpenetrated CIFs
interpen_films = []
films = list(set(per_MOF_data['film']))
for i in films:
    if 'i_'+i+'_0' in films:
        interpen_films.append(i)

# define figures
fig = plt.figure()
# define sub plots of figure
ax1 = plt.subplot()

# define colour map
# set mid point based on DIB threshold of 0.67
DIB_thresh = 0.67

new_cmap = aoplt.define_plot_cmap(fig, ax1, DIB_thresh, cm.RdBu,
                                  ticks=[0, 0.25, 0.5, DIB_thresh, 0.75, 1.0],
                                  labels=['0', '0.25', '0.5',
                                          str(DIB_thresh), '0.75', '1.0'],
                                  cmap_label='$\Delta$IB')

fig3, ax13 = plt.subplots(figsize=(5, 5))
fig4, ax14 = plt.subplots(figsize=(5, 5))
# # inset
left, bottom, width, height = [0.5, 0.5, 0.5, 0.5]
ax14ins = fig4.add_axes([left, bottom, width, height])
fig1, (ax11, ax21) = plt.subplots(2, 1, figsize=(5, 10))
fig2, (ax12, ax22) = plt.subplots(2, 1, figsize=(8, 10))

# label
ax14.text(0.01, 0.90, '(a)',
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax14.transAxes,
          fontsize=24)
ax13.text(0.01, 0.90, '(b)',
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax13.transAxes,
          fontsize=24)

ax11.text(0.01, 0.90, '(a)',
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax11.transAxes,
          fontsize=24)
ax21.text(0.01, 0.90, '(b)',
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax21.transAxes,
          fontsize=24)

ax12.text(0.01, 0.90, '(a)',
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax12.transAxes,
          fontsize=24)
ax22.text(0.01, 0.90, '(b)',
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax22.transAxes,
          fontsize=24)

# add a band for DIB figure
ax11.axhline(0.67, c='r', linestyle='--', alpha=0.5)
ax11.axhspan(0.6, 0.77, facecolor='grey', alpha=0.3)

# add a band for DIB figure
ax21.axhline(0.67, c='r', linestyle='--', alpha=0.5)
ax21.axhspan(0.6, 0.77, facecolor='grey', alpha=0.3)

hi_dib_plds = []
lo_dib_plds = []
hi_dib_ipplds = []
lo_dib_ipplds = []

for idx, row in per_MOF_data.iterrows():
    film = row['film']
    print('film:', film)
    # want to ignore interpenetrated versions of CIFs
    if film in interpen_films:
        continue
    # check if any data was collected
    if row['1_max_DIB'] == 'None':
        continue
    # assign DIB values
    else:
        DIB1 = float(row['1_max_DIB'])
        if row['2_max_DIB'] == 'None':
            DIB2 = 0
        else:
            DIB2 = float(row['2_max_DIB'])

    # top candidate?
    if DIB1 >= DIB_thresh:
        if DIB2 >= DIB_thresh:
            alpha = 0.6
        else:
            print('top candidate:', DIB1)
            alpha = 1.0
    else:
        alpha = 0.6

    # get overall PLD
    x = float(porosity_data[porosity_data['mof'] == film][X_])

    # collect only h+k+l = 1 cases
    hkl = str(row['1_f_hkl'])
    y = DIB1
    y2 = DIB2
    print(hkl)
    print(y, y2)
    # hkl = 100 or 010 or 001
    # implies len(str(hkl)) == 3
    # implies h+k+l = 1
    if len(hkl) == 3:
        if sum([int(i) for i in hkl]) == 1:
            if hkl not in ['100', '010', '001']:
                print(film, hkl)
                import sys
                sys.exit('Error in the logic for this case! - Exiting.')
            ani_ = aniso_poro_data[aniso_poro_data['mof'] == film]
            if len(ani_) == 0:
                continue

            x_pld = float(ani_['Free Sphere [Ang]_1'])
            y_pld = float(ani_['Free Sphere [Ang]_2'])
            z_pld = float(ani_['Free Sphere [Ang]_3'])
            # get in plane pld
            if hkl == '100':
                # x direction is out of plane
                in_plane_d = ['y', 'z']
                in_plane_pld = max(y_pld, z_pld)
                in_plane_small = min(y_pld, z_pld)
                out_plane = x_pld
            elif hkl == '010':
                # y direction is out of plane
                in_plane_d = ['x', 'z']
                in_plane_pld = max(x_pld, z_pld)
                in_plane_small = min(x_pld, z_pld)
                out_plane = y_pld
            elif hkl == '001':
                # z direction is out of plane
                in_plane_d = ['x', 'y']
                in_plane_pld = max(x_pld, y_pld)
                in_plane_small = min(x_pld, y_pld)
                out_plane = z_pld

            # ONLY DO ANISOTROPIES FOR TOP CANDIDATES
            if y >= DIB_thresh and y2 < DIB_thresh:
                # calculate pld anisotropies
                # OOP / max in_plane
                # pld_aniso_1 = out_plane / in_plane_pld
                pld_aniso_1 = in_plane_pld / out_plane
                # min in_plane / max in_plane
                # pld_aniso_2 = in_plane_small / in_plane_pld
                pld_aniso_2 = in_plane_pld / in_plane_small

                ax14.scatter(pld_aniso_1, pld_aniso_2,
                             c=new_cmap(y),
                             marker=plt_val['marker'],
                             s=plt_val['s'],
                             alpha=alpha,
                             edgecolors=plt_val['edgecolors'],
                             cmap=new_cmap)
                ax14ins.scatter(pld_aniso_1, pld_aniso_2,
                                c=new_cmap(y),
                                marker=plt_val['marker'],
                                s=plt_val['s'],
                                alpha=alpha,
                                edgecolors=plt_val['edgecolors'],
                                cmap=new_cmap)

                # calculate pld anisotropies
                # |OOP - max in_plane|
                # pld_aniso_1 = abs(out_plane - in_plane_pld)
                pld_aniso_1 = abs(in_plane_pld - out_plane)
                # |min in_plane - max in_plane|
                # pld_aniso_2 = abs(in_plane_small - in_plane_pld)
                pld_aniso_2 = abs(in_plane_pld - in_plane_small)
                ax13.scatter(pld_aniso_1, pld_aniso_2,
                             c=new_cmap(y),
                             marker=plt_val['marker'],
                             s=plt_val['s'],
                             alpha=alpha,
                             edgecolors=plt_val['edgecolors'],
                             cmap=new_cmap)

            # Need to do in-plane scatter and distribution plots
            # scatter plot is done normally
            # coloured by DIB2
            ax11.scatter(x, y,
                         s=plt_val['s'],
                         alpha=alpha,
                         c=new_cmap(y2),
                         edgecolors=plt_val['edgecolors'],
                         marker=plt_val['marker'])
            ax21.scatter(in_plane_pld, y,
                         s=plt_val['s'],
                         alpha=alpha,
                         c=new_cmap(y2),
                         edgecolors=plt_val['edgecolors'],
                         marker=plt_val['marker'])
            # separate distributions by top candidate or not
            if y >= DIB_thresh and y2 < DIB_thresh:
                hi_dib_plds.append(x)
                hi_dib_ipplds.append(in_plane_pld)
            else:
                lo_dib_plds.append(x)
                lo_dib_ipplds.append(in_plane_pld)

ax12.hist(lo_dib_plds, 40,
          alpha=0.4, density=True, histtype='stepfilled',
          label='not aligned',
          color='r')
ax12.hist(hi_dib_plds, 40,
          alpha=0.4, density=True, histtype='stepfilled',
          label='aligned',
          color='b')

ax22.hist(lo_dib_ipplds, 40,
          alpha=0.4, density=True, histtype='stepfilled',
          label='not aligned',
          color='r')
ax22.hist(hi_dib_ipplds, 40,
          alpha=0.4, density=True, histtype='stepfilled',
          label='aligned',
          color='b')

ax12.legend(loc=1, fancybox=True, ncol=1, fontsize=16)

aoplt.define_plot_variables(ax14,
                            title='',
                            xtitle='$\mathrm{PLD}_\mathrm{in\;plane,max}$/$\mathrm{PLD}_\mathrm{out\;of\;plane}$',
                            ytitle='$\mathrm{PLD}_\mathrm{in\;plane,max}$/$\mathrm{PLD}_\mathrm{in\;plane,min}$',
                            xlim=(0, 30),
                            ylim=(0, 30))
aoplt.define_plot_variables(ax14ins,
                            title='',
                            xtitle='$\mathrm{PLD}_\mathrm{in\;plane,max}$/$\mathrm{PLD}_\mathrm{out\;of\;plane}$',
                            ytitle='$\mathrm{PLD}_\mathrm{in\;plane,max}$/$\mathrm{PLD}_\mathrm{in\;plane,min}$',
                            xlim=(0, 5),
                            ylim=(0, 5))
ax14ins.set_xticks([0, 2.5, 5])
ax14ins.set_yticks([0, 2.5, 5])
ax14.set_xticks([0, 10, 20, 30])
ax14.set_yticks([0, 10, 20, 30])
aoplt.define_plot_variables(ax13,
                            title='',
                            xtitle='|$\mathrm{PLD}_\mathrm{in\;plane,max}$ - $\mathrm{PLD}_\mathrm{out\;of\;plane}$| [$\mathrm{\AA}$]',
                            ytitle='|$\mathrm{PLD}_\mathrm{in\;plane,max}$ - $\mathrm{PLD}_\mathrm{in\;plane,min}$| [$\mathrm{\AA}$]',
                            xlim=(0, 20),
                            ylim=(0, 20))
ax13.set_xticks([0, 5, 10, 15, 20])
ax13.set_yticks([0, 5, 10, 15, 20])
aoplt.define_plot_variables(ax11,
                            title='',
                            xtitle='pore limiting diameter [$\mathrm{\AA}$]',
                            ytitle='$\Delta$IB',
                            xlim=(0, 25),
                            ylim=(0, 1.15))
aoplt.define_plot_variables(ax21,
                            title='',
                            xtitle='in-plane pore limiting diameter [$\mathrm{\AA}$]',
                            ytitle='$\Delta$IB',
                            xlim=(0, 25),
                            ylim=(0, 1.15))
aoplt.define_hist_variables(ax12,
                            title='',
                            ytitle='frequency',
                            xtitle="pore limiting diameter [$\mathrm{\AA}$]",
                            xlim=(0, 25),
                            ylim=(0, 0.6))
aoplt.define_hist_variables(ax22,
                            title='',
                            ytitle='frequency',
                            xtitle="in-plane pore limiting diameter [$\mathrm{\AA}$]",
                            xlim=(0, 25),
                            ylim=(0, 0.6))

##############################################################################
# save fig
fig.tight_layout()
fig.savefig("hmof_aniso_figure_cmap.pdf",
            bbox_inches='tight', dpi=720)
fig4.tight_layout()
fig4.savefig("hmof_aniso_figure_pt1.pdf",
             bbox_inches='tight', dpi=720)
fig3.tight_layout()
fig3.savefig("hmof_aniso_figure_pt2.pdf",
             bbox_inches='tight', dpi=720)
fig1.tight_layout()
fig1.savefig("hmof_aniso_DIB.pdf",
             bbox_inches='tight')
fig2.tight_layout()
fig2.savefig("hmof_aniso_dist.pdf",
             bbox_inches='tight')

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
