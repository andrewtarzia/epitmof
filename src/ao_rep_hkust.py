#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Analyses screening result for parameterisation database specifically.

Produces Figure 3a in manuscript. Analyses CuBTC or HKUST-1 CIFs.
"""

import time
import numpy as np
import argparse
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import general_functions as genf
import analysis_output_functions as aoo

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()


def define_plot_variables(ax, title, ytitle, xtitle, xlim, ylim):
    """
    Series of matplotlib pyplot settings to make all plots unitform.
    """
    # Set number of ticks for x-axis
    ax.tick_params(axis='both', which='major', labelsize=16)

    ax.set_ylabel(ytitle, fontsize=16)
    ax.set_xlabel(xtitle, fontsize=16)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)


# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

# separate results into two files
res_file_BP, per_BP_data, res_file_MOF, per_MOF_data = aoo.check_for_two_res_files(result_file)

# list of interpenetrated CIFs
interpen_films = []
films = list(set(per_MOF_data['film']))
for i in films:
    if 'i_'+i+'_0' in films:
        interpen_films.append(i)

# colour per mof - not used
colors = cm.tab20(np.linspace(0, 1, len(films)))

miller_markers = {'11-1': 'o',
                  '110': 'x',
                  '111': '^',
                  '10-1': '+',
                  '1-10': '<'}

# name convention for HKUST CIFs
name_conversion_2 = {'CuBTC': ('CuBTC-a', colors[0], 'o'),
                     'CuBTCa': ('CuBTC-a', colors[0], 'o'),
                     'COD2300380_mod': ('CuBTC-b', colors[1], 'X'),
                     'XAMDUM_clean': ('CuBTC-c', colors[2], 'P'),
                     'XAMDUM01_clean': ('CuBTC-d', colors[3], 'v'),
                     'XAMDUM02_clean': ('CuBTC-e', colors[4], '^'),
                     'XAMDUM03_clean': ('CuBTC-f', colors[5], '<'),
                     'XAMDUM04_clean': ('CuBTC-g', colors[6], '>'),
                     'XAMDUM05_clean': ('CuBTC-h', colors[7], 's'),
                     'XAMDUM06_clean': ('CuBTC-i', colors[8], 'p'),
                     'XAMDUM07_clean': ('CuBTC-j', colors[9], 'D')
                     }


# set standard plot values for interpen and non-interpen case
standard_plot_values = {'i':
                        {'s': 180, 'c': 'none', 'edgecolors': 'k',
                         'alpha': 0.8, 'marker': 'o'}}

# spacing in X direction
dx = 0.6

fig, ax = plt.subplots(figsize=(8, 5))

plt_val = standard_plot_values['i']

for film in films:
    print('film:', film)
    # only desired HKUST films
    if film not in name_conversion_2.keys():
        continue
    film_converted, C, M = name_conversion_2[film]
    # want to ignore interpenetrated versions of CIFs
    if film in interpen_films:
        continue

    # collect results for all binding planes in film
    film_data = per_BP_data[per_BP_data['film'] == film]
    asos_list = []
    bs_list = []
    for idx, row in film_data.iterrows():
        # get max ASO and number of binding sites per UC
        max_aso = float(row['aso'])
        binding_sites = float(row['sb']) / float(row['new_rel.area'])
        asos_list.append(max_aso)
        bs_list.append(binding_sites)

    asos_max = max(asos_list)
    for i, j in enumerate(asos_list):
        bs = bs_list[i]
        if bs == 2:
            D = 2
        elif bs == 4:
            D = 4
        elif bs == 6:
            D = 6
        elif bs == 8:
            D = 8

        if j == asos_max:
            CD = 'gray'
        else:
            CD = 'none'

        ax.scatter(D+(dx*(np.random.random() - 0.5) * 2),
                   j,
                   s=plt_val['s'],
                   alpha=plt_val['alpha'],
                   edgecolors=plt_val['edgecolors'],
                   marker=M,
                   c=CD)

for f in name_conversion_2.keys():
    if 'BTC' in name_conversion_2[f][0]:
        # decoy for legend
        film_converted, C, M = name_conversion_2[f]
        plt_val = standard_plot_values['i']
        a = ax.scatter(100, 100, s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       marker=M,
                       c='none',
                       label=film_converted)

ax.legend(loc=3, fancybox=True, bbox_to_anchor=(1.01, 0.01),
          fontsize=16)
ax.set_xticks([2, 4, 6, 8])
ax.axvline(x=3, c='gray', alpha=0.4)
ax.axvline(x=5, c='gray', alpha=0.4)
ax.axvline(x=7, c='gray', alpha=0.4)

define_plot_variables(ax,
                      title='',
                      xtitle='binding sites per unit-cell',
                      ytitle='ASO',
                      xlim=(1, 9),
                      ylim=(0, 1.1))

##############################################################################
# save figures
fig.tight_layout()
fig.savefig("hkust_ASO_cf.pdf",
            bbox_inches='tight', dpi=720)
