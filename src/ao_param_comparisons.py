#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script that analyses all parameter test results used to produce multiple
Figures in the Supporting Information.

Produces an array of parity plots comparing a standard/default run and a run
with modified parameters.
"""

import pandas as pd
import time
import glob
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

output_files = sorted(glob.glob("screening_results_per_BP*.csv"))

# parameter changes for each run
param_change = {"40": "std",
                "41": "length tol. = 2%",
                "42": "length tol. = 5%",
                "43": "length tol. = 15%",
                "44": "length tol. = 20%",
                "45": "angle tol. = 5%",
                "46": "angle tol. = 10%",
                "47": "angle tol. = 15%",
                "48": "angle tol. = 20%",
                "49": r"$\alpha_{\mathrm{AR}}$ = 5%",
                "50": r"$\alpha_{\mathrm{AR}}$ = 10%",
                "51": r"$\alpha_{\mathrm{AR}}$ = 20%",
                "52": r"$\alpha_{\mathrm{AR}}$ = 25%",
                "53": "trials = 10",
                "54": "trials = 20",
                "55": "trials = 80",
                "56": "steps = 100",
                "57": "steps = 200",
                "58": "steps = 800",
                "63": "0.2 * initial resolution",
                "64": "2 * initial resolution",
                "65": r'$\beta$ = 1',
                "66": r'$\beta$ = 25',
                "67": r'$\beta$ = 100',
                "68": r'$\beta$ = 200',
                "72": "tol. = 0.72 $\mathrm{\AA}$",
                "73": "tol. = 1.20 $\mathrm{\AA}$",
                "74": "tol. = 1.44 $\mathrm{\AA}$",
                "75": "tol. = 1.92 $\mathrm{\AA}$",
                "76": r'$d_{\mathrm{OO}}$ = 0.4 $\mathrm{\AA}$',
                "77": r'$d_{\mathrm{OO}}$ = 0.8 $\mathrm{\AA}$',
                "78": r'$d_{\mathrm{OO}}$ = 1.0 $\mathrm{\AA}$',
                "79": 'angle = $10\degree$',
                "80": 'angle = $20\degree$',
                "81": 'angle = $45\degree$',
                "82": 'buffer = 0.8 $\mathrm{\AA}$',
                "83": 'buffer = 1.0 $\mathrm{\AA}$',
                "84": 'buffer = 1.4 $\mathrm{\AA}$',
                "85": 'buffer = 1.8 $\mathrm{\AA}$',
                "86": 'max area multi. = (15, 1000)',
                }

plt_val = {'s': 80, 'c': 'none', 'edgecolors': 'k',
           'alpha': 0.6, 'marker': 'o'}
std_data = pd.read_csv("screening_results_per_BP_40.csv")

######################################################################
title = 'area'
tests = ['86']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: max area multi. = (9, 90))', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=12)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'buffer'
tests = ['82', '83', '84', '85']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: buffer = 1.2 $\mathrm{\AA}$)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'XYangle'
tests = ['79', '80', '81']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: angle = $30\degree$)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'DOO'
tests = ['76', '77', '78']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: $d_{\mathrm{OO}}$ = 0.6 $\mathrm{\AA}$)',
              fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Bind_Tol'
tests = ['72', '73', '74', '75']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: tol. = 0.96 $\mathrm{\AA}$)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=4, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Max Area Ratio'
tests = ['49', '50', '51', '52']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel(r'$\Delta$IB (default: $\alpha_{\mathrm{AR}}$ = 15%)',
              fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Angle'
tests = ['45', '46', '47', '48']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: angle tol. = 2%)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Length'
tests = ['41', '42', '43', '44']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)


# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: length tol. = 10%)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Beta'
tests = ['65', '66', '67', '68']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel(r'$\Delta$IB (default: $\beta$ = 50)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Trials'
tests = ['53', '54', '55']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: trials = 40)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################
title = 'Steps'
tests = ['56', '57', '58']
colors = cm.tab20(np.linspace(0, 0.7, len(tests)))
markers = ['o', 'X', 'P', 'v']

fig, ax = plt.subplots(figsize=(5, 5))
x = np.linspace(0, 1.5, 5)
ax.plot(x, x, c='grey', alpha=0.5)

counted = 0
leg_1, leg_2 = [], []
for I, test_f in enumerate(output_files):
    test = test_f.replace(".csv", "").split("_")[-1]
    if test == '40':
        continue
    if test not in tests:
        continue
    C = colors[counted]
    M = markers[counted]
    counted += 1
    param = param_change[test]
    data = pd.read_csv(test_f)

    try:
        D = ax.scatter(std_data['DIB1'],
                       data['DIB1'],
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    except ValueError:
        # have to consider missing points
        for i, row in std_data.iterrows():
            # print(row)
            film_df = data[data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(row['DIB1'],
                           config_df['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(row['DIB1'],
                           0,
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        for i, row in data.iterrows():
            # print(row)
            film_df = std_data[std_data['film'] == row['film']]
            # print(film_df)
            miller_df = film_df[film_df['f_hkl'] == row['f_hkl']]
            # print(miller_df)
            config_df = miller_df[miller_df['config'] == row['config']]
            # print(config_df)
            if len(config_df) > 0:
                ax.scatter(config_df['DIB1'],
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
            else:
                ax.scatter(0,
                           row['DIB1'],
                           c=C,
                           marker=M,
                           s=plt_val['s'],
                           alpha=plt_val['alpha'],
                           edgecolors=plt_val['edgecolors'])
        # # decoy for legend
        D = ax.scatter(-1000,
                       -1000,
                       c=C,
                       marker=M,
                       s=plt_val['s'],
                       alpha=plt_val['alpha'],
                       edgecolors=plt_val['edgecolors'],
                       label=param)
    leg_1.append(D)
    leg_2.append(param)

# Set number of ticks for x-axis
ax.tick_params(axis='both', which='major', labelsize=16)
ax.set_ylabel('$\Delta$IB (modified)', fontsize=16)
ax.set_xlabel('$\Delta$IB (default: steps = 400)', fontsize=16)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.legend(leg_1, leg_2, loc=2, fancybox=True, fontsize=14)
ax.set_xlim(0, 1.1)
ax.set_ylim(0, 1.1)

fig.savefig("DIB_vs_DIB_"+str(title)+".pdf",
            bbox_inches='tight')

######################################################################

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
#########################
