#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script to output CSV of screening results for the dataset in the current
directory.

Will output a CSV for the maximum and second best DIB of each MOF + other
descriptors.

Will output a CSV for the maximum and second best DIB of each binding plane
for each MOF + other descriptors.
"""

import time
import numpy as np
import glob
import argparse
import general_functions as genf
import analysis_output_functions as aoo

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Analysis + Output")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# output/result CSV name
result_file = param_dict['output_csv_name'][0]

# read in CIF TODO list as dataframe
# read in CIF TODO list as dataframe
CIF_TODO_files_step_1 = glob.glob(param_dict['TODO_name'][0]+"_step_1_*.csv")
CIF_TODO_files_step_3 = glob.glob(param_dict['TODO_name'][0]+"_step_3_*.csv")

# porosity file
porosity_file = param_dict['porosity_csv'][0]

# topology file
topo_file = param_dict['topology_csv'][0]

# surface
surface = param_dict['surfaces'][0]
surface_hkl = param_dict['surf_millers'][0]

print("=====================================================================")
print("Write Results to Files for Plotting!")
print("=====================================================================")

# get the MOF list from all CIF_TODO files
# move MOFs into status dict
print("=====================================================================")
print("Step 3:")
status_dict_step_3 = aoo.get_status_dict_step_3(CIF_TODO_files_step_3)

ASO_completed_films = sorted(status_dict_step_3['5'])
# list of interpenetrated CIFs
interpen_films = []
for i in ASO_completed_films:
    if 'i_'+i+'_0' in ASO_completed_films:
        interpen_films.append(i)

max_angle_tol = float(param_dict['max_angle_tol'][0])
# get tolerance from substrate binding atom and film
# binding atom ionic radii
film_radii = float(param_dict['film_radii'][0])
sub_radii = float(param_dict['sub_radii'][0])
# use Lorentz mixing rules for ionic radii
# ij = (ii + jj) / 2
# use radius!
tolerance = (film_radii + sub_radii) / 2
print("binding tolerance =", tolerance, "Angstrom")
# define the tolerance from the distance between copper sites in Cu(OH)2
# a distance of 0.5 Ansgtrom leads to an angle of rotation of the Cu
# atoms of about 10 degrees
tol_u = 0.5
print("uniqueness tolerance =", tol_u, "Angstrom")
print("uniqueness tolerance =",
      np.degrees(2 * np.arcsin(tol_u / (2 * 2.947))), "degrees")

# separate results into two files
# the results for each binding plane for all MOFs
res_file_BP = result_file.replace(".csv", "_per_BP.csv")
# the final results (considering all binding planes) for each MOF
res_file_MOF = result_file.replace(".csv", "_per_MOF.csv")

# output a single line for each substrate-hkl:film-hkl combination with the
# necessary information
aoo.init_res_file_per_BP(res_file_BP)
aoo.init_res_file_per_MOF(res_file_MOF)

for film in ASO_completed_films:
    film_data = aoo.init_film_dict(surface, surface_hkl, film)
    ASO_files = aoo.get_ASO_files(film)
    prefix = surface+"_"+surface_hkl+"_"+film+"_"
    # if film not in ['mof_11029']:
    #     continue
    print("----------------------------------------------------------")
    print("doing:", film)
    aoo.collect_write_film_results(ASO_files, surface, surface_hkl, film,
                                   tol_u, max_angle_tol, prefix,
                                   res_file_BP,
                                   res_file_MOF)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
