#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script to produce figures of supercells of a certain MOF and Miller plane
using Step 3 of the algorithm.

Produces schematics in Figure 1 of manuscript.
"""

import numpy as np
import time
import argparse
from ase.io import read
# from ase.visualize import view
import pymatgen as mg
from pymatgen.core.surface import generate_all_slabs
import general_functions as genf
import binding_plane as bp
import ASO_functions as ASO
import pmg_substrate_analyzer as pmgsa_AT
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex=True)

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Run main step 3")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
parser.add_argument('CIF', metavar='CIF', type=str, nargs='+',
                    help='CIF to use as example')
parser.add_argument('hkl', metavar='hkl', type=str, nargs='+',
                    help='hkl of CIF to use as example')
parser.add_argument('multi', metavar='multi', type=int, nargs='+',
                    help='area multiple to output')

args = parser.parse_args()
film = args.CIF[0]
film_miller = args.hkl[0]
multi_to_output = args.multi[0]
param_file = args.paramfile[0]

# read in parameter dictionary
param_dict = genf.read_parameter_file(param_file)

# %%

# obtain necessary surface information for all required surfaces
print("=========== Collecting Surface information ===========")

substrates = {}
for surf, surf_metal in zip(param_dict['surfaces'], param_dict['sub_metal']):
    substrate = mg.Structure.from_file(surf+".cif")
    # build unit plane for all surfaces in surf_millers
    # as defined by configuration
    print("===", 'building', surf, 'slabs', "===")
    # get substrate millers
    if param_dict['surf_millers'][0] == "*":
        substrate_millers = None
    else:
        substrate_millers = [genf.miller_reader(param_dict['surf_millers'][0])]
    sub_structures, s_millers = bp.produce_surface_slabs(surf, substrate,
                                                         substrate_millers,
                                                         surf_metal,
                                                         0.8)
    substrates[surf] = [sub_structures, s_millers]

print("=========== Collected Surface information ===========")

# %%


def plot_cell(v1, v2, face, face_a, lines, lines_a):
    """
    Plot a cell of vectors v1 and v2 with matplotlib.

    Arguments:
        v1 (array) - vector 1 of cell
        v2 (array) - vector 2 of cell
        face (str) - colour for face of cell
        face_a (float) - alpha for face of cell
        lines (str) - line properties for cell
        lines_a (float) - alpha for lines of cell
    """
    Path = mpath.Path
    path_data = [
        (Path.MOVETO, (0, 0)),
        (Path.LINETO, (v1[0], v1[1])),
        (Path.LINETO, (v1[0] + v2[0], v1[1] + v2[1])),
        (Path.LINETO, (v2[0], v2[1])),
        (Path.CLOSEPOLY, (0, 0)),
        ]
    codes, verts = zip(*path_data)
    path = mpath.Path(verts, codes)
    patch = mpatches.PathPatch(path, facecolor=face, alpha=face_a)
    ax.add_patch(patch)
    # plot control points and connecting lines
    x, y = zip(*path.vertices)
    line, = ax.plot(x, y, lines, alpha=lines_a, lw=3)


surface = param_dict['surfaces'][0]
film_metal = param_dict['film_metal'][0]
print("=== surface: ",
      surface,
      " -- film:",
      film,
      "===")

# do ASO test
print("=========== Doing ASO test ===========")
ASO_start_time = time.time()
completed_ASO = False
# minimum angle from XY plane allowed in configuration search
XY_angle = float(param_dict['XY_angle'][0])
# maximum distance in the Z component between oxygens in a carboxylate
# allowed
OO_buffer = float(param_dict['OO_buffer'][0])
# angstroms to consider in Z range for configuration search
# consider maximal Z distance between atoms in your configuration to
# test the buffer for substrates (Cu(OH)2 for example) is automatically
# set to 0.8
buffer = float(param_dict['buffer'][0])
# set lattice rules
max_area_ratio_tol = float(param_dict['max_area_ratio_tol'][0])
max_length_tol = float(param_dict['max_length_tol'][0])
max_angle_tol = float(param_dict['max_angle_tol'][0])
# maximum area multiple to test
f_max_area_multi = float(param_dict['f_max_area_multi'][0])
s_max_area_multi = float(param_dict['s_max_area_multi'][0])

sub_structures, s_millers = substrates[surface]

##################
# get tolerance from substrate binding atom and film
# binding atom ionic radii
film_radii = float(param_dict['film_radii'][0])
sub_radii = float(param_dict['sub_radii'][0])
# use Lorentz mixing rules for ionic radii
# ij = (ii + jj) / 2
# use radius!
tolerance = (film_radii + sub_radii) / 2

film_results = []
film_mg_struct = mg.Structure.from_file(film+".cif").get_primitive_structure()
# produce all possible slabs of film
all_slabs = generate_all_slabs(film_mg_struct,
                               max_index=1,
                               min_slab_size=0.5,
                               min_vacuum_size=0.0,
                               primitive=False)
print("===", 'building film slabs', "===")
check_complete = param_dict['check_complete'][0]
if check_complete == 'T':
    check_complete = True
else:
    check_complete = False
film_structures, f_millers = bp.get_binding_structures(film,
                                                       all_slabs,
                                                       film_metal,
                                                       buffer,
                                                       XY_angle,
                                                       OO_buffer,
                                                       check_complete)
print("===", 'done', "===")

# remove duplicates in f_millers
f_millers = set(f_millers)

# iterate over all found configurations for this miller index then run
# calculation!
# film miller indices for which the ASO calculation has
# been undertaken. We note that the pymatgen code used
# creates multiple slabs for each miller with a
# shifting factor (see get_slabs() function in
# surface.py) our code only needs one version of the
# slab as it checks for chemical configurations
# throughout the slab cell (in the C direction)
# as of 16/1/18 - where we want to add testing of multiple
# configurations per slab, we will use a dictionary of
# miller - config count pairs to determine if a config has
# been tested
millers_configs_tested = {}

film_results = []
# if an ASO of one has been obtained then we do not
# test any further
millers_ASO_1 = []
for mill_idx in f_millers:
    print("-------------------")
    miller_name = str(mill_idx[0])+str(mill_idx[1])+str(mill_idx[2])
    # ## REMOVE THIS
    # if miller_name != '11-1':
    #     continue
    print('doing film hkl =', miller_name)
    config_counts = []
    for config_key in film_structures.keys():
        if config_key[0] == miller_name:
            config_counts.append(int(config_key[1]))
    # iterate over config counts
    for config in config_counts:
        print('doing film config =', str(config))
        # skip if we have already tested these
        # miller indices and configuration
        try:
            if millers_configs_tested[miller_name] == config:
                continue
        except KeyError:
            pass
        millers_configs_tested[miller_name] = config
        f_prefix = film+"_"+miller_name+"_"+str(config)
        film_out_cif_name = f_prefix+"_bind_plane.cif"
        film_bonds_name = f_prefix+"_bind_bonds.txt"
        for s_mill in s_millers:
            sub_miller = str(s_mill[0])+str(s_mill[1])+str(s_mill[2])
            s_prefix = surface+"_"+sub_miller
            sub_out_cif_name = s_prefix+"_surf_out.cif"
            # read in film and substrate slab with pymatgen
            # to obtain slab area and cell vectors
            # for Zurr and McGill algorithm.
            # these vectors are not reduced
            film_slab = mg.Structure.from_file(film_out_cif_name)
            rpf1, rpf2 = [film_slab.lattice.matrix[0],
                          film_slab.lattice.matrix[1]]
            sub_slab = mg.Structure.from_file(sub_out_cif_name)
            rps1, rps2 = [sub_slab.lattice.matrix[0],
                          sub_slab.lattice.matrix[1]]
            f_area = pmgsa_AT.vec_area(rpf1, rpf2)
            s_area = pmgsa_AT.vec_area(rps1, rps2)

            # the max_area to be used in Zur algorithm
            # is defined here by the smallest area of either the max
            # multiplier * the film unit-cell area or the sub unit cell
            # area
            # Note: it is often defined by the substrate
            smallest_max = min([f_max_area_multi*round(f_area, 3),
                                s_max_area_multi*round(s_area, 3)])
            print("-------------------")
            print("film area:", round(f_area, 3),
                  "substrate area:", round(s_area, 3))
            print("film multi max:", f_max_area_multi,
                  "substrate multi max:", s_max_area_multi)
            print("max allowed area:", round(smallest_max, 3))
            print("-------------------")
            print('getting matches...')

            print(smallest_max, multi_to_output*f_area,
                  [(multi_to_output+2)*round(f_area, 3),
                  s_max_area_multi * round(s_area, 3)])
            # define pymatgen ZSL class
            ZSL = pmgsa_AT.ZSLGenerator(
                            max_angle_tol=max_angle_tol,
                            max_area=smallest_max,
                            max_area_ratio_tol=max_area_ratio_tol,
                            max_length_tol=max_length_tol
                            )
            # generate all possible sl transformation sets
            # produces a generator of arrays
            # each item in the array is tuple (i, j)
            # where i is an array of Zur matrices possible for a given
            # integer multiple of the film unit cell and j is an array
            # of Zur matrices possible for the corresponding integer
            # integer multiple of the substrate unit cell
            trans_sets = ZSL.generate_sl_transformation_sets(
                    film_area=round(f_area, 3),
                    substrate_area=round(s_area, 3))

            matches_done = []
            matches = ZSL.get_equiv_transformations_wZM(
                    transformation_sets=trans_sets,
                    film_vectors=[rpf1, rpf2],
                    substrate_vectors=[rps1, rps2])
            print('done')
            # iterate over and check all produced
            # transformations
            print("-------------------")
            print("test all matches...")
            print("substrate -- hkl -- film -- hkl",
                  "-- config -- film SC match area --",
                  " multiple of film UC area --",
                  " replicated multiple of film UC area --",
                  " no. sub atoms -- no. film atoms")

            miller_results = []
            COP_sets = []
            counts = 0
            for match in matches:
                match_start_time = time.time()
                m = ZSL.match_as_dict(
                            film_sl_vectors=match[0],
                            substrate_sl_vectors=match[1],
                            film_vectors=[rpf1, rpf2],
                            substrate_vectors=[rps1, rps2],
                            match_area=pmgsa_AT.vec_area(*match[0]))

                # match = match rotation matrix [(x1 ,x2) , (x3, x4)]
                # note that the match will correspond to the transform
                # matrix pre reduction + replication
                fmatch_1, fmatch_2, fmatch_3, fmatch_4 = match[2].reshape(4, )
                smatch_1, smatch_2, smatch_3, smatch_4 = match[3].reshape(4, )

                # calculate required multiples and
                # apply replication algorithm
                multi_1, multi_2 = ASO.get_replication_vect(rpf1, rpf2,
                                                            match)
                print("no replication for this script")
                XY = np.asarray([(1, 1)])

                # mulltiply supercell vectors by X and Y
                new_f_match = XY.reshape(2, 1) * np.array(match[0])
                new_s_match = XY.reshape(2, 1) * np.array(match[1])
                new_Zf_match = XY.reshape(2, 1) * np.array(match[2])
                new_Zs_match = XY.reshape(2, 1) * np.array(match[3])

                new_m = ZSL.match_as_dict(
                        film_sl_vectors=new_f_match,
                        substrate_sl_vectors=new_s_match,
                        film_vectors=[rpf1, rpf2],
                        substrate_vectors=[rps1, rps2],
                        match_area=pmgsa_AT.vec_area(*new_f_match))
                new_match_area = new_m['match_area']

                # check if this match dictionary has been tested before
                match_done = ASO.check_match_duplicates(matches_done,
                                                        new_m)
                if match_done is True:
                    continue
                else:
                    matches_done.append(new_m)

                print("-------------------------------------------")
                print("match info:")
                print(fmatch_1, fmatch_2, fmatch_3, fmatch_4)
                print(smatch_1, smatch_2, smatch_3, smatch_4)
                print(match[0][0])
                print(match[0][1])
                print(match[1][0])
                print(match[1][1])
                print(new_f_match)
                print(new_s_match)
                print(XY)
                print("-------------------------------------------")

                bps = ASO.read_in_binding_planes(sub_out_cif_name,
                                                 film_out_cif_name,
                                                 new_m, verbose=False)
                substrate, mat2d, sub_latt_a, sub_latt_b, film_latt_a, film_latt_b, map_f, map_s = bps
                frot_1, frot_2 = map_f[0][0], map_f[0][1]
                frot_3, frot_4 = map_f[1][0], map_f[1][1]
                srot_1, srot_2 = map_s[0][0], map_s[0][1]
                srot_3, srot_4 = map_s[1][0], map_s[1][1]
                if substrate is None and mat2d is None:
                    print("no matching super lattice found..")
                    print("therefore, this match has been skipped!")
                    continue

                # get angle and direction of rotation necessary to
                # make a vector (shortest before replication)
                # of film SC parallel with a vector (shortest before
                # replication) of substrate SC
                R_fa_sa, fa_sa_angle = ASO.get_supercell_rot_matrix(
                                                sub_latt_a,
                                                film_latt_a,
                                                verbose=False)

                # do rotation into substrate coordinate system
                # rotate super cell vectors
                rot_cells = ASO.rotate_to_substrate_system(
                                                sub_latt_a,
                                                sub_latt_b,
                                                R_fa_sa,
                                                film_latt_a,
                                                film_latt_b,
                                                verbose=False)
                r_film_latt_a, r_film_latt_b, r_sub_latt_a, r_sub_latt_b = rot_cells

                # determine if extra rotations are necessary and
                # and determine if translations can be applied
                skip, apply_translation = ASO.determine_extra_rotations(
                                        r_sub_latt_a, r_sub_latt_b,
                                        r_film_latt_a, r_film_latt_b,
                                        max_angle_tol,
                                        verbose=False)
                # transfer substrate atoms to ASE structure
                s_str = ASO.get_substrate_ASE(substrate, r_sub_latt_a,
                                              r_sub_latt_b)

                # rotate atoms and output into ASE structure
                f_str = ASO.get_film_ASE(mat2d, r_film_latt_a,
                                         r_film_latt_b, R_fa_sa,
                                         apply_translation)

                # wrap atoms into their respective supercells
                f_str.wrap()
                s_str.wrap()

                ########################################
                # debugging functions
                ASO.plot_transitions(match, new_f_match, new_s_match,
                                     film_latt_a, film_latt_b,
                                     sub_latt_a, sub_latt_b,
                                     r_film_latt_a, r_film_latt_b,
                                     r_sub_latt_a, r_sub_latt_b,
                                     substrate, mat2d, R_fa_sa,
                                     plot=False)

                ASO.visualise_all_cells(f_str, s_str, viz=False)
                ########################################

                # if input('interested in MC?') is not 't':
                #     continue

                if skip is True:
                    print('skipped')
                    continue

                # Sb must be number of film atoms
                Sa = len(s_str)
                Sb = len(f_str)
                # make sure all atoms have Z = 0
                for atom in f_str:
                    atom.z = 0
                for atom in s_str:
                    atom.z = 0
                f_cell_vectors = [f_str.cell[0, :],
                                  f_str.cell[1, :]]
                s_cell_vectors = [s_str.cell[0, :],
                                  s_str.cell[1, :]]
                cell_match_area = pmgsa_AT.vec_area(*f_cell_vectors)

                # do plot here
                ##############################################################
                # unit cell of film
                fig, ax = plt.subplots(figsize=(5, 5))
                # origin
                # ax.scatter(0, 0, c='k', alpha=0.5)

                # atom positions
                f_orig_uc = read(film_out_cif_name).repeat([10, 10, 1])
                COP = [np.average(f_orig_uc.positions[:, 0]),
                       np.average(f_orig_uc.positions[:, 1])]
                f_orig_uc.translate([-COP[0], -COP[1], 0])
                for a in f_orig_uc:
                    ax.scatter(a.x, a.y, c=(251/255, 38/255, 34/255),
                               s=60,
                               edgecolor='none', alpha=1.0)

                # cells
                plot_cell(film_slab.lattice.matrix[0],
                          film_slab.lattice.matrix[1],
                          face='r', face_a=0.0, lines='k--',
                          lines_a=0.5)

                lim = max([2.5*film_slab.lattice.matrix[0][0],
                           2.5*film_slab.lattice.matrix[1][1]])
                # manually set limits for each figure - as long as they are all
                # the same size space
                ax.set_xlim(-15, 15)
                ax.set_ylim(-15, 15)
                # general figure
                # ax.set_xlim(-40, 40)
                # ax.set_ylim(-40, 40)
                ax.set_xticks([])
                ax.set_yticks([])
                # ax.tick_params(axis='both', which='major', labelsize=16)
                # ax.set_xlabel('$x~[\mathrm{\AA}]$', fontsize=16)
                # ax.set_ylabel('$y~[\mathrm{\AA}]$', fontsize=16)
                fig.savefig("cf_"+film+"_"+film_miller+"_UC.pdf",
                            bbox_inches='tight', dpi=360)
                plt.close()
                ##############################################################
                # unit cell of substrate
                fig, ax = plt.subplots(figsize=(5, 5))
                # origin
                # ax.scatter(0, 0, c='k', alpha=0.5)

                # atom positions
                s_orig_uc = read(sub_out_cif_name).repeat([15, 15, 1])
                COP = [np.average(s_orig_uc.positions[:, 0]),
                       np.average(s_orig_uc.positions[:, 1])]
                s_orig_uc.translate([-COP[0], -COP[1], 0])
                for a in s_orig_uc:
                    ax.scatter(a.x, a.y, c=(33/255, 171/255, 83/255),
                               s=60,
                               edgecolor='none', alpha=1.0)

                # cells
                plot_cell(sub_slab.lattice.matrix[0],
                          sub_slab.lattice.matrix[1],
                          face='r', face_a=0.0, lines='k--',
                          lines_a=0.5)

                lim = max([2*film_slab.lattice.matrix[0][0],
                           2*film_slab.lattice.matrix[1][1]])
                # manually set limits for each figure - as long as they are all
                # the same size space
                # BDC FIGURE
                # 0
                ax.set_xlim(-15, 15)
                ax.set_ylim(-15, 15)
                # 2
                # ax.set_xlim(-15, 15)
                # ax.set_ylim(-5, 25)
                # general figure
                # ax.set_xlim(-40, 40)
                # ax.set_ylim(-40, 40)
                ax.set_xticks([])
                ax.set_yticks([])
                # ax.tick_params(axis='both', which='major', labelsize=16)
                # ax.set_xlabel('$x~[\mathrm{\AA}]$', fontsize=16)
                # ax.set_ylabel('$y~[\mathrm{\AA}]$', fontsize=16)
                fig.savefig("cf_sub_"+film+"_"+film_miller+"_UC.pdf",
                            bbox_inches='tight', dpi=360)
                plt.close()
                ##############################################################
                # reduced super cell of film
                fig, ax = plt.subplots(figsize=(5, 5))
                # origin
                # ax.scatter(0, 0, c='k', alpha=0.5)

                # atom positions
                f_orig_uc = read(film_out_cif_name).repeat([10, 10, 1])
                COP = [np.average(f_orig_uc.positions[:, 0]),
                       np.average(f_orig_uc.positions[:, 1])]
                f_orig_uc.translate([-COP[0], -COP[1], 0])
                for a in f_orig_uc:
                    ax.scatter(a.x, a.y, c=(251/255, 38/255, 34/255),
                               s=60,
                               edgecolor='none', alpha=1.0)

                # cells
                plot_cell(m['film_sl_vecs'][0],
                          m['film_sl_vecs'][1],
                          face='r', face_a=0.0, lines='r-',
                          lines_a=0.8)
                plot_cell(film_slab.lattice.matrix[0],
                          film_slab.lattice.matrix[1],
                          face='r', face_a=0.0, lines='k--',
                          lines_a=0.5)

                lim = max([2.5*film_slab.lattice.matrix[0][0],
                           2.5*film_slab.lattice.matrix[1][1]])
                # manually set limits for each figure - as long as they are all
                # the same size space
                ax.set_xlim(-15, 15)
                ax.set_ylim(-15, 15)
                # general figure
                # ax.set_xlim(-40, 40)
                # ax.set_ylim(-40, 40)
                ax.set_xticks([])
                ax.set_yticks([])
                # ax.tick_params(axis='both', which='major', labelsize=16)
                # ax.set_xlabel('$x~[\mathrm{\AA}]$', fontsize=16)
                # ax.set_ylabel('$y~[\mathrm{\AA}]$', fontsize=16)
                fig.savefig("cf_"+film+"_"+film_miller+"_"+str(counts)+".pdf",
                            bbox_inches='tight', dpi=360)
                plt.close()
                ##############################################################
                # reduced super cell of substrate
                fig, ax = plt.subplots(figsize=(5, 5))
                # origin
                # ax.scatter(0, 0, c='k', alpha=0.5)

                # atom positions
                s_orig_uc = read(sub_out_cif_name).repeat([15, 15, 1])
                COP = [np.average(s_orig_uc.positions[:, 0]),
                       np.average(s_orig_uc.positions[:, 1])]
                s_orig_uc.translate([-COP[0], -COP[1], 0])
                for a in s_orig_uc:
                    ax.scatter(a.x, a.y, c=(33/255, 171/255, 83/255),
                               s=60,
                               edgecolor='none', alpha=1.0)

                # cells
                plot_cell(m['sub_sl_vecs'][0],
                          m['sub_sl_vecs'][1],
                          face='r', face_a=0.0, lines='b-',
                          lines_a=0.8)
                plot_cell(sub_slab.lattice.matrix[0],
                          sub_slab.lattice.matrix[1],
                          face='r', face_a=0.0, lines='k--',
                          lines_a=0.5)

                lim = max([2*film_slab.lattice.matrix[0][0],
                           2*film_slab.lattice.matrix[1][1]])
                # manually set limits for each figure - as long as they are all
                # the same size space
                # BDC FIGURE
                # 0
                ax.set_xlim(-15, 15)
                ax.set_ylim(-15, 15)
                # 2
                # ax.set_xlim(-15, 15)
                # ax.set_ylim(-5, 25)
                # general figure
                # ax.set_xlim(-40, 40)
                # ax.set_ylim(-40, 40)
                ax.set_xticks([])
                ax.set_yticks([])
                # ax.tick_params(axis='both', which='major', labelsize=16)
                # ax.set_xlabel('$x~[\mathrm{\AA}]$', fontsize=16)
                # ax.set_ylabel('$y~[\mathrm{\AA}]$', fontsize=16)
                fig.savefig("cf_sub_"+film+"_"+film_miller+"_"+str(counts)+".pdf",
                            bbox_inches='tight', dpi=360)
                plt.close()
                ##############################################################
                # rotated super cells of film and substrate
                fig, ax = plt.subplots(figsize=(5, 5))
                # origin
                # ax.scatter(0, 0, c='k', alpha=0.5)

                # atom positions
                # film
                f_str = f_str.repeat([10, 10, 1])
                COP = [np.average(f_str.positions[:, 0]),
                       np.average(f_str.positions[:, 1])]
                f_str.translate([-COP[0], -COP[1], 0])
                # to highlight the effect of the ASO
                # switch to translate film atoms to high ASO position
                trans = False
                if trans is True:
                    print("Translating")
                    f_str.translate([0, 2.5, 0])
                for a in f_str:
                    ax.scatter(a.x, a.y, c=(251/255, 38/255, 34/255), s=60,
                               edgecolor='none', alpha=1.0)

                # substrate
                s_str = s_str.repeat([15, 15, 1])
                COP = [np.average(s_str.positions[:, 0]),
                       np.average(s_str.positions[:, 1])]
                s_str.translate([-COP[0], -COP[1], 0])
                for a in s_str:
                    ax.scatter(a.x, a.y, c=(33/255, 171/255, 83/255), s=60,
                               edgecolor='none', alpha=1.0)

                # cells
                plot_cell(f_cell_vectors[0],
                          f_cell_vectors[1],
                          face='r', face_a=0.0, lines='r-',
                          lines_a=0.8)
                plot_cell(s_cell_vectors[0],
                          s_cell_vectors[1],
                          face='r', face_a=0.0, lines='b-',
                          lines_a=0.8)

                lim = max([3*film_slab.lattice.matrix[0][0],
                           3*film_slab.lattice.matrix[1][1]])
                # manually set limits for each figure - as long as they are all
                # the same size space
                # BDC FIGURE
                # 0
                ax.set_xlim(-15, 15)
                ax.set_ylim(-15, 15)
                # general figure
                # ax.set_xlim(-100, 100)
                # ax.set_ylim(-100, 100)
                ax.set_xticks([])
                ax.set_yticks([])
                # ax.tick_params(axis='both', which='major', labelsize=16)
                # ax.set_xlabel('$x~[\mathrm{\AA}]$', fontsize=16)
                # ax.set_ylabel('$y~[\mathrm{\AA}]$', fontsize=16)
                fig.savefig("scf_"+film+"_"+film_miller+"_"+str(counts)+".pdf",
                            bbox_inches='tight', dpi=360)
                plt.close()

                ##############################################################
                counts += 1
                # sys.exit()
                # break

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
