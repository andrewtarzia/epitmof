#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Determines if CIFs are interpenetrated and updates screening to do list
appropriately.

Should be run after write_TODO_step_3.py and before main_step_3.py.
"""

import pandas as pd
import time
import argparse
import os
import glob
import sys
from ase.io import read
import interpenetration as inter
import general_functions as genf

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

genf.print_welcome_message()
start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(
        description="Prepare CIFs for interpenetration handling")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
parser.add_argument('NP', metavar='NP', type=int, nargs='+',
                    help='process to run. associated with CIFS_TODO_NP.csv')

args = parser.parse_args()
NP = args.NP[0]
param_file = args.paramfile[0]

# read in parameter dictionary
param_dict = genf.read_parameter_file(param_file)

# read in CIF TODO list as dataframe
CIF_TODO_file = param_dict['TODO_name'][0]+"_step_3_"+str(NP)+".csv"
if os.path.isfile(CIF_TODO_file) is True:
    CIF_TODO = pd.read_csv(CIF_TODO_file)
else:
    sys.exit("CIF_TODO file does not exist! Exitting!")

# collect all CIFs at this step
film_CIFs = []
for file in glob.glob(param_dict['TODO_name'][0]+"_step_3_*.csv"):
    d = pd.read_csv(file)
    cifs = list(d['film name'])
    for c in cifs:
        film_CIFs.append(c)


# status meaning
# status = 0 : not tested
# status = 1 : did not pass chemistry test
# status = 2 : passed chemistry test
# status = 3 : did not pass lattice test
# status = 4 : passed lattice test
# status = 5 : completed ASO screening


for index, row in CIF_TODO.iterrows():
    status = row['status']
    surface = row['surface name']
    film = row['film name']

    if status == 4:
        print('------------------------------------------------------')
        print("film:", film)
        # do ASO test
        print("--- doing interpenetration test")
        inter_start_time = time.time()
        completed_inter = False

        interpen, mol_to_keep = inter.is_CIF_interpen(film+".cif")
        if interpen is True:
            print("--- CIF is interpenetrated!")
            film_struct = read(film+".cif")
            # extract all interpenetrated nets as ASE structures
            # this does not include the original structure.
            net_structures = inter.extract_interpen_nets(film_struct)
            print("--- Adding", len(net_structures), "CIFs")
            for item, value in net_structures.items():
                # assign new file name
                new_film = 'i_'+film+"_"+str(item)
                # write this net to CIF file
                value.write(filename=new_film+'.cif', format="cif")
                row['film name'] = new_film
                # check that this particular net hasn't been added already
                if new_film not in film_CIFs:
                    CIF_TODO = CIF_TODO.append(row)

        print("--- Completed Interpenetration Test")
        end_time = time.time()

    # no need to update status in CIF TODO. But we do need to add files to
    # CIF_TODO as we go along and find interpenetrated CIFs
    # update file
    CIF_TODO.to_csv(CIF_TODO_file, index=False)


end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
