#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script that obtains the binding planes for all CIFs for step 3.

Can be run before step 3 or will be done as part of step 3.
"""

import pandas as pd
import time
import argparse
import os
import sys
import pymatgen as mg
from pymatgen.core.surface import generate_all_slabs
import general_functions as genf
import binding_plane as bp
import matplotlib
matplotlib.use('agg')

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

genf.print_welcome_message()
start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Run binding plane builder")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
parser.add_argument('NP', metavar='NP', type=int, nargs='+',
                    help='process to run. associated with CIFS_TODO_NP.csv')

args = parser.parse_args()
NP = args.NP[0]
param_file = args.paramfile[0]

# read in parameter dictionary
param_dict = genf.read_parameter_file(param_file)

# read in CIF TODO list as dataframe
CIF_TODO_file = param_dict['TODO_name'][0]+"_step_3_"+str(NP)+".csv"
if os.path.isfile(CIF_TODO_file) is True:
    CIF_TODO = pd.read_csv(CIF_TODO_file)
else:
    sys.exit("CIF_TODO file does not exist! Exitting!")

# obtain necessary surface information for all required surfaces
print("------------------------------------------------------")
print("collecting surface information...")

substrates = {}
for surf, surf_metal in zip(param_dict['surfaces'], param_dict['sub_metal']):
    substrate = mg.Structure.from_file(surf+".cif")
    # build unit plane for all surfaces in surf_millers
    # as defined by configuration
    print("===", 'building', surf, 'slabs', "===")
    # get substrate millers
    if param_dict['surf_millers'][0] == "*":
        substrate_millers = None
    else:
        substrate_millers = [genf.miller_reader(param_dict['surf_millers'][0])]
    sub_structures, s_millers = bp.produce_surface_slabs(surf, substrate,
                                                         substrate_millers,
                                                         surf_metal,
                                                         0.8)
    substrates[surf] = [sub_structures, s_millers]
print('done')
print("------------------------------------------------------")

# status meaning
# status = 0 : not tested
# status = 1 : did not pass chemistry test
# status = 2 : passed chemistry test
# status = 3 : did not pass lattice test
# status = 4 : passed lattice test
# status = 5 : completed ASO screening


for index, row in CIF_TODO.iterrows():
    status = row['status']
    surface = row['surface name']
    film = row['film name']
    film_metal = param_dict['film_metal'][0]
    print("=== surface: ",
          surface,
          " -- film:",
          film,
          "===")

# %%

    if status == 4 or str(status) == '4a':
        print("------------------------------------------------------")
        print("build binding planes...")
        # minimum angle from XY plane allowed in configuration search
        XY_angle = float(param_dict['XY_angle'][0])
        # maximum distance in the Z component between oxygens in a carboxylate
        # allowed
        OO_buffer = float(param_dict['OO_buffer'][0])
        # angstroms to consider in Z range for configuration search
        # consider maximal Z distance between atoms in your configuration to
        # test the buffer for substrates (Cu(OH)2 for example) is automatically
        # set to 0.8
        buffer = float(param_dict['buffer'][0])
        # get surface
        sub_structures, s_millers = substrates[surface]

        ##################
        # get tolerance from substrate binding atom and film
        # binding atom ionic radii
        film_radii = float(param_dict['film_radii'][0])
        sub_radii = float(param_dict['sub_radii'][0])
        # use Lorentz mixing rules for ionic radii
        # ij = (ii + jj) / 2
        # use radius!
        tolerance = (film_radii + sub_radii) / 2

        film_results = []
        film_mg_struct = mg.Structure.from_file(film+".cif").get_primitive_structure()
        # produce all possible slabs of film
        print("-------------------")
        print('building film slabs...')
        all_slabs = generate_all_slabs(film_mg_struct,
                                       max_index=1,
                                       min_slab_size=0.5,
                                       min_vacuum_size=0.0,
                                       primitive=False)
        check_complete = param_dict['check_complete'][0]
        if check_complete == 'T':
            check_complete = True
        else:
            check_complete = False
        film_structures, f_millers = bp.get_binding_structures(film,
                                                               all_slabs,
                                                               film_metal,
                                                               buffer,
                                                               XY_angle,
                                                               OO_buffer,
                                                               check_complete)
        status = '4a'
        print("-------------------")
    # update status in CIF_TODO
    row['status'] = status
    CIF_TODO.loc[index] = row
    # update file
    # use index=False to avoid adding column everytime you save the file
    CIF_TODO.to_csv(CIF_TODO_file, index=False)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
