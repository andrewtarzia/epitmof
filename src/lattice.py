#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used in Step 2 of the screening algorith.

Uses slightly modified pymatgen code for lattice matching algorithm defined in:
    "Lattice match: An application to heteroepitaxy"
"""

import pmg_substrate_analyzer as pmgsa_AT
from pymatgen.core.surface import (SlabGenerator,
                                   get_symmetrically_distinct_miller_indices)

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def lattice_cull(substrate, film, substrate_millers, max_area_ratio_tol,
                 max_length_tol, max_angle_tol, f_max_area_multi,
                 s_max_area_multi,
                 film_max_miller, substrate_max_miller):
    """Run the Lattice matching algorithm to determine if CIF passes step 2.

    Keywords:
        substrate (pymatgen Structure) - substrate crystal structure
        film (pymatgen Structure) - film crystal structure w primitive symmetry
        substrate_millers (list([h,k,l])) - list of Miller planes to test for
            substrate ([[0, 1, 0]] for Cu(OH2))
        max_area_ratio_rol (float) - maximum area ratio tolerance in lattice
            matching algorithm
        max_length_tol (float) - maximum length tolerance in lattice
            matching algorithm
        max_angle_tol (float) - maximum angle tolerance in lattice
            matching algorithm
        f_max_area_multi (int) - maximum integer multiple of film UC area
            allowed for super cells - used to determine max_area in lattice
            matching algorithm
        s_max_area_multi (int) - maximum integer multiple of substrate UC area
            allowed for super cells - used to determine max_area in lattice
            matching algorithm
        film_max_miller (int) - sets the maximum value of film h, k, l
        substrate_max_miller (int) - sets the maximum value of substrate
            h, k, l

    Returns:
        (bool) - True if there is at least one film-hkl and substrate-hkl pair
            with at least one matching lattice for a given film and substrate.

    """

    # iterate through combinations. For efficiency we simply want to find a
    # single combination and then we move onto the next film.
    # note that the use of spglib in sub_analyzer.calculate can produce errors
    # for some CIFs. It is unclear why it occurs - but we introduced a try and
    # except on TypeError here to avoid this issue for particular CIFs. Note
    # these CIFs will get skipped.

    # get the unit cell areas of the slabs for this film and substrate
    try:
        film_millers = sorted(
                get_symmetrically_distinct_miller_indices(film,
                                                          film_max_miller))
    except TypeError:
        print("spglib error occured for -- therefore, it has been passed")
        return False
    max_film_area = 0
    for f in film_millers:
        film_slab = SlabGenerator(initial_structure=film,
                                  miller_index=f,
                                  min_slab_size=0.5,
                                  min_vacuum_size=0.0,
                                  primitive=False).get_slab()
        film_vectors = [film_slab.lattice.matrix[0],
                        film_slab.lattice.matrix[1]]
        film_area = pmgsa_AT.vec_area(*film_vectors)
        if film_area > max_film_area:
            max_film_area = film_area

    if substrate_millers is None:
        sub_millers = sorted(get_symmetrically_distinct_miller_indices(
            film, film_max_miller))
    else:
        sub_millers = substrate_millers

    max_sub_area = 0
    for s in sub_millers:
        sub_slab = SlabGenerator(initial_structure=substrate,
                                 miller_index=s,
                                 min_slab_size=0.5,
                                 min_vacuum_size=0.0,
                                 primitive=False).get_slab()
        sub_vectors = [sub_slab.lattice.matrix[0],
                       sub_slab.lattice.matrix[1]]
        sub_area = pmgsa_AT.vec_area(*sub_vectors)
        if sub_area > max_sub_area:
            max_sub_area = sub_area

    # the max_area to be used in Zur algorithm is defined here by the smallest
    # area of either the film max multiplier * the film unit-cell area or
    # the substrate max multiplier * the sub unit cell area.
    # Note: it is generally defined by the substrate due to the much larger
    # size of MOF unit cells.
    # In the lattice matching code the definition of the film area depends on
    # all possible miller planes of the film - we take the maximum film area
    # associated with any of the film millers (same for the substrate if
    # substrate_millers == None) to make sure that we do not falsely exclude
    # any matches (although it may falsely include matches but these will be
    # lost in the ASO code, which uses a miller plane dependant max area!)
    smallest_max = min([f_max_area_multi*film_area,
                        s_max_area_multi*sub_area])

    # define pymatgen ZSL class
    # set the max area for this film to be the max_area_multi * largest film
    # area of the miller planes available
    ZSL = pmgsa_AT.ZSLGenerator(max_angle_tol=max_angle_tol,
                                max_area_ratio_tol=max_area_ratio_tol,
                                max_length_tol=max_length_tol,
                                max_area=smallest_max
                                )

    sub_analyzer = pmgsa_AT.SubstrateAnalyzer(zslgen=ZSL)
    try:
        for match in sub_analyzer.calculate(film=film,
                                            substrate=substrate,
                                            film_millers=None,
                                            substrate_millers=substrate_millers,
                                            lowest=True):
            # Proliferate film if a match is found
            # print(match)
            return True
    except TypeError:
        print("spglib error occured for -- therefore, it has been passed")
        pass
    return False
