#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used in determination of binding planes from MOF crystal.

My naive coding style here may (will almost definitely) confuse:
    I used the ASE Atom object properties of index, charge and tag to
    differentiate atoms in different parts of the code.
    I have attempted to make the translation between the three well documented.

"""

import numpy as np
import glob
import matplotlib.path as mpath
from ase.data import chemical_symbols, covalent_radii
from ase.io import read
from ase import Atoms
# from ase.visualize import view  # use for debugging
from ase.neighborlist import NeighborList
from scipy.spatial.distance import pdist
from pymatgen.core.surface import generate_all_slabs, SlabGenerator
from pmg_substrate_analyzer import vec_angle
import general_functions as genf
from analysis_output_functions import get_distance_bet_structs

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def build_neigh_list(struct):
    """Builds neigh list using ASE module for ASE Atoms object.

    """
    # for each atom in struct - write covalent radii to list
    cov_rad = []
    for atom in struct:
        cov_rad.append(covalent_radii[atom.number])
    # build neighbour list
    neigh = NeighborList(cutoffs=cov_rad,
                         bothways=True,
                         self_interaction=False,
                         skin=0.3)
    neigh.build(struct)
    return neigh


def get_bond_dictionary(struct):
    """Get bond dictionary for ASE Atoms object

    Returns:
    index_to_tag (dict) - keys = index - values = tags for translation from
        atom indices to atom tags
    bond_dict (dict) - keys = atom index, values = list of bonds
        (index and tags). Each item is the atom bonded to key.

    """
    # define bonds in system
    # dictionary - keys = atom indices,
    # values = list of bonds (each item is the atom bonded to key)
    index_to_tag = {}
    bond_dict = {}

    # create neighbourlist
    neigh = build_neigh_list(struct)

    for atom in struct:
        conn, offsets = neigh.get_neighbors(atom.index)
        tags = [i for i in conn], [struct[i].tag for i in conn]
        index_to_tag[atom.index] = atom.tag
        bond_dict[atom.index] = tags

    return bond_dict, index_to_tag


def get_idx_from_tag(struct, tag):
    """Get the ID of an atom in struct with the given tag.

    """
    idx = None
    for atom in struct:
        if atom.tag == tag:
            idx = atom.index
            break
    return idx


def get_idx_from_charge(struct, charge):
    """Get the ID of an atom in struct with the given charge.

    !! Charge is not used here as the physical property, but just naming
    property !!

    """
    idx = None
    for atom in struct:
        if atom.charge == charge:
            idx = atom.index
            break
    return idx


def get_q_from_idx(struct, index):
    """Get the charge of an atom in struct with the given index.

    !! Charge is not used here as the physical property, but just naming
    property !!

    """
    q = None
    for atom in struct:
        if atom.index == index:
            q = atom.charge
            break
    return q


def get_HCW(struct):
    """Get half cell width of all cell vectors.

    """
    X, Y, Z = struct.cell
    HCW = [np.linalg.norm(i) / 2 for i in [X, Y, Z]]
    return HCW


def get_molecule_list(ASE_struct):
    """Obtain list of molecules in ASE Atoms object defined to be connected by
    a covalent bond.

    Uses ASE neighbour list function.

    Returns:
        molecules_and_atoms (list) - each item in list is a molecule, which is
            a list of atom ids of atoms in the molecule
    """
    struct = ASE_struct.copy()
    neigh = build_neigh_list(struct)
    molecules_and_atoms = []
    assigned_atoms = []
    count = 0  # count of molecules
    for atom in struct:
        # has this atom already been assigned a molecule?
        if atom.index in assigned_atoms:
            # find it's mol_index
            for m in molecules_and_atoms:
                if atom.index in m:
                    mol_index = molecules_and_atoms.index(m)
        else:
            mol_index = None
        # create ASE neigh list
        nl, diss = neigh.get_neighbors(atom.index)
        # new molecule, solo atom
        if len(nl) == 0 and mol_index is None:
            molecules_and_atoms.append([atom.index])
            assigned_atoms.append(atom.index)
            count += 1
        else:
            # for each neigh, check if the neigh is in a molecule already
            for n in nl:
                # if the atom in question is not part of a molecule
                if mol_index is None:
                    if n in [i for j in np.arange(len(molecules_and_atoms))
                             for i in molecules_and_atoms[j]]:
                        # which molecule?
                        for m in molecules_and_atoms:
                            if n in m:
                                mol_index = molecules_and_atoms.index(m)
                        # add this atom to that molecule
                        molecules_and_atoms[mol_index].append(atom.index)
                        assigned_atoms.append(atom.index)
                    else:
                        # neither neighbor or current atom are
                        # in a molecule, so lets assign them both one
                        mol_index = len(molecules_and_atoms)
                        molecules_and_atoms.append([])
                        molecules_and_atoms[mol_index].append(atom.index)
                        molecules_and_atoms[mol_index].append(n)
                        assigned_atoms.append(atom.index)
                        assigned_atoms.append(n)

                else:
                    n_mol_index = None  # unassigned
                    # if atom in question is a part of a molecule
                    # then we must make all of its neighbours part
                    # of a molecule atom in question has mol_index,
                    # make sure it matches the mol_index of all neighbors
                    for m in molecules_and_atoms:
                        if n in m:
                            n_mol_index = molecules_and_atoms.index(m)
                    if n_mol_index is None and n not in assigned_atoms:
                        # unassigned neighbor, therefore assign it
                        # to the same molecule as the current atom
                        assigned_atoms.append(n)
                        molecules_and_atoms[mol_index].append(n)
                    elif n_mol_index is not None:
                        if mol_index != n_mol_index:
                            ord_index = sorted([mol_index, n_mol_index])
                            # move all from the largest mol_index to the
                            # smallest mol_index
                            atoms_to_move = molecules_and_atoms[ord_index[1]]
                            for a in atoms_to_move:
                                molecules_and_atoms[ord_index[0]].append(a)
                            # remove the bigger indexed molecule
                            del molecules_and_atoms[ord_index[1]]
                            mol_index = ord_index[0]
                            n_mol_index = ord_index[0]
    # collect atom in largest molecules, require 'nets' amounts of largest
    # molecules
    # sort molecules in atoms such that the largest molecules are first
    molecules_and_atoms = sorted(molecules_and_atoms, key=len, reverse=True)

    return molecules_and_atoms


def is_upwards_carboxylate(ASE_struct, atom_id, metal, ASE_neigh,
                           XY_angle, OO_buffer):
    """Check if a given atom_id in an ASE_struct is part of a carboxylate group
    that is pointing towards the interface.

    This version was used in the manuscript.

    Arguments:
        ASE_struct - ASE Atoms object : structure to test
        atom_id - int : ID of atom in ASE_struct to test if is a carboxylate
        ASE_neigh - ASE Neighbourlist object:
        XY_angle - float : minimum angle allowed between the z axis and the
            normal vector bisecting the COO group can be orientated
        OO_buffer - float : maximum distance in the Z direction that oxygens
            can be apart.

    Returns:
        [(boolean - True or False), (oxygen 1 id, oxygen 2 id)]
    """
    is_carboxylate = False

    nstruct = ASE_struct.copy()

    neigh = ASE_neigh
    neigh.build(nstruct)

    atom = nstruct[atom_id]
    # get neighbours
    nl, diss = neigh.get_neighbors(atom_id)
    # is neighbours a C and metal?
    neigh_types = [nstruct[i].symbol for i in nl]
    # print(neigh_types)
    if 'C' in neigh_types and metal in neigh_types:
        C_index = nl[neigh_types.index('C')]
        # get neighbour list of C
        nl2, diss = neigh.get_neighbors(C_index)
        # get neighbours of C that are not the original O
        neigh_C_new = [i for i in nl2 if i != atom_id]
        neigh_C_types = [nstruct[i].symbol for i in nl2 if i != atom_id]
        # print(neigh_C_types)
        # is C neighbours O?
        if 'O' in neigh_C_types:
            O2_index = neigh_C_new[neigh_C_types.index('O')]
            # get neighbour list of O2
            nl3, diss = neigh.get_neighbors(O2_index)
            # get neighbours of O2 that are not C
            neigh_O2_types = [nstruct[i].symbol for i in nl3 if i != C_index]
            # print(neigh_O2_types)
            # is O2 neighbours M?
            if metal in neigh_O2_types:
                # set bool and atom positions
                is_carboxylate = True
                O1_pos = atom.position
                C_pos = nstruct[C_index].position
                O2_pos = nstruct[O2_index].position
                # print(O1_pos, C_pos, O2_pos)
    # now test if it is pointing upwards, below the
    # binding plane and if the oxygens are in line with each other
    # print('is carb?', is_carboxylate)
    if is_carboxylate is True:
        # print(atom_id)
        # These two vectors are in the plane
        v1, v2 = O2_pos - C_pos, O1_pos - C_pos
        # print(O2_pos, C_pos, O1_pos)
        # print(v1)
        # print(v2)
        # these vectors do not consider PBCs
        # if the length of these vectors is much larger than an O-C bond
        # then translate Cell in XY direction until they are not
        # more than likely, we would only have to translate once
        # NOTE: this is a crude way of using the minimum image convetion....
        count_trans = 1
        var = 0
        while np.linalg.norm(v1) > 2 or np.linalg.norm(v2) > 2:
            print('require translation')
            print('no:', count_trans)
            # print(np.linalg.norm(v1))
            # print(np.linalg.norm(v2))
            nstruct.translate([2*(count_trans+var), 1*(count_trans), 0])
            nstruct.wrap()
            O1_pos = atom.position
            C_pos = nstruct[C_index].position
            O2_pos = nstruct[O2_index].position
            # These two vectors are in the plane
            v1, v2 = O2_pos - C_pos, O1_pos - C_pos
            if count_trans > 10:
                # the translation is obviously (very unluckily) along some
                # cell vector - therefore skew it.
                var = 3
            if count_trans > 20:
                var = 2
            if count_trans > 30:
                var = 5
            if count_trans > 40:
                return [False, ()]
        #     print(O2_pos, C_pos, O1_pos)
        #     print(v1)
        #     print(v2)
        # print(np.linalg.norm(v1))
        # print(np.linalg.norm(v2))
        # the cross product is a vector normal to the plane
        cp = np.cross(v1, v2)
        a, b, c = cp
        # This evaluates a * x3 + b * y3 + c * z3
        # which equals d
        # d = np.dot(cp, p3)
        norm_vector = np.asarray([a, b, c])
        # print(norm_vector)
        # calculate angle between atomic plane norm vector
        # and norm vector of x,y plane
        angle_between_norms = np.degrees(
                vec_angle(norm_vector,
                          np.asarray([0, 0, 1])))
        angle_between_norms2 = np.degrees(
                vec_angle(norm_vector,
                          np.asarray([0, 0, -1])))
        # HERE I SET THE ANGULAR ALLOWANCE TO 30 DEGREES.
        # NOTE 0 DEGREES IS THE XY PLANE!
        # print(angle_between_norms, angle_between_norms2)
        angle_to_use = min([angle_between_norms, angle_between_norms2])
        # what is the position of the middle atom?
        # - confirm it is lower than the other two
        # atoms in the z direction (not facing the wrong way)
        middle_atom_position = C_pos
        outer_atom_positions = [O1_pos,
                                O2_pos]
        z_differences = np.asarray(
                [middle_atom_position[2]-i[2] for i in outer_atom_positions])
        # add a final test - are the two carboxylates in the binding
        # functional group at the same position in the Z direction
        # (within some buffer - set to 0.6A here)
        # approximately covalent radii of oxygen
        differ_O_z_posi = abs(
                outer_atom_positions[0][2] - outer_atom_positions[1][2])

        # another final test for fluke occurences where the oxygens and their
        # neighbouring Carbon may be on the Z PBC and therefore pass all tests
        # when they shouldn't
        # in this case, check for z_differences > 3 Angstrom (O-C bonds
        # cannot be that long!)
        # print(atom_id, angle_to_use, z_differences, differ_O_z_posi)
        if angle_to_use > XY_angle and np.all(z_differences < 0) and differ_O_z_posi < OO_buffer and np.all(abs(z_differences) < 3):
            return [True, (atom_id, O2_index)]

    return [False, ()]


def is_upwards_carboxylate_no_metal(ASE_struct, atom_id, ASE_neigh,
                                    XY_angle, OO_buffer):
    """Check if a given atom_id in an ASE_struct is part of a carboxylate group
    that is pointing towards the interface. In this case no specific metal is
    needed to be bound to the oxygens.

    Not used in the manuscript.

    Arguments:
        ASE_struct - ASE Atoms object : structure to test
        atom_id - int : ID of atom in ASE_struct to test if is a carboxylate
        ASE_neigh - ASE Neighbourlist object:
        XY_angle - float : minimum angle allowed between the z axis and the
            normal vector bisecting the COO group can be orientated
        OO_buffer - float : maximum distance in the Z direction that oxygens
            can be apart.

    Returns:
        [(boolean - True or False), (oxygen 1 id, oxygen 2 id)]
    """
    transitionMetals = [
            symbol for symbol in chemical_symbols if symbol not in [
                    chemical_symbols[main_index] for main_index in [
                            1, 2, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 18, 33,
                            34, 35, 36, 52, 53, 54, 85, 86]]]
    is_carboxylate = False

    nstruct = ASE_struct.copy()

    neigh = ASE_neigh
    neigh.build(nstruct)

    atom = nstruct[atom_id]
    # get neighbours
    nl, diss = neigh.get_neighbors(atom_id)
    # is neighbours one C and one metal?
    neigh_types = [
            ASE_struct[i].symbol
            for i in nl if ASE_struct[i].symbol not in transitionMetals]
    if 'C' in neigh_types and len(neigh_types) == 1:
        C_index = nl[neigh_types.index('C')]
        # get neighbour list of C
        nl2, diss = neigh.get_neighbors(C_index)
        # get neighbours of C that are not the original O
        neigh_C_new = [i for i in nl2 if i != atom_id]
        neigh_C_types = [nstruct[i].symbol for i in nl2 if i != atom_id]
        # is C neighbours O?
        if 'O' in neigh_C_types:
            O2_index = neigh_C_new[neigh_C_types.index('O')]
            # get neighbour list of O2
            nl3, diss = neigh.get_neighbors(O2_index)
            # get neighbours of O2 that are not C
            neigh_O2_types = [
                    ASE_struct[i].symbol
                    for i in nl3 if i != C_index and
                    ASE_struct[i].symbol not in transitionMetals]
            # is O2 neighbours 0 if C is removed?
            if len(neigh_O2_types) == 0:
                # set bool and atom positions
                is_carboxylate = True
                O1_pos = atom.position
                C_pos = nstruct[C_index].position
                O2_pos = nstruct[O2_index].position
    # now test if it is pointing upwards, below the
    # binding plane and if the oxygens are in line with each other
    if is_carboxylate is True:
        # These two vectors are in the plane
        v1, v2 = O2_pos - C_pos, O1_pos - C_pos
        # print(O2_pos, C_pos, O1_pos)
        # print(v1)
        # print(v2)
        # these vectors do not consider PBCs
        # if the length of these vectors is much larger than an O-C bond
        # then translate Cell in XY direction until they are not
        # more than likely, we would only have to translate once
        count_trans = 1
        var = 0
        while np.linalg.norm(v1) > 2 or np.linalg.norm(v2) > 2:
            print('require translation')
            print('no:', count_trans)
            # print(np.linalg.norm(v1))
            # print(np.linalg.norm(v2))
            nstruct.translate([2*(count_trans+var), 1*(count_trans), 0])
            nstruct.wrap()
            O1_pos = atom.position
            C_pos = nstruct[C_index].position
            O2_pos = nstruct[O2_index].position
            # These two vectors are in the plane
            v1, v2 = O2_pos - C_pos, O1_pos - C_pos
            if count_trans > 10:
                # the translation is obviously (very unluckily) along some
                # cell vector - therefore skew it.
                var = 3
            if count_trans > 20:
                var = 2
            if count_trans > 30:
                var = 5
            if count_trans > 40:
                return [False, ()]
        #     print(O2_pos, C_pos, O1_pos)
        #     print(v1)
        #     print(v2)
        # print(np.linalg.norm(v1))
        # print(np.linalg.norm(v2))
        # the cross product is a vector normal to the plane
        cp = np.cross(v1, v2)
        a, b, c = cp
        # This evaluates a * x3 + b * y3 + c * z3
        # which equals d
        # d = np.dot(cp, p3)
        norm_vector = np.asarray([a, b, c])
        # calculate angle between atomic plane norm vector
        # and norm vector of x,y plane
        angle_between_norms = np.degrees(
                vec_angle(norm_vector,
                          np.asarray([0, 0, 1])))
        angle_between_norms2 = np.degrees(
                vec_angle(norm_vector,
                          np.asarray([0, 0, -1])))
        # HERE I SET THE ANGULAR ALLOWANCE TO 30 DEGREES.
        # note: 90 DEGREES WOULD BE IN THE XY PLANE
        # NOTE I AM USING THE OPPOSITE DIRECTION SUCH THAT THE ANGLES
        # THEREFORE 0 DEGREES IS THE XY PLANE!
        # DESIRED ARE GREATER THAN 60 DEGREES
        angle_to_use = min([angle_between_norms, angle_between_norms2])
        # what is the position of the middle atom?
        # - confirm it is lower than the other two
        # atoms in the z direction (not facing the wrong way)
        middle_atom_position = C_pos
        outer_atom_positions = [O1_pos,
                                O2_pos]
        z_differences = np.asarray(
                [middle_atom_position[2]-i[2] for i in outer_atom_positions])
        # add a final test - are the two carboxylates in the binding
        # functional group at the same position in the Z direction
        # (within some buffer - set to 0.6A here)
        differ_O_z_posi = abs(
                outer_atom_positions[0][2] - outer_atom_positions[1][2])

        # another final test for fluke occurences where the oxygens and their
        # neighbouring Carbon may be on the Z PBC and therefore pass all tests
        # when they shouldn't
        # in this case, check for z_differences > 3 Angstrom (O-C bonds
        # cannot be that long!)
        if angle_to_use > XY_angle and np.all(z_differences < 0) and differ_O_z_posi < OO_buffer and np.all(abs(z_differences) < 3):
            return [True, (atom_id, O2_index)]

    return [False, ()]


def modify_cell_params(ASE_struct, c=5, alpha=90, beta=90, gamma=90):
    """Change cell parameters of binding plane.

    Works in-place.

    """
    struct = ASE_struct
    curr_p = struct.get_cell_lengths_and_angles()
    if curr_p[5] != 90:
        struct.set_cell([curr_p[0], curr_p[1], c, alpha, beta, curr_p[5]])
    else:
        struct.set_cell([curr_p[0], curr_p[1], c, alpha, beta, gamma])


def modify_cell_params_interface(ASE_struct, c=200, alpha=90,
                                 beta=90, gamma=90):
    """Change cell parameters of 3D binding interface structure.

    Works in-place.

    """
    struct = ASE_struct
    curr_p = struct.get_cell_lengths_and_angles()
    if curr_p[5] != 90:
        struct.set_cell([curr_p[0], curr_p[1], c, alpha, beta, curr_p[5]])
    else:
        struct.set_cell([curr_p[0], curr_p[1], c, alpha, beta, gamma])


def translate_slab_to_Z_0(ASE_struct):
    """Translate all binding atoms in a binding plane to the XY plane (Z = 0),
    taking into account cell vectors.

    Works in-place.

    """
    s = ASE_struct
    p_cell = s.cell
    p_z_vec = p_cell[2]
    p_n = p_z_vec / np.linalg.norm(p_z_vec)

    # what is the displacement required in the Z direction
    # such that min Z position of binding atoms = 0
    z_mov = min(s.get_positions()[:, 2])
    c_z_mov = z_mov/p_n[2]
    z_trans = c_z_mov * p_n

    # translate the struct
    s.translate(-z_trans)


def translate_slab_to_Z_0_interface(ASE_struct):
    """Translate all binding atoms in a 3D binding interface to the XY plane
    (Z = 0), taking into account cell vectors.

    Works in-place.

    """

    s = ASE_struct
    p_cell = s.cell
    p_z_vec = p_cell[2]
    p_n = p_z_vec / np.linalg.norm(p_z_vec)

    # what is the displacement required in the Z direction
    # such that min Z position of binding atoms = 0
    z_mov = min(s.get_positions()[:, 2])-0.1
    c_z_mov = z_mov/p_n[2]
    z_trans = c_z_mov * p_n

    # translate the struct
    s.translate(-z_trans)


def convert_cell_to_straight_Z(ASE_struct, vectors):
    """Convert ASE Atoms object cell to one with Z cell vector along (0, 0, 1).

    This is achieved by multiplying the original cell in the X and Y lattice
    directions. Then cutting out (using the PATH) a cell with the new cell.
    Periodicity is maintained.
    """
    old_cell = ASE_struct.cell
    struct = ASE_struct.repeat([10, 10, 1])
    COMxy = (np.average(struct.get_positions()[:, 0]),
             np.average(struct.get_positions()[:, 1]))

    struct.translate([-COMxy[0], -COMxy[1], 0])

    new_struct = Atoms()

    # set XY vectors for new (straight Z) cell as PATH
    v1, v2 = old_cell[0, :], old_cell[1, :]

    Path = mpath.Path
    pathdata = [
        (Path.MOVETO, (0, 0)),
        (Path.LINETO, (v1[0], v1[1])),
        (Path.LINETO, (v1[0] + v2[0], v1[1] + v2[1])),
        (Path.LINETO, (v2[0], v2[1])),
        (Path.CLOSEPOLY, (0, 0)),
        ]
    codes, verts = zip(*pathdata)
    path = mpath.Path(verts, codes)
    # loop through and only keep those within path
    for atom in struct:
        point_2d = [atom.x, atom.y]
        if path.contains_point(point_2d):
            new_struct.append(atom)

    new_struct.set_cell(np.asarray([old_cell[0, :],
                                    old_cell[1, :],
                                    [0, 0, 200]]))
    return new_struct


def get_test_positions(struct, sub_binder, XY_angle,
                       OO_buffer, slice_region):
    """Get Z coordinates of desired binding sites.

    Desired binding site defined as carboxylate group using
    "is_upwards_carboxylate"

    Keywords:
        struct (ASE Atoms) - ASE Atoms structure of slab
        sub_binder (str) - substrate binding atom type
        XY_angle (float) - minimum angle from the XY plane the normal vector
            of the COO group can be orientated
        OO_buffer (float) - maximum distance in the Z direction that oxygens
            can be apart.
        slice_region (tuple) - region in Z coordinates for which we want to
            check for slicing positions
    Returns:
        test_spots (list) - list of Z positions of binding sites to test

    """

    # build neighbor list here so
    # it isn't remade every time is_upwards_carboxylate is called
    cov_rad = []
    for atom in struct:
        cov_rad.append(covalent_radii[atom.number])
    # build neighbour list
    neigh = NeighborList(cutoffs=cov_rad,
                         bothways=True,
                         self_interaction=False,
                         skin=0.3)
    # a list of atoms in carboxylates already
    in_carboxylates = []
    # dictionary of test spots - keys = z coord,
    # value = tuple of oxygens in carboxylate
    test_spots = {}
    # to test if there are PBC straddlers in the XY plane - we run the same
    # test multiple times with translations and wraps in the X and Y directions
    # translations to test
    translations = [0]  # , [3, 0, 0], [0, 3, 0], [3, 3, 0]]
    for T, trans in enumerate(translations):
        # translate
        if T > 0:
            struct.translate([i for i in trans])
            struct.wrap()
        # view(struct)
        for atom in struct:
            if atom.symbol == 'O' and atom.index not in in_carboxylates:
                if atom.z < slice_region[0]:
                    # in the first cell (in Z dimension) - so we skip it
                    continue
                if atom.z > slice_region[1]:
                    # in the first cell (in Z dimension) - so we skip it
                    continue
                # res[0] is bool, res[1] is other oxygen in carboxylate
                if sub_binder != '*':
                    res = is_upwards_carboxylate(struct, atom.index,
                                                 sub_binder, neigh, XY_angle,
                                                 OO_buffer)
                elif sub_binder == '*':
                    # non specific metal case
                    res = is_upwards_carboxylate_no_metal(struct, atom.index,
                                                          neigh, XY_angle,
                                                          OO_buffer)
                # print(atom.index, atom.symbol, res[0])
                if res[0] is True:
                    # add each oxygen that is in carboxylates to the list
                    in_carboxylates.append(res[1][0])
                    in_carboxylates.append(res[1][1])
                    # add z coordinate to test_spots
                    if max([struct[i].z for i in res[1]]) not in test_spots.keys():
                        test_spots[max([struct[i].z
                                        for i in res[1]])] = [res[1][0],
                                                              res[1][1]]
                    else:
                        old_list = test_spots[
                                max([struct[i].z for i in res[1]])]
                        old_list.append(res[1][0])
                        old_list.append(res[1][1])
                        test_spots[
                                max([struct[i].z for i in res[1]])] = old_list
        # reverse translation
        if T > 0:
            struct.translate([-i for i in trans])
            struct.wrap()

    return test_spots


def get_binding_structures(film, all_slabs, sub_binder, buffer, XY_angle,
                           OO_buffer, check_complete=False):
    """Get cleaned binding interface and plane structures.

    Keywords:
        film (str) - name of film
        all_slabs (pymatgen Slabs objects) - all possible slabs of film
        sub_binder (str) - binding atom of substrate layer
            "*" if non specifc metal
        buffer (float) - space around slice height that allows for neighboring
            atoms
        XY_angle (float) - minimum angle from the XY plane the normal vector of
            the COO group can be orientated
        OO_buffer (float) - maximum distance in the Z direction that oxygens
            can be apart.
        check_complete (bool) - switch to check for already obtained binding
            planes. default False.

    Returns:
        final_binding_output (dict) - dictionary object of binding planes
            this information is also output to file
        millers_output (list) - list of Miller planes with binding planes
    """

    # check if we have already obtained binding planes
    if check_complete is True:
        completed = get_completed_structures(film)
        if completed[0] is True:
            final_binding_output, millers_output = completed[1:]
            print("already completed.")
            return final_binding_output, millers_output

    transitionMetals = [
            symbol for symbol in chemical_symbols if symbol not in [
                    chemical_symbols[main_index] for main_index in [
                            1, 2, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 18, 33,
                            34, 35, 36, 52, 53, 54, 85, 86]]]
    # output dictionary
    binding_output = {}
    millers_output = []

    # miller indices that have a binding plane
    millers_with_outs = []

    # film miller indices for which the search for a desired
    # chemical configuration has been undertaken (with success
    # or failure)
    millers_done = []

    for slabs in all_slabs:
        miller = slabs.miller_index
        miller_str = str(miller[0])+str(miller[1])+str(miller[2])
        print("===", "hkl =", miller, "===")
        # millers done will include all millers
        # that did not produce an out file on first try
        if miller not in millers_with_outs and miller_str not in millers_done:
            # if miller_str not in ['011']:
            #     continue
            # save to CIF - add _slab at end to differentiate
            # between CIFs of database
            slabs.to(filename=film+"_"+miller_str+"_slab.cif", fmt='cif')
            # read into ASE
            # here we duplicate the original structure to make sure that we
            # have a larger enough cell below the place where we slice
            struct = read(film+"_"+miller_str+"_slab.cif").repeat([1, 1, 2])

            # we duplicate the structure to make sure at least one cell is
            # below any interface we make. We therefore do not want to test
            # any oxygens within the first cell, which ends in the Z dimension:
            end_of_first_cell = struct.cell[2, 2]

            # slicing region is the third unit cell in the Z direction
            slice_region = (end_of_first_cell, end_of_first_cell*1.5)
            print(slice_region)

            # duplicate struct again to make sure the slicing to make the
            # final interface occurs with at least 2 unit cells bellow it.
            #
            #   ----------------
            #   |  skip in     |
            #   |    here      |
            #   ----------------   - slice region top
            #   |  slice in    |
            #   |    here      |
            #   ----------------   - slice region bottom
            #   |   always...  |
            #   |   below...   |
            #   ----------------
            #   |    slice.    |
            #   |              |
            #   ----------------

            # this may lead to more dupicate configurations that'll  be deleted

            struct = struct.repeat([1, 1, 2])
            # set tags (which get duplicated later) as indices in struct
            for atom in struct:
                atom.tag = atom.index
            print("------------------------------")
            print("searching for binding sites")
            # get z positions to test
            test_spots = get_test_positions(struct, sub_binder,
                                            XY_angle, OO_buffer, slice_region)
            if len(test_spots) == 0:
                print("no desired binding sites")
            else:
                print("found desired binding sites:")
                print(test_spots)
            print("------------------------------")
            # duplicate in the Z direction again tags will be replciated
            # in both layers but atom ids may change
            test_struct = struct.copy().repeat([1, 1, 2])
            # set charges (which do not get duplicated)
            # as indices in struct
            for atom in test_struct:
                atom.charge = atom.index
            # define bonds in system as dictionary
            # - keys = atom indices
            # - values = list of bonds (each item is the atom bonded to key)
            full_bond_dict, full_i2t = get_bond_dictionary(test_struct)
            # number of output configurations
            config_out = 0

            # check for other carboxylates
            # test_spots keys in buffer zone
            test_spots = merge_test_spots(test_spots, buffer)
            print("merged test spots:")
            print(test_spots)
            # view(test_struct)
            # input('done?')
            # slice at carboxylates
            # start at the top
            for lz in sorted(list(test_spots.keys()), reverse=True):
                print("------------------------------")
                print("testing binding site at Z =", round(lz, 2), 'Angstrom')
                O_ids = test_spots[lz]
                # make sliced system
                slice_struct = test_struct.copy()
                # slice at the highest oxygen Z position
                # add a very small buffer here to avoid rounding errors
                del slice_struct[[atom.index
                                  for atom in slice_struct
                                  if atom.z > lz+0.01]]
                # define bonds of sliced system
                # dictionary - keys = atom indices,
                # values = list of bonds (each item is the atom bonded to key)
                slice_bond_dict, slice_i2t = get_bond_dictionary(slice_struct)
                # find broken bonds:
                # iterate over slice_bond_dict - if any values differ from
                # full_bond_dict implies broken bonds
                # collect all broken bonds and determine if any are organic
                organic_bond_broke, broken_bonds_final = analyze_bonds(
                                        slice_bond_dict, slice_i2t,
                                        full_bond_dict, test_struct,
                                        transitionMetals)

                # if organic bonds broken
                # determine if it can be a missing linker defect
                print("Organic bonds broken?", organic_bond_broke)
                O_q = [get_q_from_idx(test_struct, i) for i in O_ids]
                print("cleaning interface...")
                # build linker only molecule lists of test_struct
                # and slice_struct
                test_struct_linker, test_mol_list = get_separate_molecules(
                                            test_struct, transitionMetals)
                slice_struct_linker, slice_mol_list = get_separate_molecules(
                                            slice_struct, transitionMetals)

                # print('mol list')
                # print(slice_mol_list)

                # define binding molecules
                # molecules that contain the binding sites for this slice
                m_lists = get_binding_molecules(O_ids, test_struct,
                                                test_mol_list,
                                                slice_mol_list)
                binding_slice_mol_list, binding_full_mol_list = m_lists
                if organic_bond_broke is False:
                    final_slice_struct = slice_struct.copy()
                    # make neighbor list of final_slice
                    neigh_final_slice = build_neigh_list(final_slice_struct)
                    # clean up hydrogens
                    atoms_to_delete_from_slice = []
                    for atom in slice_struct:
                        # hydrogens
                        if atom.symbol == 'H':
                            # get neighbor list
                            nl_h, diss = neigh_final_slice.get_neighbors(
                                    atom.index)
                            # if a H has no neighbors
                            # delete it
                            if len(nl_h) == 0:
                                atoms_to_delete_from_slice.append(
                                        atom.charge)

                    # delete atoms
                    del final_slice_struct[
                            [atom.index
                             for atom in final_slice_struct
                             if atom.charge in atoms_to_delete_from_slice]]
                elif organic_bond_broke is True:
                    # clean the interface by removing all molecules that have
                    # atoms in the buffer zone and do not have atoms in the
                    # binding molecules in both full struct and slice struct
                    final_slice_struct = clean_interface_buffer(
                                            slice_struct,
                                            lz, buffer,
                                            transitionMetals,
                                            test_mol_list,
                                            slice_mol_list,
                                            binding_full_mol_list,
                                            binding_slice_mol_list)

                    # print("first delete")
                    # view(final_slice_struct)
                    # input('done?')
                # make sure all neighbours of the binding sites are removed
                # except for the C in the carboxylate
                final_slice_struct = delete_binding_site_neigh(
                                        final_slice_struct, O_ids)

                # make sure there are no lone metals or hydrogens!
                # use any transition metal in non specific metal cases
                # IN SPECIFIC METAL CASES, ONLY sub_binder would be
                # present in the CIF anyway
                type_list_to_rm = ['H'] + [sub_binder] + transitionMetals
                final_slice_struct = delete_lone_atoms(final_slice_struct,
                                                       type_list_to_rm)
                # print("del lone atoms")
                # view(final_slice_struct)
                # input('done?')

                # update molecule lists
                final_struct_linker, f_slice_mol_list = get_separate_molecules(
                                    final_slice_struct, transitionMetals)
                # print('mol list')
                # print(f_slice_mol_list)
                # Add atoms to structure to complete molecules such that no
                # organic linkers are incorrectly broken
                # repeat the following block of code multiple
                # times to make sure all necessary atoms are added
                # stop when atoms_added in the last step == 0
                neigh_test = build_neigh_list(test_struct)
                atoms_added = 1000
                atoms_to_delete_from_slice = []
                while atoms_added > 0:
                    print("to delete:")
                    print(atoms_to_delete_from_slice)
                    # make neighbor list of final_slice and test_struct
                    neigh_final_slice = build_neigh_list(
                            final_slice_struct)

                    # make sure there are no unphysical cutting
                    # of molecules by checking the neighbor list of
                    # each atom in slice_struct after the clean above
                    # if the neigh list does not match that in the test
                    # struct then we add the necessary atoms
                    atoms_to_add_to_slice = []
                    for atom in final_slice_struct:
                        q = atom.charge
                        # skip if the atom is already designated to be deleted
                        if q in atoms_to_delete_from_slice:
                            continue
                        # allow for undercoordinated metals
                        if atom.symbol in transitionMetals:
                            continue
                        # and do not add anything to the binding oxygens
                        if q not in O_q:
                            test_idx = get_idx_from_charge(
                                    test_struct, q)
                            nl_slice, diss = neigh_final_slice.get_neighbors(
                                    atom.index)
                            nl_test, diss = neigh_test.get_neighbors(
                                    test_idx)
                            # convert neighbour lists to charge IDs
                            nl_slice = [get_q_from_idx(final_slice_struct,
                                                       i)
                                        for i in nl_slice]
                            nl_test = [get_q_from_idx(test_struct, i)
                                       for i in nl_test]
                            # get binding atoms neighbors in nl_test
                            nl_bind = []
                            for i in O_ids:
                                nl_binds, diss = neigh_test.get_neighbors(i)
                                nl_binds = [get_q_from_idx(test_struct, j)
                                            for j in nl_binds]
                                for j in nl_binds:
                                    nl_bind.append(j)
                            nl_bind = set(nl_bind)
                            new_lists = check_atoms_to_add(
                                            atoms_to_add_to_slice,
                                            atoms_to_delete_from_slice,
                                            test_struct, nl_test,
                                            transitionMetals, nl_slice,
                                            atom, q, lz, test_mol_list,
                                            f_slice_mol_list, nl_bind,
                                            binding_slice_mol_list,
                                            binding_full_mol_list)
                            atoms_to_add_to_slice, atoms_to_delete_from_slice = new_lists
                    # add missing atoms
                    for a in atoms_to_add_to_slice:
                        # get test_struct_id
                        idx = get_idx_from_charge(test_struct, a)
                        print('adding', a, idx, test_struct[idx])
                        final_slice_struct.append(test_struct[idx])
                    atoms_added = len(atoms_to_add_to_slice)
                    # update molecule lists
                    final_struct_linker, f_slice_mol_list = get_separate_molecules(
                                        final_slice_struct, transitionMetals)
                    # print('mol list')
                    # print(f_slice_mol_list)
                    # print("add atoms")
                    # view(final_slice_struct)
                    # input('done?')
                # delete atoms in molecules that would cause collisions
                del final_slice_struct[
                            [atom.index
                                for atom in final_slice_struct
                                if atom.charge in atoms_to_delete_from_slice]]
                # we want to check for and delete any metals with no neighbours
                # as the delete methodology does not check the metals
                type_list_to_rm = [sub_binder] + transitionMetals
                final_slice_struct = delete_lone_atoms(final_slice_struct,
                                                       type_list_to_rm)

                # update molecule lists
                final_struct_linker, f_slice_mol_list = get_separate_molecules(
                                    final_slice_struct, transitionMetals)

                # print("del lone atoms 2")
                # view(final_slice_struct)
                # input('done?')
                # print('mol list')
                # print(f_slice_mol_list)
                print("interface cleaned. Check for collisions...")
                # after cleaning we check if there
                # are still atoms above the binding oxygens
                # if there is then there is no point testing that binding plane
                collision = False
                # only_H_collisions = None
                maximum_z_of_all_atoms = max(
                        final_slice_struct.positions[:, 2])
                if maximum_z_of_all_atoms > lz+0.01:
                    collision = True

                    # view(final_slice_struct)
                    # input('done?')
                print("collisions?", collision)
                print('maximum Z of all atoms =',
                      round(maximum_z_of_all_atoms, 2), 'Angstrom')
                # print('H only collision?', only_H_collisions)
                print("------------------------------")
                if collision is False:
                    print("build binding interface and planes...")
                    # extract binding plane
                    binding_interface = final_slice_struct.copy()
                    # here we just take the slice surounding lz (+/- buffer)
                    # only extracting oxygens - all oyxgens!
                    binding_plane = binding_interface.copy()
                    # delete non binder atoms
                    # i.e. not in test spots
                    del binding_plane[
                            [atom.index
                             for atom in binding_plane
                             if atom.tag not in test_spots[lz]]]

                    # double check that no structures are duplicates
                    print("check for dupicate interfaces")
                    duplicate = check_for_duplicate_structures(binding_plane,
                                                               binding_output,
                                                               miller_str)
                    # append binding plane and binding interface to
                    # output dictionary
                    # keys = (miller of slab, config number)
                    # values = [binding interface, binding plane, lz]
                    if duplicate is False:
                        print("not a duplicate!")
                        # obtain number of metal-linker bonds lost during the
                        # creation of a suitable interface
                        final_bond_dict, final_i2t = get_bond_dictionary(
                                binding_interface)
                        final_bond_breakages = determine_final_broken_bonds(
                                    final_bond_dict, final_i2t,
                                    full_bond_dict, test_struct,
                                    broken_bonds_final
                                    )
                        # output to dictionary
                        binding_output[
                                (miller_str,
                                 str(config_out))] = [
                                 binding_interface,
                                 binding_plane, lz,
                                 final_bond_breakages]
                        millers_output.append(miller)
                        config_out += 1

            if miller in millers_output:
                millers_with_outs.append(miller)
            else:
                millers_done.append(miller_str)
    print("------------------------------")
    print("collate all binding structures")
    print("check again for duplicate interfaces")
    # transpose structures to cells where cell vector[2] is parallel to Z
    # functions are straight from old versions of the code
    final_binding_output = {}
    for key, value in binding_output.items():
        miller_str, config_out = key
        print(miller_str, config_out)
        binding_interface, binding_plane, lz, bond_data = value
        # binding plane
        translate_slab_to_Z_0(binding_plane)
        modify_cell_params(binding_plane)

        # binding interface
        translate_slab_to_Z_0_interface(binding_interface)
        # X and Y vectors of cell
        X_vec, Y_vec = binding_interface.cell[0], binding_interface.cell[1]
        binding_interface = convert_cell_to_straight_Z(binding_interface,
                                                       [X_vec, Y_vec])
        modify_cell_params_interface(binding_interface)

        # double check that no structures are duplicates
        duplicate = check_for_duplicate_structures(binding_plane,
                                                   final_binding_output,
                                                   miller_str)
        if duplicate is False:
            print('not a duplicate')
            # output structures to files
            prefix_name = film+"_"+miller_str+"_"+str(config_out)
            write_binding_planes(prefix_name, binding_interface,
                                 binding_plane, bond_data)
            # print('binding plane:')
            # view(binding_plane)
            # write to final dictionary
            final_binding_output[key] = [binding_interface, binding_plane, lz]
    print("all done.")
    return final_binding_output, millers_output


def determine_final_broken_bonds(final_bond_dict, final_i2t,
                                 full_bond_dict, test_struct,
                                 broken_bonds_final):
    """Determine the final set of broken bonds after interface cleaning.

    Bonds defined as ID Type - ID Type

    Keywords:
        final_bond_dict (dict) - bonding dictionary for final structure
        final_i2t (dict) - ID to Tag translation dict for final structure
        full_bond_dict (dict) - bonding dictionary for full structure
        test_struct (ASE Atoms) - ASE Atoms object of full structure
        broken_bonds_final (list) - list of broken bonds pre cleaning

    Returns:
        final_bond_breakages (list) - all bonds broken

    """
    final_bond_breakages = []
    for atag, abonds in final_bond_dict.items():
        # convert final index into final tag
        final_tag = final_i2t[atag]
        # get full_bond index from final_tag
        # NOTE TAGS ARE CONTINUOUS IN ALL STRUCTS
        full_bond_index = final_tag
        # abonds = ([indices],[tags])
        # if [tags] not the same as full_bond_dict [tags]
        # implies broken bonds
        if abonds[1] != full_bond_dict[full_bond_index][1]:
            # get broken bonds as the index of the
            # atom in test_struct fo which full_bond_index
            # has had its bond broken to
            broken_bonds = [i
                            for i, j in zip(*full_bond_dict[full_bond_index])
                            if j not in abonds[1]]
            # need to check that the broken bond does not
            # straddle the Z PBC i.e. go from bottom of the
            # cell to the top of the cell get difference in
            # i dimension position from test_struct if >
            # half cell width (HCW) then ignore this broken
            # bond. for now only ignore those crossing the
            # Z PBC
            for bb in broken_bonds:
                atom1_position = test_struct[full_bond_index].position
                atom2_position = test_struct[bb].position
                vector = abs(
                        atom1_position - atom2_position)
                # if the Z coordinate of the distance
                # between them is > 3 angstrom then it is
                # fair to assume that the atoms are
                # straddling a periodic boundary
                if vector[2] < 3.0:
                    # implies NOT PBC staddler
                    # append to broken_bonds_final
                    broken_bonds_final.append(bb)
                    # get the atom types of broken bonds
                    atom1_type = test_struct[full_bond_index].symbol
                    atom2_type = test_struct[bb].symbol

                    # bond data to be saved:
                    # all atom ids are wrt slab structure
                    # atom1 id, atom1 symbol,
                    # atom2 id, atom2 symbol
                    bond_data = (full_bond_index,
                                 atom1_type,
                                 bb, atom2_type)
                    final_bond_breakages.append(bond_data)

    return final_bond_breakages


def check_for_duplicate_structures(bind_plane, bind_output, miller_str):
    """Check dictionaries to be output for duplicate binding structures.

    Duplicates are determined by two tests:
        1 - same number of atoms in plane
            must be passed
        2 - same fractional XY coords
            can fail if the next test passes
        3 - atom positions relative to their centre of mass are similar
            should pass if test 2 passes - can pass if test 2 fails

    Arguments:
        bind_plane (ASE Atoms) - ASE Atoms structure of binding plane being
            tested
        bind_output (dict) - dictionary of binding structures + data
        miller_str (str) - Miller indices in string format

    Returns:
        duplicate (bool) - True if interface is found to be a duplicate

    """

    # hardcoded tolerance on the whether two configurations are equivalent in
    # test 3.
    tol_u = 0.25

    b_struct = bind_plane.copy()

    duplicate = False
    for exist_key, exist_result in bind_output.items():
        if exist_key[0] == miller_str:
            e_struct = exist_result[1].copy()
            # same number of atoms
            if len(e_struct) == len(b_struct):
                e_frac_coord = e_struct.get_scaled_positions()
                b_frac_coord = b_struct.get_scaled_positions()
                x_e_frac_coord = [
                        round(i, 4) for i in e_frac_coord[:, 0]
                                 ]
                y_e_frac_coord = [
                        round(i, 4) for i in e_frac_coord[:, 1]
                                 ]
                x_b_frac_coord = [
                        round(i, 4) for i in b_frac_coord[:, 0]
                                 ]
                y_b_frac_coord = [
                        round(i, 4) for i in b_frac_coord[:, 1]
                                 ]
                test2x = np.all(sorted(x_e_frac_coord) == sorted(x_b_frac_coord))
                test2y = np.all(sorted(y_e_frac_coord) == sorted(y_b_frac_coord))
                if test2x is True and test2y is True:
                    # get distance array of all oxygens
                    e_dist_array = sorted(pdist(e_struct.positions))
                    b_dist_array = sorted(pdist(b_struct.positions))
                    # avoid rounding errors
                    e_dist_array = [round(i, 4) for i in e_dist_array]
                    b_dist_array = [round(i, 4) for i in b_dist_array]
                    # if the sorted distance arrays are all the same
                    # then assume duplicate
                    if np.all(e_dist_array == b_dist_array):
                        duplicate = True
                else:
                    # do test 3
                    for atom in e_struct:
                        atom.z = 0
                    for atom in b_struct:
                        atom.z = 0
                    e_COP = [np.average(e_struct.positions[:, 0]),
                             np.average(e_struct.positions[:, 1])]
                    e_struct.translate([-e_COP[0], -e_COP[1], 0])
                    b_COP = [np.average(b_struct.positions[:, 0]),
                             np.average(b_struct.positions[:, 1])]
                    b_struct.translate([-b_COP[0], -b_COP[1], 0])
                    dists = get_distance_bet_structs(e_struct,
                                                     b_struct,
                                                     verbose=False)
                    min_dist, avg_dist, max_dist = dists
                    print('max pair distance =', max_dist)
                    if max_dist ** 2 < tol_u ** 2:
                        print('is duplicate!')
                        duplicate = True
    return duplicate


def write_binding_planes(prefix_name, binding_interface,
                         binding_plane, bond_data):
    """Write binding plane information to structure and text files.

    Arguments:
        prefix_name (str) - prefix for output file names
        binding_interface (ASE Atoms) - binding interface Atoms structure
        binding_plane (ASE Atoms) - binding plane Atoms structure
        bond_data (list) - list of broken bonds to make interface
    """
    inter_out_name = prefix_name+"_bind_inter.cif"
    bonds_out_name = prefix_name+"_bind_bonds.txt"
    plane_out_name = prefix_name+"_bind_plane.cif"
    binding_interface.write(format='cif', filename=inter_out_name)
    binding_plane.write(format='cif', filename=plane_out_name)

    # output bonds to file
    with open(bonds_out_name, 'w') as f:
        f.write("atom1_id,atom1_type,atom2_id,atom2_type"+'\n')
        for bond in bond_data:
            f.write(str(bond[0])+','+str(bond[1])+',')
            f.write(str(bond[2])+','+str(bond[3])+'\n')


def get_binding_molecules(O_ids, test_struct, test_mol_list,
                          slice_mol_list):
    """Determine which molecules have binding atoms in them for this slice.

    Ligands are assumed to be separate molecules if they are not connected when
    transition metals are removed.

    Arguments:
        O_ids (list) - atom ids for binding oxygens
        test_struct (ASE Atoms) - ASE Atoms object of full structure
        test_mol_list (list) - list of molecules present in full structure
        slice_mol_list (list) - list of molecules present in sliced structure

    Returns:
        binding_slice_mol_list (list) - list of molecules with binding sites for
            slice structure
        binding_full_mol_list (list) - list of molecules with binding sites for
            full structure

    """
    binding_slice_mol_list = []
    binding_full_mol_list = []
    for b in O_ids:
        bq = get_q_from_idx(test_struct, b)
        for m in slice_mol_list:
            if bq in m and slice_mol_list.index(m) not in binding_slice_mol_list:
                binding_slice_mol_list.append(
                        slice_mol_list.index(m))
                break
        for m in test_mol_list:
            if bq in m and test_mol_list.index(m) not in binding_full_mol_list:
                binding_full_mol_list.append(
                        test_mol_list.index(m))
                break
    return binding_slice_mol_list, binding_full_mol_list


def delete_lone_atoms(struct, types):
    """Delete any atoms of type in types with no neighbours.

    Keywords:
        struct (ASE Atoms) - ASE structure
        types (list) - atom symbols to delete if they have no neighbours
    Returns:
        struct_clean (ASE Atoms) - ASE structure with lone atoms deleted
    """
    struct_clean = struct.copy()
    neigh_final = build_neigh_list(struct_clean)
    metals_or_H_to_del = []
    for atom in struct_clean:
        if atom.symbol in types:
            nl, diss = neigh_final.get_neighbors(
                    atom.index)
            # if type has no neighbours delete it
            if len(nl) == 0:
                metals_or_H_to_del.append(atom.charge)
            # if its only neighbours are of the same type - delete it
            else:
                # print(nl)
                # neighbor types
                neigh_types = [struct_clean[i].symbol for i in nl]
                # print(neigh_types)
                all_neigh_also_rm = True
                for type in neigh_types:
                    if type not in types:
                        all_neigh_also_rm = False
                if all_neigh_also_rm is True:
                    metals_or_H_to_del.append(atom.charge)

    # delete atoms
    del struct_clean[
            [atom.index
             for atom in struct_clean
             if atom.charge in metals_or_H_to_del]]

    return struct_clean


def merge_test_spots(test_spots, buffer):
    """Merge multiple binding sites into one if all within some buffer distance

    Keywords:
        test_spots (list) - test spots (Z positions in Angstrom)
    Returns:
        test_spots (list) - merged test spots (Z positions in Angstrom)

    """

    lz_merged = []
    for lz in test_spots.keys():
        O_ids = test_spots[lz]

        for T_keys in test_spots.keys():
            if T_keys not in lz_merged:
                if T_keys != lz and abs(T_keys - lz) < buffer:
                    # want to only keep the highest Z test
                    if T_keys > lz:
                        # add ids from T_keys to oxygen ids
                        for O in O_ids:
                            if O not in test_spots[T_keys]:
                                test_spots[T_keys].append(O)
                        lz_merged.append(lz)
                    elif T_keys <= lz:
                        # add ids from T_keys to oxygen ids
                        for O in test_spots[T_keys]:
                            if O not in O_ids:
                                O_ids.append(O)
                        lz_merged.append(T_keys)

    # remove from test_spots Z positions that were merged with others
    for T in list(set(lz_merged)):
        del test_spots[T]
    return test_spots


def clean_interface_buffer(slice_struct, lz, buffer, transitionMetals,
                           test_mol_list, slice_mol_list,
                           binding_full_mol_list,
                           binding_slice_mol_list):
    """Clean interface by removing molecules with atoms in buffer zone.

    After slicing, all atoms in the buffer zone (buffer) attached to molecules
    that do not contain binding atoms are removed (as well as the rest of their
    molecule). This code could be simplified dramatically, but in its currrent
    state forces the need to make sure all organic ligands can be rebuilt
    without causing collisions.

    Arguments:
        slice_struct (ASE Atoms) - ASE Atoms object of sliced structure
        lz (float) - slicing postion in Angstrom
        buffer (float) - size of buffer zone in Angstrom
        transitionMetals (list) - list of transition metal atom symbols
        test_struct (ASE Atoms) - ASE Atoms object of full structure
        test_mol_list (list) - list of molecules present in full structure
        slice_mol_list (list) - list of molecules present in sliced structure
        binding_slice_mol_list (list) - list of molecules with binding sites
            for slice structure
        binding_full_mol_list (list) - list of molecules with binding sites for
            full structure
    Returns:
        clean_slice_struct (ASE Atoms) - copy of input structure with cleaning
    """
    atoms_to_delete_from_slice = []
    for atom in slice_struct:
        if atom.symbol in transitionMetals:
            continue
        # in buffer zone
        if atom.z < lz+0.01 and atom.z > lz - buffer:
            # get molecule id
            for mid, mol in enumerate(test_mol_list):
                if atom.charge in mol:
                    m_full_id = mid
                    break
            for mid, mol in enumerate(slice_mol_list):
                if atom.charge in mol:
                    m_slice_id = mid
                    break
            # not in binding molecules
            if m_slice_id not in binding_slice_mol_list:
                if m_full_id not in binding_full_mol_list:
                    # append charge of all atoms in molecules
                    # to delete list
                    for _atom in slice_mol_list[m_slice_id]:
                        atoms_to_delete_from_slice.append(_atom)

    # delete atoms
    final_slice_struct = slice_struct.copy()
    del final_slice_struct[[atom.index
                            for atom in final_slice_struct
                            if atom.charge in atoms_to_delete_from_slice]]
    return final_slice_struct


def get_completed_structures(film):
    """Check if binding structures have been calculated already and collect the
    necessary information from them.
    """

    final_binding_output = {}
    millers_output = []

    plane_files = glob.glob(film+"_*_bind_plane.cif")
    inter_files = glob.glob(film+"_*_bind_inter.cif")
    bonds_files = glob.glob(film+"_*_bind_bonds.txt")

    # are there files and the same amount for both plane and inter?
    # if so then we have calculated the binding planes already!
    if len(plane_files) > 0 and len(plane_files) == len(inter_files):
        if len(bonds_files) == len(plane_files):
            for plane in plane_files:
                p = plane.replace(film+"_", "")
                p = p.replace("_bind_plane.cif", "")
                miller_str, config_out = p.split("_")
                for inter in inter_files:
                    i = inter.replace(film+"_", "")
                    i = i.replace("_bind_inter.cif", "")
                    imiller_str, iconfig_out = i.split("_")
                    if imiller_str == miller_str and iconfig_out == config_out:
                        # set lz
                        # (the z position where the miller plane is sliced)
                        # to 0 -- it is not used anywhere else.
                        lz = 0
                        binding_interface = read(filename=inter)
                        binding_plane = read(filename=plane)
                        key = (miller_str, config_out)
                        final_binding_output[key] = [binding_interface,
                                                     binding_plane, lz]
                        miller = genf.miller_reader(miller_str)
                        millers_output.append((miller[0],
                                               miller[1],
                                               miller[2]))
                        break
            return [True, final_binding_output, millers_output]
        else:
            return [False]
    else:
        return [False]


def find_metal(ASE_struct, binder):
    """Find the desired binder metal in ASE Atoms object.

    """
    nstruct = ASE_struct
    # loop through all atoms present checking for each test
    if len(nstruct) > 0:
        all_tests_passed = True
        # only keep binding atoms
        del nstruct[
                [atom.index
                 for atom in nstruct if atom.symbol not in [binder]]]
        return all_tests_passed, nstruct
    return [False]


def produce_surface_slabs(surf, substrate, surface_millers,
                          sub_des_types, buffer):
    """Obtain binding planes of the desired metal for substrate.

    This code will produce only one configuration, effectively taking the top
    layer of binding atoms.

    Keyword arguments:
        surf (str) - name of surface/substrate
        substrate (pymatgen structure object) - Structure from CIF file
        surface_millers (list) - miller planes of interest (* if non-specific)
        sub_des_types (list) - binding atom types for surface/substrate
        buffer (float) - width of increment in Z dimension = 2 * buffer
    """
    s_millers = []
    sub_structures = {}
    if surface_millers is None:
        # implies we do not know if there is preferential lattice match
        # therefore generate all slabs
        all_sub_slabs = generate_all_slabs(substrate, max_index=1,
                                           min_slab_size=0.5,
                                           min_vacuum_size=0.0,
                                           primitive=False)
        for sub_slabs in all_sub_slabs:
            mill = sub_slabs.miller_index
            miller_name = str(mill[0])+str(mill[1])+str(mill[2])
            # save to CIF
            sub_slabs.to(filename=surf+"_"+miller_name+"_surf.cif",
                         fmt='cif')
            # output CIF
            sub_out_cif_name = surf+"_"+miller_name+"_surf_out.cif"
            # USE ASE SCRIPT TO FIND CONFIGURATION
            # AND PRODUCE DICT OF STRUCTURES
            # produce a dictionary of all binding structures
            struct = read(surf+"_"+miller_name+"_surf.cif")
            out_struct = Atoms()
            # remove undesired types
            del struct[[
                    atom.index
                    for atom in struct if atom.symbol not in [sub_des_types]]]
            # loop from - ZLP to + ZLP
            # scanning atoms within +/- 1 angstrom from Z position
            z_lp = struct.get_cell_lengths_and_angles()[2]
            for l in np.arange(-z_lp, z_lp, 0.5):
                z_range = [l-buffer, l+buffer]
                nstruct = struct.copy()
                # remove undesirable atoms
                del nstruct[[
                        atom.index
                        for atom in nstruct
                        if atom.z < z_range[0] or atom.z > z_range[1]]]
                res = find_metal(nstruct, sub_des_types)
                if res[0] is True:
                    out_struct = res[1]
                    # stop when you find the configuration desired
                    S_Z = l
                    translate_slab_to_Z_0(out_struct)
                    modify_cell_params(out_struct)
                    out_struct.write(format='cif',
                                     filename=sub_out_cif_name)
                    sub_structures[miller_name] = [out_struct, S_Z]
                    s_millers.append(mill)
                    break
            ##############################
    else:
        # only generate preferntial slabs
        for mill in surface_millers:
            miller_name = str(mill[0])+str(mill[1])+str(mill[2])
            sub_slabs = SlabGenerator(substrate, miller_index=mill,
                                      min_slab_size=0.5,
                                      min_vacuum_size=0,
                                      primitive=False)
            new_slab = sub_slabs.get_slab()
            # save to CIF
            new_slab.to(filename=surf+"_"+miller_name+"_surf.cif",
                        fmt='cif')
            sub_out_cif_name = surf+"_"+miller_name+"_surf_out.cif"
            # USE ASE SCRIPT TO FIND CONFIGURATION AND PRODUCE DICT OF
            # STRUCTURES
            # produce a dictionary of all binding structures
            struct = read(surf+"_"+miller_name+"_surf.cif")
            out_struct = Atoms()
            # remove undesired types
            del struct[[
                    atom.index
                    for atom in struct if atom.symbol not in [sub_des_types]]]
            # loop from - ZLP to + ZLP
            # scanning atoms within +/- 1 angstrom from Z position
            z_lp = struct.get_cell_lengths_and_angles()[2]
            for l in np.arange(-z_lp, z_lp, 0.5):
                z_range = [l-buffer, l+buffer]
                nstruct = struct.copy()
                # remove undesirable atoms
                del nstruct[[
                        atom.index
                        for atom in nstruct
                        if atom.z < z_range[0] or atom.z > z_range[1]]]
                res = find_metal(nstruct, sub_des_types)
                if res[0] is True:
                    out_struct = res[1]
                    # stop when you find the configuration desired
                    S_Z = l
                    translate_slab_to_Z_0(out_struct)
                    modify_cell_params(out_struct)
                    out_struct.write(format='cif',
                                     filename=sub_out_cif_name)
                    sub_structures[miller_name] = [out_struct,
                                                   S_Z]
                    s_millers.append(mill)
                    break
    return sub_structures, s_millers


def delete_binding_site_neigh(struct, O_q):
    """Delete any atoms of type other than "C" from neighbour list of binding
       sites.

    Keywords:
        struct (ASE Atoms) - ASE structure
        O_q (list) - atom ids for binding oxygens based on charge definition
    Returns:
        struct_clean (ASE Atoms) - ASE structure with lone atoms deleted
    """
    atoms_to_del = []
    struct_clean = struct.copy()
    neigh_final = build_neigh_list(struct_clean)
    # convert O_ids into final slice structure relevant IDs
    final_O_ids = [get_idx_from_charge(struct_clean, i)
                   for i in O_q]
    # print(final_O_ids)
    for O in final_O_ids:
        nl, diss = neigh_final.get_neighbors(O)
        # print(nl)
        for n in nl:
            if struct_clean[n].index in final_O_ids:
                continue
            if struct_clean[n].symbol != 'C':
                print('binding site neighbour to delete',
                      struct_clean[n].charge,
                      'is', struct_clean[n].symbol)
                atoms_to_del.append(struct_clean[n].charge)
                # print(struct_clean[n])

    # note that if we delete any atoms that are not C and belong in the Ligands
    # they will be added by atom adding section
    # delete atoms
    del struct_clean[
            [atom.index
             for atom in struct_clean
             if atom.charge in atoms_to_del]]

    return struct_clean


def analyze_bonds(slice_bond_dict, slice_i2t, full_bond_dict, test_struct,
                  transitionMetals):
    """Find and analyze broken bonds.

    Keywords:
        slice_bond_dict (dict) - bonding dictionary for full structure
        slice_i2t (dict) - conversion between index and tag convention
        full_bond_dict (dict) - bonding dictionary for full structure
        test_struct (ASE Atoms) - ASE Atoms object of full structure
        transitionMetals (list) - list of transition metal atom symbols

    Returns:
        organic_bond_broke (bool) - switch to say if organic bonds were broken
        broken_bonds_final (list) - list of bonds that were broken by slicing
    """

    organic_bond_broke = False
    broken_bonds_final = []
    for atag, abonds in slice_bond_dict.items():
        # convert slice index into slice tag
        slice_tag = slice_i2t[atag]
        # get full_bond index from slice_tag
        # NOTE TAGS ARE CONTINUOUS IN ALL STRUCTS
        # slice tag = full_struct index
        full_bond_index = slice_tag
        # abonds = ([indices],[tags])
        # if [tags] not the same as full_bond_dict [tags]
        # implies broken bonds
        if abonds[1] != full_bond_dict[full_bond_index][1]:
            # get broken bonds as the index of the
            # atom in test_struct fo which full_bond_index
            # has had its bond broken to
            broken_bonds = [i for i, j in zip(*full_bond_dict[full_bond_index]) if j not in abonds[1]]
            # need to check that the broken bond does not
            # straddle the Z PBC i.e. go from bottom of the cell
            # to the top of the cell get difference in i dimension
            # position from test_struct if > half cell width (HCW)
            # then ignore this broken bond. for now only ignore
            # those crossing the Z PBC
            for bb in broken_bonds:
                atom1_position = test_struct[full_bond_index].position
                atom2_position = test_struct[bb].position
                vector = abs(atom1_position - atom2_position)
                # if the Z coordinate of the distance between
                # them is > 3 angstrom then it is fair to assume
                # that the atoms are straddling a periodic boundary
                if vector[2] < 3:
                    # implies NOT PBC staddler
                    # append to broken_bonds_final
                    broken_bonds_final.append(bb)
                    # get the atom types of broken bonds
                    atom1_type = test_struct[full_bond_index].symbol
                    atom2_type = test_struct[bb].symbol
                    # if either type is in transitionMetals
                    # then it is not organic
                    # else assume organic
                    if atom1_type not in transitionMetals:
                        if atom2_type not in transitionMetals:
                            organic_bond_broke = True

    return organic_bond_broke, broken_bonds_final


def get_separate_molecules(struct, transitionMetals):
    """Get structure and list of separate entities in a structure.

    Keywords:
        struct (ASE Atoms) - Structure to get molecule list from
        transitionMetals (list) - list of transition metal atom symbols

    Returns:
        struct_linker (ASE Atoms) - ASE structure with no metals
        mol_list (list) - list of molecules and contained atoms for structure.
    """
    struct_linker = struct.copy()
    del struct_linker[
            [atom.index
             for atom in struct_linker
             if atom.symbol in transitionMetals]]
    # get molecule list
    # list of separate entities in the structure
    # sorted by size
    mol_list = get_molecule_list(struct_linker)
    # convert test_mol_list to charges of slice atoms
    # therefore matching IDs as test_struct
    mol_list = [
        [get_q_from_idx(struct_linker, i)
         for i in j] for j in mol_list]
    return struct_linker, mol_list


def check_atoms_to_add(atoms_to_add_to_slice, atoms_to_delete_from_slice,
                       test_struct, nl_test, transitionMetals, nl_slice,
                       atom, q, lz, test_mol_list, slice_mol_list, nl_bind,
                       binding_slice_mol_list, binding_full_mol_list):
    """Check the atoms that need to be added for collisions and PBC issues.

    Collision defined as any atom with a Z postion > binding atoms Z position.
    PBC issues defined by bonds that cross the Z periodic boundary.

    Keywords:
        atoms_to_add_to_slice (list) - list of atoms to add to complete ligands
        atoms_to_delete_from_slice (list) - list of atoms to delete after
            adding atoms to complete ligands
        test_struct (ASE Atoms) - ASE Atoms object of full structure
        nl_test (list) - list of neighbours to atom from test_struct
        transitionMetals (list) - list of transition metal atom symbols
        nl_slice (list) - list of neighbours to atom from slice_struct
        atom (ASE Atom) - Atom whose neighbours we are testing
        q (int) - atom identifier
        lz (float) - Z position of slice
        test_mol_list (list) - list of molecules present in full structure
        slice_mol_list (list) - list of molecules present in sliced structure
        nl_bind (list) - list of neighbours of binding atoms in test_struct
        binding_slice_mol_list (list) - list of molecules with binding sites
            for slice structure
        binding_full_mol_list (list) - list of molecules with binding sites for
            full structure

    Returns:
        atoms_to_add_to_slice (list) - list of atoms to add to complete ligands
        atoms_to_delete_from_slice (list) - list of atoms to delete after
            adding atoms to complete ligands

    """
    for n in nl_test:
        n_atom = test_struct[
                get_idx_from_charge(test_struct,
                                    n)]
        if n in atoms_to_add_to_slice:
            continue
        # we do not want to add transition metals
        if n_atom.symbol in transitionMetals:
            continue
        if n not in nl_slice:
            # print(atom)
            # print(n_atom)
            # print('-')
            # only append to add atoms list if not Z PBC straddlers
            # ie. atom and n not > 3 Angstrom
            # (just use some set value as bonded atoms are not going to
            # be 3 Angstoms apart)
            atom1_position = atom.position
            atom2_position = n_atom.position
            vector = abs(atom1_position - atom2_position)
            # print(vector)
            if vector[2] < 3:
                # will the new atom cause a collision?
                # print(atom2_position)
                if atom2_position[2] > lz+0.01:
                    print("adding atoms leads to collision")
                    print("delete ligand??")
                    # collision
                    # can we remove the whole ligand?
                    # if we cannot, we must add atoms
                    # does ligand belong to binding molecule?
                    #   - if yes then we cannot delete the ligand
                    # ONE FUNCTION #
                    in_binding = True
                    for mid, mol in enumerate(test_mol_list):
                        if q in mol:
                            m_full_id = mid
                            break
                    for mid, mol in enumerate(slice_mol_list):
                        if q in mol:
                            m_slice_id = mid
                            break
                    # not in binding molecules
                    if m_slice_id not in binding_slice_mol_list:
                        if m_full_id not in binding_full_mol_list:
                            in_binding = False
                    if in_binding is False:
                        # ONE FUNCTION #
                        # append charge of all atoms in
                        # molecules to delete list
                        for _atom in slice_mol_list[m_slice_id]:
                            atoms_to_delete_from_slice.append(_atom)
                        print("these atoms are not in binding ligands:")
                        print(atoms_to_delete_from_slice)
                        print('they will be deleted...')
                        continue
                    else:
                        print("atom in binding ligands - they will be added...")
                        print("even if bound directly to binding sites")
                        atoms_to_add_to_slice.append(n)
                # if the atom does not cause a collision:
                # do not append atom if it is a neighbour of the binding
                # atoms
                elif n not in nl_bind:
                    # append atom charge to atoms to add list
                    atoms_to_add_to_slice.append(n)

    return atoms_to_add_to_slice, atoms_to_delete_from_slice
