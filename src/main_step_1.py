#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Script that runs step 1 and step 2 on a series of CIFs in the current
directory.
"""

import pandas as pd
import time
import argparse
import sys
import os
from ase.io import read
import pymatgen as mg
import general_functions as genf
import chemistry as chemistry
import lattice as lattice
import matplotlib
matplotlib.use('agg')

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

genf.print_welcome_message()
start_time = time.time()

# parse CL arguments
parser = argparse.ArgumentParser(description="Run main step 1")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
parser.add_argument('NP', metavar='NP', type=int, nargs='+',
                    help='process to run. associated with CIFS_TODO_NP.csv')


args = parser.parse_args()
NP = args.NP[0]
param_file = args.paramfile[0]

# read in parameter dictionary
param_dict = genf.read_parameter_file(param_file)

# read in CIF TODO list as dataframe
CIF_TODO_file = param_dict['TODO_name'][0]+"_step_1_"+str(NP)+".csv"
if os.path.isfile(CIF_TODO_file) is True:
    CIF_TODO = pd.read_csv(CIF_TODO_file)
else:
    sys.exit("CIF_TODO file does not exist! Exitting!")

# porosity file
# porosity_file = param_dict['porosity_csv'][0]

# status meaning
# status = 0 : not tested
# status = 1 : did not pass chemistry test
# status = 2 : passed chemistry test
# status = 3 : did not pass lattice test
# status = 4 : passed lattice test
# status = 4a : binding planes built
# status = 5 : completed ASO screening


for index, row in CIF_TODO.iterrows():
    status = row['status']
    surface = row['surface name']
    film = row['film name']
    film_metal = param_dict['film_metal'][0]
    print("=== surface: ",
          surface,
          " -- film:",
          film,
          "===")

    if status == 0:
        # do chemistry test
        print("------------------------------------------------------")
        print("doing chemistry test")
        pass_chemistry = False
        # Run Cull
        cif = film+".cif"
        # CHECK METAL PRESENCE
        # Read CIF coordinates - confirm that the desired metal is
        # there and is the only metal there
        metal = film_metal
        if metal != '*':
            if chemistry.metal_present(cif, metal):
                # CHECK LIGAND PRESENCE WITH ASE
                # use repeated structure to avoid PBC issues!
                nstruct = read(cif).repeat([2, 2, 2])
                res = chemistry.find_carboxylate(nstruct, metal)
                if res is True:
                    # check SA > 0:
                    # 26/03/18: removed porosity check for now!
                    # poro_data = pd.read_csv(porosity_file)
                    # prop = float(poro_data[poro_data['mof'] == film]
                    # [porosity_prop])
                    # if prop > 0:
                    pass_chemistry = True
        elif metal == '*':
            # CHECK LIGAND PRESENCE
            # WITH ASE
            # with no specific metal
            # use repeated structure to avoid PBC issues!
            nstruct = read(cif).repeat([2, 2, 2])
            res = chemistry.find_carboxylate_no_metal(nstruct)
            if res is True:
                # check SA > 0:
                # 26/03/18: removed porosity check for now!
                # poro_data = pd.read_csv(porosity_file)
                # prop = float(poro_data[poro_data['mof'] == film]
                # [porosity_prop])
                # if prop > 0:
                pass_chemistry = True
        if pass_chemistry is False:
            status = 1
        else:
            status = 2
            print("passed chemistry test")
    if status == 1:
        print("failed chemistry test")

    if status == 2:
        # do lattice test
        print("------------------------------------------------------")
        print("doing lattice test")
        # set lattice rules
        max_area_ratio_tol = float(param_dict['max_area_ratio_tol'][0])
        max_area = int(param_dict['max_area'][0])
        film_max_miller = int(param_dict['film_max_miller'][0])
        substrate_max_miller = int(param_dict['sub_max_miller'][0])
        max_length_tol = float(param_dict['max_length_tol'][0])
        max_angle_tol = float(param_dict['max_angle_tol'][0])
        # maximum area multiple to test
        f_max_area_multi = float(param_dict['f_max_area_multi'][0])
        s_max_area_multi = float(param_dict['s_max_area_multi'][0])

        # read in pmg structures
        # we only make the film structure primitive.
        # (this is consistent throughout the code)
        surf_mg_struct = mg.Structure.from_file(surface+".cif")
        film_mg_struct = mg.Structure.from_file(film+".cif").get_primitive_structure()
        # if there is known preferential miller orientation
        # check if there is a match that agrees with it
        # else (below) we check the unknown case
        # set the substrate millers based on known
        # preferred orientations of particular substrates
        # IF THEY ARE KNOWN! (* if they are not)
        if param_dict['surf_millers'][0] == "*":
            substrate_millers = None
        else:
            substrate_millers = [
                    genf.miller_reader(param_dict['surf_millers'][0])]

        pass_lattice = lattice.lattice_cull(surf_mg_struct, film_mg_struct,
                                            substrate_millers,
                                            max_area_ratio_tol,
                                            max_length_tol,
                                            max_angle_tol,
                                            f_max_area_multi,
                                            s_max_area_multi,
                                            film_max_miller,
                                            substrate_max_miller)
        if pass_lattice is False:
            status = 3
            print("failed lattice test")
        else:
            status = 4
            print("passed lattice test")

    # update status in CIF_TODO
    row['status'] = status
    CIF_TODO.loc[index] = row
    # update file
    CIF_TODO.to_csv(CIF_TODO_file, index=False)

end_time = time.time()
print("=====================================================================")
print("All Done!!! --", "time taken was:",
      "{0:.2f}".format(end_time-start_time), "s")
print("=====================================================================")
