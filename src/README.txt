UNIX commands to run epitMOF from terminal in a directory with CIFs of interest:

1 - edit parameters in screening_parameters.txt (see an example in examples/)

2 - run script to write CIF TODO list:
    python3 SRCDIR/write_TODO_step_1.py screening_parameters.txt

3 - run step 1+2 of the screening algorithm (run for each process NP)
    python3 SRCDIR/main_step_1.py screening_parameters.txt NP > epitmof_step_1_NP.txt &

4 - run script to write CIF TODO list for step 3:
    python3 SRCDIR/write_TODO_step_3.py screening_parameters.txt

5 - run check for interpenetrated structures (run for each process NP)
    python3 SRCDIR/main_inter.py screening_parameters.txt NP > epitmof_inter_NP.txt &

(optional: 6 is done within 7 if not done before hand.)
6 - build all binding planes of each CIF (run for each process NP)
    python3 SRCDIR/main_bp.py screening_parameters.txt NP > epitmof_bp_NP.txt &

7 - run step 3 (ASO calculation) screening step (run for each process NP)
    python3 SRCDIR/main_step_3.py screening_parameters.txt NP > epitmof_step_3_NP.txt &


ANYTIME - run get_progress to see your progress
	python3 SRCDIR/get_progress.py screening_parameters.txt
