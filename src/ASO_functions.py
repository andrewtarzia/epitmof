#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used to prepare binding planes for ASO calculations.
"""

import numpy as np
import pymatgen as mg
from ase import Atoms, Atom
from super_cell import align_lattices
from math_functions import rotation_matrix
from pmg_substrate_analyzer import vec_angle

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def get_replication_vect(rpf1, rpf2, match):
    """
    Get vector to multiply supercell vectors by to span at least 3X and 3Y.

    Arguments:
        rpf1 (numpy array): reduced film unit-cell vector a
        rpf2 (numpy array): reduced film unit-cell vector b
        match (pmg match object): object containing super-cell information
    Returns:
        multi_1 (int): multiplication of super-cell vector u
        multi_2 (int): multiplication of super-cell vector v
    """
    # replication algorithm such that
    # the super cell spans at least 3 * the X and Y
    # components of the film unit cell
    a = rpf1[:2]
    b = rpf2[:2]
    u = match[0][0][:2]
    v = match[0][1][:2]
    for m in np.arange(1, 10):
        for n in np.arange(1, 10):
            # transpose mU and nV array to get the correct
            # matrix algebra
            auv = np.linalg.solve(np.array([m*u, n*v]).T,
                                  a)
            buv = np.linalg.solve(np.array([m*u, n*v]).T,
                                  b)
            na = (auv[0])**2 + (auv[1])**2
            na = 1/np.sqrt(na)
            nb = (buv[0])**2 + (buv[1])**2
            nb = 1/np.sqrt(nb)
            # print(a, b, u, v, auv, buv, m, n, na, nb)
            if na >= 3 and nb >= 3:
                break
        if na >= 3 and nb >= 3:
            multi_1, multi_2 = m, n
            break
    return multi_1, multi_2


def check_match_duplicates(matches_done, new_m):
    """
    Check if match has been tested already after replication and reduction.

    Arguments:
        matches_done (list): list of matches that have been completed
        new_m (dict): current match to be tested after replication/reduction.

    Returns:
        match_done (bool): True if match has been done.
    """
    match_done = False
    if len(matches_done) > 0:
        for old_m in matches_done:
            # compare each DICT to new_m DICT
            if np.all(new_m['film_sl_vecs'] == old_m['film_sl_vecs']):
                if np.all(new_m['sub_sl_vecs'] == old_m['sub_sl_vecs']):
                    match_done = True
    return match_done


def plot_replication(match, new_f_match, rpf1, rpf2,
                     plot=False):
    """Plot information for debugging translations

    """
    if plot is False:
        return 0
    import matplotlib.pyplot as plt
    plt.figure(figsize=(5, 5))
    plt.scatter(0, 0, c='k')
    plt.scatter(rpf1[0],
                rpf1[1], c='b', alpha=0.4)
    plt.scatter(rpf2[0],
                rpf2[1], c='b', alpha=0.4)
    plt.scatter(rpf1[0]*3,
                rpf1[1]*3, c='green', alpha=0.4)
    plt.scatter(rpf2[0]*3,
                rpf2[1]*3, c='green', alpha=0.4)
    plt.scatter(match[0][0][0],
                match[0][0][1], c='r', alpha=0.4)
    plt.scatter(match[0][1][0],
                match[0][1][1], c='r', alpha=0.4)
    plt.scatter(new_f_match[0][0],
                new_f_match[0][1], c='orange', alpha=0.4)
    plt.scatter(new_f_match[1][0],
                new_f_match[1][1], c='orange', alpha=0.4)
    plt.xlim(-100, 100)
    plt.ylim(-100, 100)
    plt.savefig("temp_replicate.pdf")


def plot_transitions(match, new_f_match, new_s_match,
                     film_latt_a, film_latt_b, sub_latt_a, sub_latt_b,
                     r_film_latt_a, r_film_latt_b, r_sub_latt_a, r_sub_latt_b,
                     substrate, mat2d, R_fa_sa,
                     plot=False):
    """Plot information for debugging translations

    """
    if plot is False:
        return 0
    import matplotlib.pyplot as plt
    plt.figure(figsize=(5, 5))
    plt.scatter(0, 0, c='k')
    plt.scatter(match[0][0][0],
                match[0][0][1], c='b', alpha=0.4)
    plt.scatter(match[0][1][0],
                match[0][1][1], c='b', alpha=0.4)
    plt.scatter(match[1][0][0],
                match[1][0][1], c='r', alpha=0.4)
    plt.scatter(match[1][1][0],
                match[1][1][1], c='r', alpha=0.4)

    plt.scatter(new_f_match[0][0],
                new_f_match[0][1], c='b')
    plt.scatter(new_f_match[1][0],
                new_f_match[1][1], c='b')
    plt.scatter(new_s_match[0][0],
                new_s_match[0][1], c='r')
    plt.scatter(new_s_match[1][0],
                new_s_match[1][1], c='r')

    plt.scatter(film_latt_a[0],
                film_latt_a[1], c='b', marker='x')
    plt.scatter(film_latt_b[0],
                film_latt_b[1], c='b', marker='x')
    plt.scatter(sub_latt_a[0],
                sub_latt_a[1], c='r', marker='x')
    plt.scatter(sub_latt_b[0],
                sub_latt_b[1], c='r', marker='x')

    for i in substrate.sites:
        # cartesian coordinates
        plt.scatter(i.x,
                    i.y, c='purple', marker='x', alpha=0.5)
    for i in mat2d.sites:
        # cartesian coordinates
        plt.scatter(i.x,
                    i.y, c='green', marker='x', alpha=0.5)

    plt.scatter(r_film_latt_a[0],
                r_film_latt_a[1], c='b', marker='<')
    plt.scatter(r_film_latt_b[0],
                r_film_latt_b[1], c='b', marker='<')
    plt.scatter(r_sub_latt_a[0],
                r_sub_latt_a[1], c='r', marker='<')
    plt.scatter(r_sub_latt_b[0],
                r_sub_latt_b[1], c='r', marker='<')

    for i in mat2d.sites:
        # cartesian coordinates
        posi_vector = [i.x, i.y, 0]
        r_posi_vector = np.dot(R_fa_sa, posi_vector)
        plt.scatter(r_posi_vector[0],
                    r_posi_vector[1], c='orange',
                    marker='x', alpha=0.5)

    plt.xlim(-100, 100)
    plt.ylim(-100, 100)
    plt.savefig("temp_supercells.pdf")


def visualise_all_cells(f_str, s_str, viz=False):
    """Debug function to visualise all cells produced.

    """
    if viz is True:
        if input("interested?") is 't':
            from ase.visualize import view
            view(f_str)
            view(s_str)
            view(f_str + s_str)
            input('done?')


def read_in_binding_planes(sub_out_cif_name, film_out_cif_name,
                           new_m, verbose=False):
    """Read in MOF and substrate binding planes as pymatgen structure objects
        and expand to supercells.

    Arguments:
        sub_out_cif_name (str) - name of substrate binding plane CIF
        film_out_cif_name (str) - name of film binding plane CIF
        new_m (dict) - dictionary of match including supercell vectors
        verbose (bool) - switch for verbose output

    Returns:
        substrate (PMG Structure) - pymatgen structure object of substrate SC
        mat2d (PMG Structure) - pymatgen structure object of film SC
        sub_latt_a (list) - substrate supercell 'a' vector
            (should == new_m)
        sub_latt_b (list) - substrate supercell 'b' vector
            (should == new_m)
        film_latt_a (list) - film supercell 'a' vector
            (should == new_m)
        film_latt_b (list) - film supercell 'b' vector
            (should == new_m)
        map_f (list) - integer matrix to transform unit cell to supercell
        map_s (list) - integer matrix to transform unit cell to supercell
    """
    # read in binding plane unit cells using pymatgen
    sub_ = mg.Structure.from_file(sub_out_cif_name)
    ff = mg.Structure.from_file(film_out_cif_name)

    # orient pymatgen unit cell with prescribed super cell
    substrate, mat2d, map_s, map_f = align_lattices(
            slab_sub=sub_,
            slab_film=ff,
            uv_substrate=new_m['sub_sl_vecs'],
            uv_film=new_m['film_sl_vecs'])

    sub_latt_a = substrate.lattice.matrix[0, :]
    sub_latt_b = substrate.lattice.matrix[1, :]

    film_latt_a = mat2d.lattice.matrix[0, :]
    film_latt_b = mat2d.lattice.matrix[1, :]

    if verbose is True:
        print(substrate.lattice.matrix)
        print(sub_latt_a)
        print(sub_latt_b)
        print(mat2d.lattice.matrix)
        print(film_latt_a)
        print(film_latt_b)
    return substrate, mat2d, sub_latt_a, sub_latt_b, film_latt_a, film_latt_b, map_f, map_s


def rotate_to_substrate_system(sub_latt_a, sub_latt_b, R_fa_sa,
                               film_latt_a, film_latt_b, verbose=False):
    """Rotate both supercells into substrate coordinate system.

    This is equivalent to aligning the 'a' vector of the film supercell with
    the 'a' vector of the substrate supercell.

    Keywords:
        sub_latt_a (list) - substrate supercell 'a' vector
            (should == new_m)
        sub_latt_b (list) - substrate supercell 'b' vector
            (should == new_m)
        R_fa_sa (3x3 array) - Rotation matrix about 'z' axis by angle between
            'a' vectors of film and substrate supercells
        film_latt_a (list) - film supercell 'a' vector
            (should == new_m)
        film_latt_b (list) - film supercell 'b' vector
            (should == new_m)
        verbose (bool) - switch for verbose output
    Returns:
        rotated supercell vectors of film and substrate
    """
    r_sub_latt_a = sub_latt_a
    r_sub_latt_b = sub_latt_b
    r_film_latt_a = np.dot(R_fa_sa, film_latt_a)
    r_film_latt_b = np.dot(R_fa_sa, film_latt_b)

    if verbose is True:
        print(r_sub_latt_a)
        print(r_sub_latt_b)
        print(r_film_latt_a)
        print(r_film_latt_b)

    return r_film_latt_a, r_film_latt_b, r_sub_latt_a, r_sub_latt_b


def get_supercell_rot_matrix(sub_latt_a, film_latt_a,
                             verbose=False):
    """Get rotation matrix about 'z' axis by the angle between supercell 'a'
        vectors.

    Must check that the resultant rotation is in the correct direction.

    Keywords:
        sub_latt_a (list) - substrate supercell 'a' vector
            (should == new_m)
        film_latt_a (list) - film supercell 'a' vector
            (should == new_m)
        verbose (bool) - switch for verbose output
    Returns:
        R_fa_sa (3x3 array) - Rotation matrix about 'z' axis by angle between
            'a' vectors of film and substrate supercells
        fa_sa_angle (float) - angle in radian between supercell 'a' vectors
    """
    fa_sa_angle = vec_angle(sub_latt_a, film_latt_a)

    R_fa_sa = rotation_matrix([0, 0, 1], fa_sa_angle)
    # check if this rotation was in the right direction
    f_SC_r = np.dot(R_fa_sa, film_latt_a)
    # if angle between rotated vector and set vector above
    # is not very close to 0, then the rotation is wrong
    close_to_zero = np.isclose(0,
                               vec_angle(sub_latt_a, f_SC_r),
                               rtol=0, atol=0.01)

    if verbose is True:
        print('rotation angle:', fa_sa_angle, np.degrees(fa_sa_angle))
        print(vec_angle(sub_latt_a, f_SC_r))
    if close_to_zero  == False:
        print('swapped direction of rotation angle')
        # if angle between rotated vector and set vector
        # above is not very close to 0, then the rotation
        # is wrong direction
        R_fa_sa = rotation_matrix([0, 0, 1], -fa_sa_angle)
        f_SC_r = np.dot(R_fa_sa, film_latt_a)
        if verbose is True:
            print('new rotation angle:', vec_angle(sub_latt_a, f_SC_r))

    return R_fa_sa, fa_sa_angle


def get_substrate_ASE(substrate, r_sub_latt_a, r_sub_latt_b):
    """Get ASE Atoms() object corresponding to substrate supercell.

    Keywords:
        substrate (PMG structure) - pymatgen structure object of substrate SC
        r_sub_latt_a (list) - rotated substrate supercell 'a' vector
        r_sub_latt_b (list) - rotated substrate supercell 'b' vector

    Returns:
        s_str (ASE Atoms) - ASE Atoms object of substrate supercell

    """
    s_str = Atoms()
    s_str.set_cell([r_sub_latt_a, r_sub_latt_b, [0, 0, 5]])
    s_str.set_pbc([True, True, True])
    for i in substrate.sites:
        # cartesian coordinates
        posi_vector = [i.x, i.y, 0]
        species = str(i.specie)
        s_str.append(Atom(symbol=species,
                          position=posi_vector))

    return s_str


def get_film_ASE(mat2d, r_film_latt_a, r_film_latt_b, R_fa_sa,
                 apply_translation):
    """Get ASE Atoms() object corresponding to film supercell.

    Keywords:
        mat2d (PMG structure) - pymatgen structure object of film SC
        r_film_latt_a (list) - rotated film supercell 'a' vector
        r_film_latt_b (list) - rotated film supercell 'b' vector
        R_fa_sa (3x3 array) - Rotation matrix about 'z' axis by angle between
            'a' vectors of film and substrate supercells
        apply_translation (bool) - whether or not to translate film supercell
            by 'b' vector

    Returns:
        f_str (ASE Atoms) - ASE Atoms object of film supercell
    """
    f_str = Atoms()
    # if translation of supercell is possible
    if apply_translation is True:
        f_str.set_cell([r_film_latt_a,
                        -r_film_latt_b,
                        [0, 0, 5]])
    else:
        f_str.set_cell([r_film_latt_a, r_film_latt_b,
                        [0, 0, 5]])
    f_str.set_pbc([True, True, True])
    for i in mat2d.sites:
        # cartesian coordinates
        posi_vector = [i.x, i.y, 0]
        r_posi_vector = np.dot(R_fa_sa, posi_vector)
        # if translation of supercell is possible
        if apply_translation is True:
            r_posi_vector = [
                r_posi_vector[0] - r_film_latt_b[0],
                r_posi_vector[1] - r_film_latt_b[1],
                0]
        species = str(i.specie)
        f_str.append(Atom(symbol=species,
                          position=r_posi_vector))

    return f_str


def determine_extra_rotations(r_sub_latt_a, r_sub_latt_b,
                              r_film_latt_a, r_film_latt_b,
                              max_angle_tol,
                              verbose=False):
    """Determine if the supercells are mirror images and if they can be over
       lapped.

    Keywords:
        r_sub_latt_a (list) - rotated substrate supercell 'a' vector
        r_sub_latt_b (list) - rotated substrate supercell 'b' vector
        r_film_latt_a (list) - rotated film supercell 'a' vector
        r_film_latt_b (list) - rotated film supercell 'b' vector
        max_angle_tol (float) - tolerance on angle between supercells
        verbose (bool) - switch for verbose output
    Returns:
        skip (bool) - if supercell shoudl be skipped
        apply_translation (bool) - whether or not to translate film supercell
            by 'b' vector

    """
    extra_rotation_nec = False
    apply_translation = False
    skip = False
    # use angle between b vectors of film and substrate SC
    # after rotation that aligns a vectors
    # if not near 0, then extra rotations would be required
    fb_sb_angle = vec_angle(r_sub_latt_b, r_film_latt_b)
    fa_fb_angle = vec_angle(r_film_latt_a, r_film_latt_b)

    if verbose is True:
        print('b angle after rotation:', fb_sb_angle,
              np.degrees(fb_sb_angle))
        print('fab angle after rotation:', fa_fb_angle,
              np.degrees(fa_fb_angle),
              fa_fb_angle * max_angle_tol,
              np.degrees(fa_fb_angle*max_angle_tol))
    if fb_sb_angle > fa_fb_angle*max_angle_tol:
        extra_rotation_nec = True
    if extra_rotation_nec is True:
        # check if it is a special case where the two super
        # cells can be overlapped by a translation
        # this is the case if the supercells are
        # rectangles
        # - i.e. fa_fb_angle is near 90 deg.
        # and sa_sb_angle is near 90 deg.
        sa_sb_angle = vec_angle(r_sub_latt_a, r_sub_latt_b)

        special_case = False
        test_f = np.isclose(fa_fb_angle, np.pi/2,
                            rtol=0,
                            atol=fa_fb_angle*max_angle_tol)
        test_s = np.isclose(sa_sb_angle, np.pi/2,
                            rtol=0,
                            atol=sa_sb_angle*max_angle_tol)
        if verbose is True:
            print('sab angle after rotation:', sa_sb_angle,
                  np.degrees(sa_sb_angle))
            print(fa_fb_angle*max_angle_tol)
            print(sa_sb_angle*max_angle_tol)
            print('f close to 90 deg?', test_f)
            print('s close to 90 deg?', test_s)
        if test_f == True and test_s == True:
            special_case = True
        if special_case is False:
            print("non-rectangular supercells")
            print("extra rotation required...")
            print("skipping.")
            # input("done?")
            skip = True
        elif special_case is True:
            print("rectangular supercells")
            print("extra rotation required...")
            print("apply translation of film supercell")
            print("by the b vector of the substrate SC")
            apply_translation = True
            # input("done?")

    return skip, apply_translation
