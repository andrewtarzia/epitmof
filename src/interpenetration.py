#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions to determine if a CIF is interpenetrated based on definition used
in CORE MOF database.
"""

from collections import Counter
from ase.io import read
from ase import Atoms
import binding_plane as bp

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def is_CIF_interpen(cif_name):
    """Determine if a CIF is interpenetrated based on definition used in CORE
    MOF paper.

    Definition:
        Interpenetrated MOF frameworks were retained by identifying the number
        of atoms, N, in the largest bonded component in the structure and
        retaining all additional components having at least 0.5N atoms.

    Returns boolean, list of molecules that are considered
        interpenetrating nets
    """
    # read structure into ASE
    struct = read(cif_name)
    molecule_list = bp.get_molecule_list(struct)

    # determine length (N) of each molecule in list
    N_for_all = [len(i) for i in molecule_list]
    max_N = max(N_for_all)
    # find molecules with N = max_N*0.5
    molecules_to_keep = [molecule_list.index(i)
                         for i in molecule_list if len(i) >= max_N*0.5]

    if len(molecules_to_keep) > 1:
        return True, molecules_to_keep
    else:
        return False, molecules_to_keep


def remove_duplicate_atoms(struct):
    """Check if there are overlapping atoms in 'struct' and remove them if so.

    """
    # check for and remove duplicate atoms using ASE
    a = struct
    b = a.get_all_distances() < 0.5
    atoms_to_rm = []
    for atom in a:
        idx = atom.index
        if Counter(b[idx])[True] > 1:
            for i, j in enumerate(b[idx]):
                if j is True:
                    if idx != i:
                        if idx not in atoms_to_rm and i not in atoms_to_rm:
                            atoms_to_rm.append(idx)
    new_struct = Atoms()
    for atom in a:
        if atom.index not in atoms_to_rm:
            new_struct.append(atom)
    return new_struct


def extract_interpen_nets(orig_struct):
    """Extract ASE structures from an input ASE structure for all nets of the
    interpenetrated CIF.

    Uses the definition of interpenetration from the CORE MOF paper, we create
    CIFs for each net to be tested separately.

    """
    orig_cell = orig_struct.get_cell()
    molecule_list = bp.get_molecule_list(orig_struct)

    # determine length (N) of each molecule
    N_for_all = [len(i) for i in molecule_list]
    max_N = max(N_for_all)
    # find molecules with N = max_N*0.5
    molecules_to_keep = [molecule_list.index(i)
                         for i in molecule_list if len(i) >= max_N*0.5]

    net_structures = {}
    for mol_id in molecules_to_keep:
        mol = molecule_list[mol_id]
        mol_struct = Atoms()
        for atom_id in mol:
            atom = orig_struct[atom_id]
            mol_struct.append(atom)
        # check for overlapping atoms
        # this is likely unnecessary, but it is there just in case.
        mol_struct = remove_duplicate_atoms(mol_struct)
        # apply cell from original structure
        mol_struct.set_pbc((True, True, True))
        mol_struct.set_cell(orig_cell)
        net_structures[mol_id] = mol_struct

    return net_structures
