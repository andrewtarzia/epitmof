#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Write the CIF_TODO.csv file for step 1/2 of the screening code
"""

import glob
from sys import exit
import os
import argparse
import general_functions as genf

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"

# parse CL arguments
parser = argparse.ArgumentParser(
        description="Write list of CIFs for screening algorithm")
parser.add_argument('paramfile', metavar='paramfile', type=str, nargs='+',
                    help='Parameter file.')
args = parser.parse_args()

# read in parameter dictionary
param_file = args.paramfile[0]
param_dict = genf.read_parameter_file(param_file)

# get number of CPUs
if param_dict['N_CPUs'][0] == 'None':
    print("please set the number of processes")
    print('exiting...')
    exit()
else:
    max_CPUS = int(param_dict['N_CPUs'][0])

# read in surface from parameter file
surfaces = param_dict['surfaces']
# attempt to read in set input CIFs from parameter file
if param_dict['films'][0] > '':
    cifs = [i for i in param_dict['films']]
else:
    # else use glob in current directory to get the list of CIFs
    cifs = []

    # exlusion list of phrases that exist in output file names
    # to distinguish input and output file names
    exlusion_list = ["_slab", "_int", "temp", "_surf", "_bind_"]

    for cif in glob.glob("*cif"):
        title = cif.replace(".cif", "")
        if title not in surfaces and os.path.isfile(cif) is True:
            not_output = True
            for i in exlusion_list:
                if i in title:
                    not_output = False
            if not_output is True:
                cifs.append(title)

genf.print_welcome_message(cifs)

cifs_sep = {}
# separate all CIFs into different processors
for NP in range(max_CPUS):
    cifs_sep[str(NP)] = []

NP = 0
for m in cifs:
    cifs_sep[str(NP)].append(m)
    if NP+1 == max_CPUS:
        NP = 0
    else:
        NP += 1

print("======================================================================")
print("No. Processes:", max_CPUS)
print("CIFs per Process:", [len(cifs_sep[i]) for i in cifs_sep.keys()])
print("======================================================================")

for NP in range(max_CPUS):
    CSV_name = param_dict['TODO_name'][0]+"_step_1_"+str(NP)+".csv"

    with open(CSV_name, 'w') as f:
        f.write('surface name,film name,status\n')
        for n in surfaces:
            try:
                for m in cifs_sep[str(NP)]:
                    f.write(str(n)+','+str(m)+',0\n')
            except KeyError:
                pass
