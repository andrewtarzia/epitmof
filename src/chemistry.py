#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Distributed under the terms of the MIT License.

"""
Functions used in step 1 of screening algorithm.
"""

import pymatgen as mg
from ase.data import chemical_symbols, covalent_radii
from ase.neighborlist import NeighborList

__author__ = "Andrew Tarzia"
__copyright__ = "Copyright 2018, Andrew Tarzia"
__email__ = "andrew.tarzia@adelaide.edu.au"


def metal_present(cif, metal):
    """Search a CIF for the desired metal atom type and confirms no other
        transition metal or alkali metal is present.

    """
    # definitions
    transitionMetals = [symbol
                        for symbol in chemical_symbols if symbol not in [
                                chemical_symbols[main_index]
                                for main_index in [
                                        1, 2, 5, 6, 7, 8, 9, 10, 14, 15, 16,
                                        17, 18, 33, 34, 35, 36, 52, 53, 54,
                                        85, 86]]]
    alkali = ['Li', 'Be', 'Na', 'Mg', 'K', 'Ca', 'Rb', 'Sr', 'Cs', 'Ba']
    list_of_metals = transitionMetals+alkali
    # remove metal from list_of_metals
    new_list_of_metals = [i for i in list_of_metals if i != metal]
    # search for metal
    structure = mg.Structure.from_file(cif)
    compo = set(list(structure.composition))
    result = None
    for cc in compo:
        if result is False:
            break
        elif str(cc) in new_list_of_metals:
            result = False
        elif str(cc) == metal:
            result = True
    return result


def find_carboxylate(ASE_struct, metal):
    """Searches for a carboxylate group in an ASE Atoms object.

    Breaks after finding one at least one carboxylate and outputs True
        or outputs False

    Carboxylate is defined with 2 Oxygens bound to the same C and bound to
    'metal' atoms
    """

    # build neighbor list here
    cov_rad = []
    for atom in ASE_struct:
        cov_rad.append(covalent_radii[atom.number])
    # build neighbour list
    neigh = NeighborList(cutoffs=cov_rad,
                         bothways=True,
                         self_interaction=False,
                         skin=0.3)
    neigh.build(ASE_struct)

    # loop through all atoms present checking for each test
    for atom in ASE_struct:
        if atom.symbol == 'O':
            # get neighbours
            nl, diss = neigh.get_neighbors(atom.index)
            # is neighbours a C and metal?
            neigh_types = [ASE_struct[i].symbol for i in nl]
            if 'C' in neigh_types and metal in neigh_types:
                C_index = nl[neigh_types.index('C')]
                # get neighbour list of C
                nl2, diss = neigh.get_neighbors(C_index)
                # get neighbours of C that are not the original O
                neigh_C_new = [i for i in nl2 if i != atom.index]
                neigh_C_types = [ASE_struct[i].symbol
                                 for i in nl2 if i != atom.index]
                # is C neighbours O?
                if 'O' in neigh_C_types:
                    O2_index = neigh_C_new[neigh_C_types.index('O')]
                    # get neighbour list of O2
                    nl3, diss = neigh.get_neighbors(O2_index)
                    # get neighbours of O2 that are not C
                    # neigh_O2_new = [i for i in nl3 if i != C_index]
                    neigh_O2_types = [ASE_struct[i].symbol
                                      for i in nl3 if i != C_index]
                    # is O2 neighbours M?
                    if metal in neigh_O2_types:
                        # return True
                        return True

    return False


def find_carboxylate_no_metal(ASE_struct):
    """Searches for a carboxylate group in an ASE Atoms object.

    Breaks after finding one at least one carboxylate and outputs True
        or outputs False

    Carboxylate is defined with 2 Oxygens bound to the same C and only
    one oxygen bound to a metal of some type (transitionMetals). The other
    oxygen can not be bound to a C or N atom.

    This was not used for the manuscript.
    """
    transitionMetals = [symbol
                        for symbol in chemical_symbols if symbol not in [
                                chemical_symbols[main_index]
                                for main_index in [
                                        1, 2, 5, 6, 7, 8, 9, 10, 14, 15, 16,
                                        17, 18, 33, 34, 35, 36, 52, 53, 54,
                                        85, 86]]]
    # build neighbor list here
    cov_rad = []
    for atom in ASE_struct:
        cov_rad.append(covalent_radii[atom.number])
    # build neighbour list
    neigh = NeighborList(cutoffs=cov_rad,
                         bothways=True,
                         self_interaction=False,
                         skin=0.3)
    neigh.build(ASE_struct)

    # loop through all atoms present checking for each test
    for atom in ASE_struct:
        if atom.symbol == 'O':
            # get neighbours
            nl, diss = neigh.get_neighbors(atom.index)
            # is neighbours one C and one metal?
            # this comprehension only adds non transition metal neighbours
            # to the list, therefore if the list length is > 1 implies 2
            # covalent bonds.
            neigh_types = [ASE_struct[i].symbol
                           for i in nl if ASE_struct[i].symbol
                           not in transitionMetals]
            if 'C' in neigh_types and len(neigh_types) == 1:
                C_index = nl[neigh_types.index('C')]
                # get neighbour list of C
                nl2, diss = neigh.get_neighbors(C_index)
                # get neighbours of C that are not the original O
                neigh_C_new = [i for i in nl2 if i != atom.index]
                neigh_C_types = [ASE_struct[i].symbol
                                 for i in nl2 if i != atom.index]
                # is C neighbours O?
                if 'O' in neigh_C_types:
                    O2_index = neigh_C_new[neigh_C_types.index('O')]
                    # get neighbour list of O2
                    nl3, diss = neigh.get_neighbors(O2_index)
                    # get neighbours of O2 that are not C
                    # neigh_O2_new = [i for i in nl3 if i != C_index]
                    neigh_O2_types = [ASE_struct[i].symbol
                                      for i in nl3
                                      if i != C_index and ASE_struct[i].symbol
                                      not in transitionMetals]
                    # is O2 neighbours 0 if C is removed?
                    if len(neigh_O2_types) == 0:
                        # return True
                        return True

    return False
